//
//  ColorLabel.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorLabel : UILabel

@property (nonatomic, copy)NSString * title;

@end
