//
//  PDAlertView.h
//  PaiDou
//
//  Created by JeremyRen on 15/1/8.
//  Copyright (c) 2015年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDAlertView : UIWindow

@property (nonatomic, copy) void(^block)(NSString * str);

-(void)show;

-(void)dismiss;

@end
