//
//  GuideView.h
//  Guide
//
//  Created by JeremyRen on 14/12/25.
//  Copyright (c) 2014年 JeremyRen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideView : UIImageView

@property (strong, nonatomic) NSArray * array;

-(id)initWithFrame:(CGRect)frame array:(NSArray *)arr;

@end
