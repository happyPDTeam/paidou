//
//  GuideView.m
//  Guide
//
//  Created by JeremyRen on 14/12/25.
//  Copyright (c) 2014年 JeremyRen. All rights reserved.
//

#import "GuideView.h"

@implementation GuideView
@synthesize array;
static int count = 1;


-(id)initWithFrame:(CGRect)frame array:(NSArray *)arr
{
    
    self = [[GuideView alloc] initWithFrame:frame];
    
    self.array = arr;
    
    if (arr.count) {
        [self setImage:[UIImage imageNamed:arr[0]]];
    }
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    
    self.userInteractionEnabled = YES;
    
    [self addGestureRecognizer:tap];

    return self;
}

//-(void)setArray:(NSArray *)iarray
//{
//    if (iarray.count) {
//        self.array = iarray;
//        
//        [self setImage:[UIImage imageNamed:self.array[count]]];
//
//    }
//}

-(void)tap
{
    if (count < self.array.count) {
        //self.image = [UIImage imageNamed:self.array[count]];
        
        [self setImage:[UIImage imageNamed:self.array[count]]];
        
        count ++;
    }
    else
    {
        self.hidden = YES;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
