//
//  ColorLabel.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ColorLabel.h"

@implementation ColorLabel

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        //[self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //[self initialize];
    }
    return self;
}

-(void)setTitle:(NSString *)title
{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:title];
    
    NSRange range;
    
    range.location = 3;
    
    if ([title hasSuffix:@"元"]) {
        
        range.length = title.length - range.location - 1;
    }
    else
    {
        range.length = title.length - range.location - 2;
    }
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appGreenColor] range:NSMakeRange(0, 3)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appOrangeColor] range:range];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appGreenColor] range:NSMakeRange(range.length + range.location,title.length - (range.length + range.location))];
    
    [str addAttribute:NSFontAttributeName value:[UIFont appCellDetailFont] range:NSMakeRange(0, 3)];
    [str addAttribute:NSFontAttributeName value:[UIFont appCommonFont] range:range];
    [str addAttribute:NSFontAttributeName value:[UIFont appCellDetailFont] range:NSMakeRange(range.length + range.location,title.length - (range.length + range.location))];
    
    self.attributedText = str;
}

@end
