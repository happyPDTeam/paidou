//
//  ShowBeansView.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowBeansView : UIView

@property (nonatomic, copy) void(^block)();

-(void)setTitle:(NSString *)firstTitle secondTitle:(NSString *)secondTitle buttonTitle:(NSString *)buttonTitle;

@end
