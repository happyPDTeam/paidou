//
//  StrikeLabel.m
//  testStrikeLabel
//
//  Created by JSen on 14/11/10.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "StrikeLabel.h"

@implementation StrikeLabel

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}



- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    return self;
}

- (void)initialize{
    _strikeOutColor = [UIColor grayColor];
    _strokeWidth = 2;
}

- (void)setStrikeColor:(UIColor *)strikeColor{
    strikeColor = strikeColor;
    [self setNeedsDisplay];
}

- (void)setStrokeWidth:(NSInteger)strokeWidth{
    _strokeWidth = strokeWidth;
    [self setNeedsDisplay];
}

- (void)drawTextInRect:(CGRect)rect{
    
    [super drawTextInRect: rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0);
    
    if (self.strikeOutColor != nil) {
        CGContextSetStrokeColorWithColor(context, self.strikeOutColor.CGColor);
    }
    else {
        CGContextSetStrokeColorWithColor(context, self.textColor.CGColor);
    }
    
    CGFloat lineStartX = 0;
    CGFloat lineEndX = 0;
    
    
    CGSize textSize = [self.text sizeWithFont: self.font constrainedToSize: rect.size lineBreakMode: self.lineBreakMode];
    CGFloat lineY = rect.origin.y + rect.size.height/2;
    if (self.textAlignment == NSTextAlignmentLeft) {
        lineEndX = lineStartX + textSize.width;
    }
    else if (self.textAlignment == NSTextAlignmentCenter) {
        lineStartX = (rect.size.width / 2) - (textSize.width / 2);
        lineEndX = (rect.size.width / 2) + (textSize.width / 2);
    }
    else {
        lineStartX = rect.size.width - textSize.width;
        lineEndX = rect.size.width;
    }
    
    CGContextMoveToPoint(context, lineStartX, lineY);
    CGContextAddLineToPoint(context, lineEndX, lineY);
    CGContextStrokePath(context);
    
    
    
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/*(
- (void)drawRect:(CGRect)rect {
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    if (self.strokeWidth > 0.0 && self.shadowColor != nil) {
        CGContextSetLineWidth(c, self.strokeWidth);
        CGContextSetLineJoin(c, kCGLineJoinRound);
        CGContextSetTextDrawingMode(c, kCGTextStroke);
        UIColor *oldColor = self.textColor;
        self.textColor = self.shadowColor;
        [super drawTextInRect:rect];
        self.textColor = oldColor;
    }
    CGContextSetTextDrawingMode(c, kCGTextFill);
    [super drawTextInRect:rect];
    return;
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetShouldAntialias(context, true);
//    CGContextSetShouldSmoothFonts(context, true);
    
    CGSize textSize;
    textSize = [self.text drawInRect:rect withFont:self.font];
  //  textSize = [self.text drawInRect:rect withAttributes:@{NSFontAttributeName:self.font}];
     const CGFloat *components = CGColorGetComponents([self.strikeColor CGColor]);
    CGContextSetRGBStrokeColor(context, components[0], components[1], components[2], CGColorGetAlpha(self.strikeColor.CGColor));
    CGContextSetLineWidth(context, 2.0);
    
    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y+textSize.height/2);
    CGContextAddLineToPoint(context, rect.origin.x+textSize.width, rect.origin.y+textSize.height/2);
    CGContextStrokePath(context);
}
*/

@end
