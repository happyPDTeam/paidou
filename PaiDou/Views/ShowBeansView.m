//
//  ShowBeansView.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ShowBeansView.h"
#import "ClickableUIView.h"
#import "ColorLabel.h"

@interface ShowBeansView ()
{
    ColorLabel * _firstLabel;
    
    ColorLabel * _secondLabel;
    
    UIButton * _detailButton;
}

@end

@implementation ShowBeansView

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _firstLabel = [[ColorLabel alloc] initWithFrame:CGRectMake(20, 5, 100 , 30)];
        
        [self addSubview:_firstLabel];
        
        _secondLabel = [[ColorLabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH / 2) - 40, 5, 120, 30)];
        
        _secondLabel.hidden = YES;
        
        [self addSubview:_secondLabel];
        
        _detailButton = [ViewManager createButton:CGRectMake(SCREEN_WIDTH - 80, 8, 80, 30) bgImage:nil image:nil title:@"派豆明细》" method:@selector(click) target:self];
        
        _detailButton.titleLabel.font = [UIFont systemFontOfSize:14];
        
        [_detailButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [self addSubview:_detailButton];
    }
    return self;
}

-(void)setTitle:(NSString *)firstTitle secondTitle:(NSString *)secondTitle buttonTitle:(NSString *)buttonTitle
{
    _firstLabel.title = secondTitle;
    
    _secondLabel.title = secondTitle;
    
    [_detailButton setTitle:buttonTitle forState:UIControlStateNormal];
}

-(void)click
{
    
    if (self.block) {
        self.block();
    }
}

@end