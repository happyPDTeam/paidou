//
//  PDAlertView.m
//  PaiDou
//
//  Created by JeremyRen on 15/1/8.
//  Copyright (c) 2015年 wifitong. All rights reserved.
//

#import "PDAlertView.h"

@interface PDAlertView ()<UITextFieldDelegate>
{
    UIImageView * _bgImageView;
    
    UITextField * _textField;
    
    UILabel * _scoreLabel;
    
    UIButton * _button;
    
    UIButton * _knowbutton;
    
    UIButton * _closeButton;
    
    UIView * _viewBefore;
    
    UIView * _viewAfter;
}

@end

@implementation PDAlertView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.windowLevel = UIWindowLevelAlert;
        // 这里，不能设置UIWindow的alpha属性，会影响里面的子view的透明度，这里我们用一张透明的图片
        // 设置背影半透明
        self.backgroundColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:0.6];
    
        [self createView];
    }
    
    return self;
}

-(void)createView
{
    _bgImageView = [ViewManager createImageView:CGRectMake(20, (self.frame.size.height - 200) / 2, self.frame.size.width - 40, 200) imageName:@"alertbg1"];
    
    _bgImageView.layer.masksToBounds = YES;
    
    _bgImageView.layer.cornerRadius = 5;
    
    [self addSubview:_bgImageView];
    //    _viewBefore = [[UIView alloc] initWithFrame:CGRectMake(0, 60, self.frame.size.width, self.frame.size.height - 60)];
    //
    //    [self addSubview:_viewBefore];
    //
    //    _viewAfter = [[UIView alloc] initWithFrame:CGRectMake(0, 60, self.frame.size.width, self.frame.size.height - 60)];
    //
    //    _viewAfter.hidden = YES;
    //
    //    [self addSubview:_viewAfter];
    
    UILabel * titleLabel = [ViewManager createLabel:CGRectMake((_bgImageView.frame.size.width - 80) / 2, 15, 80, 30) title:@"派豆兑换码" font:14 textColor:[UIColor whiteColor]];
    
    [_bgImageView addSubview:titleLabel];
    
    _closeButton = [ViewManager createButton:CGRectMake(self.frame.size.width - 60, 20, 40, 40) bgImage:nil image:@"duihuan_del" title:nil method:@selector(dismiss) target:self];
    
    _closeButton.backgroundColor = [UIColor yellowColor];
    
    [_closeButton setImage:[UIImage imageNamed:@"duihuan_del_hov"] forState:UIControlStateHighlighted];
    
    [self addSubview:_closeButton];
    
    _textField = [ViewManager createTextField:CGRectMake(20, 60, _bgImageView.frame.size.width - 40, 30) delegate:self palceHolder:@"派豆兑换码"];
    
    [_bgImageView addSubview:_textField];
    
    _button = [ViewManager createButton:CGRectMake((_bgImageView.frame.size.width - 70) / 2, 110, 70, 30) bgImage:nil image:nil title:@"立即兑换" method:@selector(click) target:self];
    
    [_bgImageView addSubview:_button];
    //
    //    _scoreLabel = [ViewManager createLabel:CGRectMake(20, 20, _viewAfter.frame.size.width - 40, 30) title:nil font:14 textColor:[UIColor whiteColor]];
    //
    //    _scoreLabel.hidden = YES;
    //
    //    [_viewAfter addSubview:_scoreLabel];
    //
    //    _knowbutton = [ViewManager createButton:CGRectMake((self.frame.size.width - 70) / 2, 70, 70, 30) bgImage:nil image:nil title:@"知道了" method:@selector(close) target:self];
    //
    //    _knowbutton.layer.borderWidth = 1;
    //
    //    _knowbutton.hidden = YES;
    //
    //    _knowbutton.layer.borderColor = [UIColor blackColor].CGColor;
    //
    //    [_viewBefore addSubview:_knowbutton];
    //
    //    _viewAfter.hidden = YES;
}

-(void)click
{
    if (_textField.text.length == 0) {
        if (self.block) {
            self.block(@"请输入派豆兑换码");
        }
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    //NSString * uid = [[UserManager sharedManager] uid];
    
    NSMutableDictionary * dic = [NSMutableDictionary new];
    
    [dic setValue:_textField.text forKey:@"code"];
    
    [manager GET:kScoreCode parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if ([responseObject[@"err_msg"] isEqualToString:@"success"]) {
                
                _viewAfter.hidden = NO;
                
                NSError *err ;
                
                NSString * string = responseObject[@"value"];
                
                [self setLabelText:string];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    if (self.block) {
                        self.block(@"兑换码错误");
                    }
                }
                
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (self.block) {
            self.block(@"兑换失败");
        }
        
        NSLog(@"%@",error);
    }];
}

-(void)setLabelText:(NSString *)string
{
    [_bgImageView setImage:[UIImage imageNamed:@"alertbg2@2x"]];
    
    NSString * text = [NSString stringWithFormat:@"恭喜你获得%@派豆", string];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text];
    
    //    NSRange range;
    //
    //    range.location = 3;
    //
    //    if ([title hasSuffix:@"元"]) {
    //
    //        range.length = title.length - range.location - 1;
    //    }
    //    else
    //    {
    //        range.length = title.length - range.location - 2;
    //    }
    //
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appGreenColor] range:NSMakeRange(0, 5)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appOrangeColor] range:NSMakeRange(6, string.length)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appGreenColor] range:NSMakeRange(6 + string.length,2)];
    
    [str addAttribute:NSFontAttributeName value:[UIFont appCellDetailFont] range:NSMakeRange(0, 5)];
    [str addAttribute:NSFontAttributeName value:[UIFont appCommonFont] range:NSMakeRange(6, string.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont appCellDetailFont] range:NSMakeRange(6 + string.length,2)];
    
    _scoreLabel.attributedText = str;
}

//-(void)close
//{
//    self.hidden = YES;
//    
//    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
//    
//    delegate.window.alpha = 1;
//    
//    [self resignFirstResponder];
//}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

- (void)show {
    [_bgImageView becomeFirstResponder];
    
    [self makeKeyAndVisible];
}

- (void)dismiss {
//    AppDelegate * del = [UIApplication sharedApplication].delegate;
//    
//    [del.window makeKeyAndVisible];
    
    [self resignKeyWindow];
    
    self.hidden = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // 点击消失
    [self dismiss];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
