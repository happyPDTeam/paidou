//
//  StrikeLabel.h
//  testStrikeLabel
//
//  Created by JSen on 14/11/10.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StrikeLabel : UILabel

@property (nonatomic, strong) UIColor *strikeOutColor ;

@property (nonatomic, assign)  NSInteger strokeWidth;

@end
