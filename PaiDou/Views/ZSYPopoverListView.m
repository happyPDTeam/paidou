//
//  ZSYPopoverListView.m
//  MyCustomTableViewForSelected
//
//  Created by Zhu Shouyu on 6/2/13.
//  Copyright (c) 2013 zhu shouyu. All rights reserved.
//

#import "ZSYPopoverListView.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>
#import "MBProgressHUD.h"

static const CGFloat ZSYCustomButtonHeight = 40;

static const char * const kZSYPopoverListButtonClickForCancel = "kZSYPopoverListButtonClickForCancel";

static const char * const kZSYPopoverListButtonClickForDone = "kZSYPopoverListButtonClickForDone";

@interface ZSYPopoverListView ()<UITextFieldDelegate>
{
    MBProgressHUD *_baseHUD;
    
    UITextField * _textField;
    
    UILabel * _label;
    
    UIButton * _knowButton;
    
    UIButton * _getButton;
    
    UIButton * _closeButton;
}

//@property (nonatomic, retain) UITableView *mainPopoverListView;                 //主的选择列表视图
@property (nonatomic, retain) UIButton *doneButton;                             //确定选择按钮
@property (nonatomic, retain) UIButton *cancelButton;                           //取消选择按钮
@property (nonatomic, retain) UIControl *controlForDismiss;                     //没有按钮的时候，才会使用这个
//初始化界面 
- (void)initTheInterface;

//动画进入
- (void)animatedIn;

//动画消失
- (void)animatedOut;

//展示界面
- (void)show;

//消失界面
- (void)dismiss;
@end

@implementation ZSYPopoverListView
//@synthesize datasource = _datasource;
//@synthesize delegate = _delegate;
//@synthesize mainPopoverListView = _mainPopoverListView;
@synthesize doneButton = _doneButton;
@synthesize cancelButton = _cancelButton;
@synthesize titleName = _titleName;
@synthesize controlForDismiss = _controlForDismiss;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initTheInterface];
    }
    return self;
}

- (void)initTheInterface
{
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.layer.borderWidth = 1.0f;
    self.layer.cornerRadius = 10.0f;
    self.clipsToBounds = TRUE;
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    imageView.image = [UIImage imageNamed:@"alertbg.jpg"];
    
    [self addSubview:imageView];
    
    _titleName = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 160) / 2, 20, 160, 30)];
    self.titleName.font = [UIFont systemFontOfSize:17.0f];
    //self.titleName.backgroundColor = [UIColor colorWithRed:59./255. green:89./255. blue:152./255. alpha:1.0f];
    
    self.titleName.textAlignment = NSTextAlignmentCenter;
    self.titleName.textColor = [UIColor whiteColor];
    CGFloat xWidth = self.bounds.size.width;
    self.titleName.lineBreakMode = NSLineBreakByTruncatingTail;
    //self.titleName.frame = CGRectMake(0, 0, xWidth, 32.0f);
    [self addSubview:self.titleName];
    
    //CGRect tableFrame = CGRectMake(0, 32.0f, xWidth, self.bounds.size.height-32.0f);

    [self createView];

}

-(void)createView
{
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(22, 80, (self.frame.size.width - 44), 40)];
    
    _textField.delegate = self;
    
    _textField.layer.masksToBounds = YES;
    
    _textField.layer.cornerRadius = 3;
    
    _textField.layer.borderWidth = 0.3;
    
    _textField.textColor = [UIColor colorWithRed:202/255.0 green:202/255.0 blue:202/255.0 alpha:1];
    
    _textField.layer.borderColor = [UIColor colorWithRed:202/255.0 green:202/255.0 blue:202/255.0 alpha:1].CGColor;
    
    _textField.placeholder = @"输入你的兑换券";
    
    [self addSubview:_textField];
    
    _getButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    _getButton.frame = CGRectMake(100, 150, 80, 40);
    
    [_getButton setTitle:@"立即兑换" forState:UIControlStateNormal];
    
    [_getButton setTitleColor:[UIColor appBlueTextColor] forState:UIControlStateNormal];
    
    _getButton.titleLabel.font = [UIFont systemFontOfSize:16];
    
    [_getButton addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_getButton];
    
    _label = [ViewManager createLabel:CGRectMake(40, 80, self.frame.size.width - 80, 40) title:nil font:18 textColor:nil];
    
    _label.hidden = YES;
    
    [self addSubview:_label];
    
    _knowButton = [ViewManager createButton:CGRectMake(14, 140, self.frame.size.width - 28, 40) bgImage:nil image:nil title:@"知道了" method:@selector(close) target:self];
    
    _knowButton.layer.masksToBounds = YES;
    
    _knowButton.layer.cornerRadius = 3;
    
    _knowButton.layer.borderWidth = 1;
    
    _knowButton.layer.borderColor =[UIColor appBlueTextColor].CGColor;
    
    _knowButton.titleLabel.textColor = [UIColor appBlueTextColor];
    
    _knowButton.hidden = YES;
    
    [self addSubview:_knowButton];
    
    _closeButton = [ViewManager createButton:CGRectMake(self.frame.size.width - 50, 10, 40, 40) bgImage:nil image:@"duihuan_del_hov" title:nil method:@selector(close) target:self];
    
    [_closeButton setImage:[UIImage imageNamed:@"duihuan_del"] forState:UIControlStateHighlighted];
    
    [self addSubview:_closeButton];
}

-(void)close
{
    [self animatedOut];
}

-(void)click
{
    if (_textField.text.length == 0) {
        
        [self showHUDText:@"请输入派豆兑换码" xOffset:0 yOffset:0];
        
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    //NSString * uid = [[UserManager sharedManager] uid];
    
    NSMutableDictionary * dic = [NSMutableDictionary new];
    
    [dic setValue:_textField.text forKey:@"code"];
    
    [manager POST:kScoreCode parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if ([responseObject[@"err_msg"] isEqualToString:@"success"]) {
                
                NSError *err ;
                
                NSString * string = [NSString stringWithFormat:@"%@", responseObject[@"value"] ];
                
                [self setLabelText:string];
                
                _label.hidden = NO;
                
                _knowButton.hidden = NO;
                
                _textField.hidden = YES;
                
                _getButton.hidden = YES;
                
                _closeButton.hidden = YES;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyNeedUpdateUserInfo object:nil];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    
                }
                
            }
            
        }
        else
        {
            [self showHUDText:@"兑换码错误" xOffset:0 yOffset:0];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog (@"operation:%@", operation.responseString);
        
        [self showHUDText:@"兑换失败" xOffset:0 yOffset:0];
        NSLog(@"%@",error);
    }];
}

- (void)showHUDText:(NSString *)text xOffset:(float)x yOffset:(float)y {
    
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    
    _baseHUD = [MBProgressHUD showHUDAddedTo:keywindow animated:YES];
    _baseHUD.userInteractionEnabled = NO;
    _baseHUD.mode = MBProgressHUDModeText;
    _baseHUD.xOffset = x;
    _baseHUD.yOffset = y;
    _baseHUD.margin = 10;
    _baseHUD.detailsLabelText = text;
    _baseHUD.detailsLabelFont =[UIFont systemFontOfSize:16];
    _baseHUD.minShowTime = 1;
    [_baseHUD show:YES];
    
    [_baseHUD hide:YES afterDelay:1.0f];
    
}

-(void)setLabelText:(NSString *)string
{
    //[_bgImageView setImage:[UIImage imageNamed:@"alertbg2@2x"]];
    
    NSString * text = [NSString stringWithFormat:@"恭喜你获得%@派豆!", string];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text];
   
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appLightGrayColor] range:NSMakeRange(0, 5)];
    [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff6c00) range:NSMakeRange(5, string.length)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appLightGrayColor] range:NSMakeRange(5 + string.length,2)];
    
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, str.length)];
    
    _label.attributedText = str;
}

- (void)refreshTheUserInterface
{
    if (self.cancelButton || self.doneButton)
    {
        //self.mainPopoverListView.frame = CGRectMake(0, 32.0f, self.mainPopoverListView.frame.size.width, self.mainPopoverListView.frame.size.height - ZSYCustomButtonHeight);
    }
    if (self.doneButton && nil == self.cancelButton)
    {
        self.doneButton.frame = CGRectMake(0, self.bounds.size.height - ZSYCustomButtonHeight, self.bounds.size.width, ZSYCustomButtonHeight);
    }
    else if (nil == self.doneButton && self.cancelButton)
    {
        self.cancelButton.frame = CGRectMake(0, self.bounds.size.height - ZSYCustomButtonHeight, self.bounds.size.width, ZSYCustomButtonHeight);
    }
    else if (self.doneButton && self.cancelButton)
    {
        self.doneButton.frame = CGRectMake(0, self.bounds.size.height - ZSYCustomButtonHeight, self.bounds.size.width / 2.0f, ZSYCustomButtonHeight);
        self.cancelButton.frame = CGRectMake(self.bounds.size.width / 2.0f, self.bounds.size.height - ZSYCustomButtonHeight, self.bounds.size.width / 2.0f, ZSYCustomButtonHeight);
    }
    if (nil == self.cancelButton && nil == self.doneButton)
    {
        if (nil == _controlForDismiss)
        {
            _controlForDismiss = [[UIControl alloc] initWithFrame:[UIScreen mainScreen].bounds];
            _controlForDismiss.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
            [_controlForDismiss addTarget:self action:@selector(touchForDismissSelf:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

#pragma mark - Animated Mthod
- (void)animatedIn
{
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)animatedOut
{
    [UIView animateWithDuration:.35 animations:^{
        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            if (self.controlForDismiss)
            {
                [self.controlForDismiss removeFromSuperview];
            }
            [self removeFromSuperview];
        }
    }];
}

#pragma mark - show or hide self
- (void)show
{
    [self refreshTheUserInterface];
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    if (self.controlForDismiss)
    {
        [keywindow addSubview:self.controlForDismiss];
    }
    [keywindow addSubview:self];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,
                              keywindow.bounds.size.height/2.0f);
    [self animatedIn];
}

- (void)dismiss
{
    [self animatedOut];
}

//#pragma mark - Reuse Cycle
//- (id)dequeueReusablePopoverCellWithIdentifier:(NSString *)identifier
//{
//    return [self.mainPopoverListView dequeueReusableCellWithIdentifier:identifier];
//}
//
//- (UITableViewCell *)popoverCellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return [self.mainPopoverListView cellForRowAtIndexPath:indexPath];
//}

+ (UIImage *)normalButtonBackgroundImage
{
	const size_t locationCount = 4;
	CGFloat opacity = 1.0;
    CGFloat locations[locationCount] = { 0.0, 0.5, 0.5 + 0.0001, 1.0 };
    CGFloat components[locationCount * 4] = {
		179/255.0, 185/255.0, 199/255.0, opacity,
		121/255.0, 132/255.0, 156/255.0, opacity,
		87/255.0, 100/255.0, 130/255.0, opacity,
		108/255.0, 120/255.0, 146/255.0, opacity,
	};
	return [self glassButtonBackgroundImageWithGradientLocations:locations
													  components:components
												   locationCount:locationCount];
}

+ (UIImage *)cancelButtonBackgroundImage
{
	const size_t locationCount = 4;
	CGFloat opacity = 1.0;
    CGFloat locations[locationCount] = { 0.0, 0.5, 0.5 + 0.0001, 1.0 };
    CGFloat components[locationCount * 4] = {
		164/255.0, 169/255.0, 184/255.0, opacity,
		77/255.0, 87/255.0, 115/255.0, opacity,
		51/255.0, 63/255.0, 95/255.0, opacity,
		78/255.0, 88/255.0, 116/255.0, opacity,
	};
	return [self glassButtonBackgroundImageWithGradientLocations:locations
													  components:components
												   locationCount:locationCount];
}

+ (UIImage *)glassButtonBackgroundImageWithGradientLocations:(CGFloat *)locations
												  components:(CGFloat *)components
											   locationCount:(NSInteger)locationCount
{
	const CGFloat lineWidth = 1;
	const CGFloat cornerRadius = 4;
	UIColor *strokeColor = [UIColor colorWithRed:1/255.0 green:11/255.0 blue:39/255.0 alpha:1.0];
	
	CGRect rect = CGRectMake(0, 0, cornerRadius * 2 + 1, ZSYCustomButtonHeight);
    
	BOOL opaque = NO;
    UIGraphicsBeginImageContextWithOptions(rect.size, opaque, [[UIScreen mainScreen] scale]);
    
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, locationCount);
	
	CGRect strokeRect = CGRectInset(rect, lineWidth * 0.5, lineWidth * 0.5);
	UIBezierPath *strokePath = [UIBezierPath bezierPathWithRoundedRect:strokeRect cornerRadius:cornerRadius];
	strokePath.lineWidth = lineWidth;
	[strokeColor setStroke];
	[strokePath stroke];
	
	CGRect fillRect = CGRectInset(rect, lineWidth, lineWidth);
	UIBezierPath *fillPath = [UIBezierPath bezierPathWithRoundedRect:fillRect cornerRadius:cornerRadius];
	[fillPath addClip];
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
	CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
	CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
	
	CGGradientRelease(gradient);
	CGColorSpaceRelease(colorSpace);
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
	CGFloat capHeight = floorf(rect.size.height * 0.5);
	return [image resizableImageWithCapInsets:UIEdgeInsetsMake(capHeight, cornerRadius, capHeight, cornerRadius)];
}

#pragma mark - Button Method
- (void)setCancelButtonTitle:(NSString *)aTitle block:(ZSYPopoverListViewButtonBlock)block
{
    if (nil == _cancelButton)
    {
        self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.cancelButton setBackgroundImage:[ZSYPopoverListView cancelButtonBackgroundImage] forState:UIControlStateNormal];
        [self.cancelButton addTarget:self action:@selector(buttonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.cancelButton];
    }
    [self.cancelButton setTitle:aTitle forState:UIControlStateNormal];
    objc_setAssociatedObject(self.cancelButton, kZSYPopoverListButtonClickForCancel, [block copy], OBJC_ASSOCIATION_RETAIN);
}

- (void)setDoneButtonWithTitle:(NSString *)aTitle block:(ZSYPopoverListViewButtonBlock)block
{
    if (nil == _doneButton)
    {
        self.doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.doneButton setBackgroundImage:[ZSYPopoverListView normalButtonBackgroundImage] forState:UIControlStateNormal];
        [self.doneButton addTarget:self action:@selector(buttonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.doneButton];
    }
    [self.doneButton setTitle:aTitle forState:UIControlStateNormal];
    objc_setAssociatedObject(self.doneButton, kZSYPopoverListButtonClickForDone, [block copy], OBJC_ASSOCIATION_RETAIN);
}
#pragma mark - UIButton Clicke Method
- (void)buttonWasPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    ZSYPopoverListViewButtonBlock __block block;
    
    if (button == self.cancelButton)
    {
        block = objc_getAssociatedObject(sender, kZSYPopoverListButtonClickForCancel);
    }
    else
    {
        block = objc_getAssociatedObject(sender, kZSYPopoverListButtonClickForDone);
    }
    if (block)
    {
        block();
    }
    [self animatedIn];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchForDismissSelf:(id)sender
{
    [self endEditing:YES];
    
    //[self animatedOut];
}
@end
