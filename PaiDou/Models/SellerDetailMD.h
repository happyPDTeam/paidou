//
//  SellerDetail.h
//  wft
//
//  Created by JSen on 14/10/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"
#import "ProductSummaryMD.h"


#warning 扫码获得的某一商家的产品列表 和 派豆兑换的产品列表的结构不同 ，pete建议暂时不解析产品列表字段

@interface SellerDetailMD : JSONModel
PropertyString(address);
PropertyString(avatar_url);

PropertyInt(balance);
PropertyString(bid);
PropertyString(brief);
@property (nonatomic, strong) NSArray *categories ;
PropertyString(city);

@property (nonatomic, assign) NSInteger max_score; //商家最多允许使用的都比

@property (nonatomic, copy) NSString<Optional> *latitude;
@property (nonatomic, copy) NSString<Optional> *longitude;

PropertyString(name);

PropertyInt(product_count);

@property (nonatomic, strong) NSArray<ProductSummaryMD,Ignore> *products ;
PropertyString(qrcode_url);
PropertyInt(station_id);
PropertyString(station_name);

@property (nonatomic, retain) NSArray *redeem_time;

@property (nonatomic, assign) NSInteger today_earnings;

@property (nonatomic, assign) NSInteger today_orders;

@property (nonatomic, assign) NSInteger total_sold;
@end
