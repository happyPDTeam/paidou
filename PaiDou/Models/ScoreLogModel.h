//
//  ScoreLogModel.h
//  PaiDou
//
//  Created by JSen on 14/11/7.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"

@interface ScoreLogModel : JSONModel

PropertyString(uid);

/*积分事件类型：
 task：完成某任务造成的加分或减分。
 order：用户下单造成的加分或减分。
 scan_biz：扫描商户二维码。
 ...
 */
PropertyString(event);

PropertyInt(score); //获得或消费的积分数值。>0赚积分，<0消耗积分

PropertyInt(create_time);
  //如果type=task，标记任务的id。否则为“”空。
@property (nonatomic, copy) NSString<Optional> *tid;

  //如果type=task，标记任务的标题。否则为“”空。
@property (nonatomic, copy) NSString<Optional> *title;

@property (nonatomic, copy) NSString<Optional> *biz_name;

//如果type=order，标记订单id，否则“”
@property (nonatomic, copy) NSString<Optional> *oid;

//如果type=order，且是购买商品，标记商品id。否则“”
@property (nonatomic, copy) NSString<Optional> *pid;
PropertyString(p_name);

PropertyString(p_count);

PropertyString(bid);//如果type=scan_biz，标记扫描的商户的id

@end
