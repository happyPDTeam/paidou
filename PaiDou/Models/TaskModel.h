//
//  TaskModel.h
//  PaiDou
//
//  Created by JSen on 14/11/7.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"

@interface TaskModel : JSONModel

PropertyString(tid);
PropertyString(title);
PropertyString(image_url);

//总共份数 int 为空时null，不限制份数
PropertyInt(limit_all);

////每人限制的份数 int，为空时为限制每人份数
PropertyInt(limit_user);

PropertyString(desc);

PropertyString(score_type); ////积分类型，固定，随机

PropertyString(score);      //可获得多少分 string可能为区间
PropertyInt(money);      ////暂时返回为空
PropertyInt(start_time);
PropertyInt(end_time);  //结束时间int 为0表示	无过期日

PropertyString(type); //任务类型 string 分别对应签到，完善信息，商家任务（跳转url）sign|info|extend

PropertyInt(join_count);//有多少人参加

//available|complete|soldout
PropertyString(status); //任务对于该用户的状态 可以做已完成|该任务份数已被用完available


@property (nonatomic, copy) NSString<Optional> *task_url;

@end
