//
//  ProductDetailMD.h
//  wft
//
//  Created by JSen on 14/10/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"
@class biz;

@interface ProductDetailMD : JSONModel


PropertyString(pid);
PropertyString(title);
PropertyInt(original_price);
PropertyInt(sale_price);
PropertyInt(score_max);

@property (nonatomic, copy) NSString<Optional> *avatar;
PropertyInt(sold_count);
PropertyInt(in_stock);
PropertyInt(limit_user);
PropertyInt(end_time);
PropertyInt(earn_money);
PropertyInt(return_rate);
PropertyString(biz_id);

//商户名
PropertyString(biz_name);

@property (nonatomic, strong) NSArray<ConvertOnDemand> *photos ;
PropertyString(info);

@property (nonatomic, copy) NSString<ConvertOnDemand> *notes;
PropertyInt(like_count);
PropertyInt(comment_count);
PropertyInt(start_time);

@property (nonatomic, strong) biz *biz;

PropertyInt(return_score);

@property (nonatomic, assign) NSInteger market_price;
@property (nonatomic, assign) BOOL display_stock;//是否显示库存信息



@end

@interface biz : JSONModel

@property (nonatomic, copy) NSString<Optional> *tel ;
@property (nonatomic, copy) NSString<Optional> * name;
@property (nonatomic, copy) NSString<Optional> *address;

@property (nonatomic, retain) NSArray<ConvertOnDemand> *redeem_time;

@end



