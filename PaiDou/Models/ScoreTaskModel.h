//
//  ScoreTaskModel.h
//  wft
//
//  Created by JSen on 14/10/28.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//
/*
 */
#import <Foundation/Foundation.h>
#import "JSONModelLib.h"
@interface ScoreTaskModel : JSONModel

@property (nonatomic, copy) NSString<Optional> *data;
@property (nonatomic, assign) NSInteger score;
@property (nonatomic, copy) NSString  *status;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *type2;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *user_task_id;
@end

/*
 {
 count = 4;
 "err_code" = 0;
 "err_msg" = Success;
 lists =     (
 {
 data = "<null>";
 score = 15;
 status = tobedone;
 type = daily;
 type2 = "scan_biz";
 "user_id" = U72DBCC00C8670ABEF8C5A9B0D576A9A0;
 "user_task_id" = UT88F8857086CDFAA86C45F982962027A3;
 },
 {
 data = "<null>";
 score = 15;
 status = tobedone;
 type = daily;
 type2 = "sign_in";
 "user_id" = U72DBCC00C8670ABEF8C5A9B0D576A9A0;
 "user_task_id" = UTC94E5E2E56C56869FBD1FFE5C0011B16;
 },
 {
 data = "<null>";
 score = 15;
 status = tobedone;
 type = specified;
 type2 = "share_friends";
 "user_id" = U72DBCC00C8670ABEF8C5A9B0D576A9A0;
 "user_task_id" = UTF851EE052A69B74A319F453EF195B81A;
 },
 {
 data = "<null>";
 score = 15;
 status = finished;
 type = specified;
 type2 = register;
 "user_id" = U72DBCC00C8670ABEF8C5A9B0D576A9A0;
 "user_task_id" = UTA6D13A834596D24A87C6128E343A41C4;
 }
 );
 }
 */

@interface ScoreListMode : JSONModel

@property (nonatomic, copy) NSString<Optional> *bid;
@property (nonatomic, copy) NSString<Optional> *biz_name;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) NSInteger create_time;
@property (nonatomic, copy) NSString *event;
@property (nonatomic, copy) NSString<Optional> *pid;
@property (nonatomic, copy) NSString<Optional> *product_name;
@property (nonatomic, assign) NSInteger score;
@property (nonatomic, copy) NSString *uid;

@end