//
//  OrderSummaryMD.h
//  wft
//
//  Created by JSen on 14/10/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"

@interface OrderSummaryMD : JSONModel
PropertyString(oid);    //服务器端生成的每个订单唯一id
PropertyString(uid);    //下单的用户id
PropertyString(bid);    //该产品所属的商户id
   //产品的id
@property (nonatomic, copy) NSString<Optional> *pid;
PropertyString(type);   //scene|cash|score|mixed
PropertyString(name);   //产品名

PropertyInt(unit_price);   //下单时该产品的实际购买单价。单位分。
PropertyInt(unit_score);    //产品需要的积分
PropertyInt(count);        //用户购买的数量。
PropertyInt(total_price);  //用户本单总价 = unit_price * count。单位分。
PropertyInt(total_score);   //
PropertyInt(create_time);  //下单时间（创建订单的时间）。
PropertyString(status);

@property (nonatomic, copy) NSString<Optional> *avatar;
@property (nonatomic, copy) NSString<Optional> *memo;
@property (nonatomic, copy) NSString<Optional> *redeem_end_time;
@property (nonatomic, copy) NSString<Optional> *redeem_time;

@property (nonatomic, copy) NSString<Optional> *pay_channel;
@property (nonatomic, copy) NSString<Optional> *biz_name;

@property (nonatomic, assign) NSInteger award_score;
@end
/*
 oid”:”xxxxx”,
 “uid”:”xxxx”,
 “bid”:”xxxxx”,
 “type”:”scene|cash|score”,
 “pid”:”xxxxx”,
 “name”:”xxxxxx”,
 “avtar”:”http://xxxxxx”,
 “unit_price”:1000,
 “count”:2,
 “total_price”: 2000,
 “create_time”:12345678,
 “status”:”xxxx”,
 
 status 解释
 一个订单只能处于以下状态中的一个：
 wait_for_pay （未付款）刚创建的订单
 payed （已付款）用户完成支付
 apply_for_refund用户申请退款
 refunding （退款受理中）我方工作人员处理中
 refunded （已退款）已完成退款
 consuming（部分消费）一个订单有多个产品时，用户可能暂时只使用一部分验证码。
 consumed （完成）用户付款完成并在商户处完成消费。
 expired_to_refund（超期退款）用户付款后长时间不去消费，超过服务期限。自动转退款处理。
 deled 用户从自己的订单‘可视’列表中删除。
 以下各个字段的时间值，初始默认都是0，当响应状态变化时，记录变化发生的时间。
 
 
 "order_brief_info" =     {
 avatar = "<null>";
 bid = B3A8C4BEFE68496A521564C74B1110E43;
 count = 0;
 "create_time" = 1416561748;
 memo = "<null>";
 name = "\U6d3e\U8c46\U626b\U7801\U652f\U4ed8\U4ea7\U54c1";
 oid = EB21617482971126;
 "pay_channel" = "<null>";
 "pay_notify_id" = "<null>";
 "pay_time" = "<null>";
 pid = "<null>";
 "redeem_end_time" = "<null>";
 "redeem_time" = "<null>";
 status = "wait_for_pay";
 "total_price" = 1;
 "total_score" = 0;
 type = scene;
 uid = U720B2E51C9F7856A6AEB689BA5EC614A;
 "unit_price" = 0;
 "unit_score" = 0;
 };
 }
 */