//
//  OrderModel.h
//  wft
//
//  Created by JSen on 14/9/28.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModelLib.h"
#import "UserVerifyCode.h"

@protocol  UserVerifyCode <NSObject>


@end

@interface OrderModel : JSONModel

PropertyString(oid);    //服务器端生成的每个订单唯一id
PropertyString(uid);    //下单的用户id
PropertyString(bid);    //该产品所属的商户id
PropertyString(pid);    //产品的id
PropertyString(name);   //产品名

PropertyInt(unit_price);   //下单时该产品的实际购买单价。单位分。
PropertyInt(unit_score);    //产品需要的积分
PropertyInt(count);        //用户购买的数量。
PropertyInt(total_price);  //用户本单总价 = unit_price * count。单位分。
PropertyInt(total_score);   //
PropertyInt(create_time);  //下单时间（创建订单的时间）。

/*
 scene：没有产品，当面直接支付金额的订单；这种情况下后面的pid,name,avtar,unit_price,count字段都为空。只有total_price总金额。
 cash：通过网络支付购买商品的订单；
 score：通过积分兑换的订单
 */
PropertyString(type);
/*
 一个订单只能处于以下状态中的一个：
 wait_for_pay （未付款）刚创建的订单
 payed （已付款）用户完成支付
 refunding （等待退款）用户申请退款中
 refunded （已退款）已完成退款
 consumed （完成）用户付款完成并在商户处完成消费。
 deled 用户从自己的订单‘可视’列表中删除。
 以下各个字段的时间值，初始默认都是0，当响应状态变化时，记录变化发生的时间。
 */
PropertyString(status);

/*
 验证码，默认空。当用户支付完成，给该订单生成一个全局唯一的验证码（建议12位数字）。用户在商户出示该码证明付款完成。同时商户通过后台验证该码将订单标为已消费（consumed）。
 */
//PropertyString(verify_codes);    //
@property (nonatomic, strong) NSArray<UserVerifyCode> *verify_codes;


PropertyInt(pay_time); //用户完成支付时间。收到支付服务器通知的时间。默认值0。
PropertyString(pay_channel);    //支付方式。默认空，alipay支付宝；wxpay微信支付；等。

/*
 第三方支付服务器通知支付结果的消息id，默认空。服务器记录所有支付通知消息的日志id，我们可通过该id核查数据。
 */
PropertyString(pay_notify_id);


@property (nonatomic, copy) NSString<Optional> *avatar;
@property (nonatomic, copy) NSString<Optional> *memo;
@property (nonatomic, copy) NSString<Optional> *redeem_end_time;
@property (nonatomic, copy) NSString<Optional> *redeem_time;

@property (nonatomic, copy) NSString<Optional> *biz_name;

@property (nonatomic, assign) NSInteger award_score;

@end
