//
//  SellerSummaryMD.h
//  wft
//
//  Created by JSen on 14/10/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"

@interface SellerSummaryMD : JSONModel

PropertyString(address);
PropertyString(avatar_url);
PropertyInt(avg_price);
PropertyString(bid);
PropertyString(brief);
PropertyString(name);
PropertyInt(products_count);
PropertyInt(support_scancode);
@end
