//
//  UserVerifyCode.h
//  wft
//
//  Created by JSen on 14/10/24.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"

@interface UserVerifyCode : JSONModel

PropertyString(product_name);
PropertyString(code);
PropertyString(status);

@property (nonatomic, copy) NSString<Optional> *oid;
@property (nonatomic, copy) NSString<Optional> *avatar;
@property (nonatomic, copy) NSString<Optional> *biz_name;
@property (nonatomic, retain) NSNumber<Optional> *consume_time;
@property (nonatomic, retain) NSNumber<Optional> *redeem_end_time;

@end
/*
 
 avatar = "http://paidou-media.stor.sinaapp.com/2014/12/1417414815-M13_.jpg";
 "biz_name" = "<null>";
 code = 281422902641;
 "consume_time" = 0;
 oid = EC11814221701294;
 "product_name" = "\U80af\U5fb7\U57fa6\U5143\U65e9\U9910";
 "redeem_end_time" = 1418140800;
 status = unconsumed;
 
 
 public String product_name;
 
 public String code;
 
 public String status;
 
 public int redeem_end_time;
 
 public String oid;
 
 public String avatar;
 
 public String biz_name;
 
 public int consume_time;
 
 */