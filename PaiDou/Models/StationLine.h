//
//  StationLine.h
//  PaiDou
//
//  Created by JSen on 14/12/15.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"


@protocol Station <NSObject>


@end

@interface StationLine : JSONModel

@property (assign) NSInteger line_id;

@property (copy) NSString *line_name;

@property (retain) NSArray<Station> *station_list;

@end

@interface Station : JSONModel

@property (assign) NSInteger station_id;
@property (copy) NSString *station_name;
@property (copy) NSString *latitude;
@property (copy) NSString *longitude;

@end


