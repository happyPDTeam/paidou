//
//  MessageModel.h
//  PaiDou
//
//  Created by JSen on 14/12/26.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "JSONModel.h"

@interface MessageModel : JSONModel

@property (copy) NSString<Optional> *send_to;
@property (copy) NSString<Optional> *title;
@property (copy) NSString<Optional> *content;
@property (assign) NSInteger create_time;

- (CGFloat)getHeight:(MessageModel *)model;

@end
