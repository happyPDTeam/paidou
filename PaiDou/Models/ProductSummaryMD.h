//
//  ProductSummaryMD.h
//  wft
//
//  Created by JSen on 14/10/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//


#import "JSONModel.h"

@protocol  ProductSummaryMD

@end

@interface ProductSummaryMD : JSONModel


PropertyString(pid);
PropertyString(title);
PropertyString(biz_name);
PropertyInt(original_price);
PropertyInt(sale_price);
PropertyInt(score_max);

@property (nonatomic, copy) NSString<Optional> *avatar;
PropertyInt(sold_count);
PropertyInt(in_stock);
PropertyInt(end_time);
PropertyInt(earn_money);
PropertyInt(return_rate);
PropertyInt(return_score);
PropertyInt(comment_count);
PropertyInt(like_count);

//
@property (nonatomic, strong) NSArray<Optional> *photos ;

@property (nonatomic, assign) NSNumber<Optional> *start_time;

@property (nonatomic, assign) NSInteger market_price;
@property (nonatomic, assign) NSInteger limit_user;
@end

/*
 public String pid; // 产品的唯一id。
 
 public String title; // 商品名字
 
 public int original_price; // 原价，单位分
 
 public int sale_price; // 售价，单位分
 
 public int score_max; // 最高豆币数
 
 public String avatar; // 用在产品列表中的图片
 
 public int sold_count; // 已兑换个数
 
 public int in_stock; // 可赚
 
 public long end_time; // 有效期
 
 public int earn_money; // 可赚钱，单位分
 
 public int return_rate; // 收益回报
 
 
 
 {
 avatar = "http://paidou-media.stor.sinaapp.com/test.jpg";
 "comment_count" = 9;
 "earn_money" = 100;
 "end_time" = 1415949319;
 "in_stock" = 86;
 "like_count" = 0;
 "limit_user" = 50;
 "original_price" = 1;
 pid = PB3D22ACEA2DCDEA7F6E7F894485F4CT6;
 "return_rate" = 10000;
 "return_score" = 1;
 "sale_price" = 1;
 "score_max" = 0;
 "sold_count" = 14;
 title = "\U652f\U4ed8\U6d4b\U8bd5\U5546\U54c1";
 },
 
 */