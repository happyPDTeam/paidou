//
//  UIColor+AppSetting.m
//  wft
//
//  Created by JSen on 14/10/8.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "UIColor+AppSetting.h"

@implementation UIColor (AppSetting)


+ (UIColor *)appBackgroundColor {
    return UIColorFromRBGString(@"0xf2f2f2");
}

+ (UIColor *)appBlackTextColor {
    return UIColorFromRGB(0x333333);
}

+ (UIColor *)appRedTextColor {
    return UIColorFromRGB(0xff2f2f);
}

+ (UIColor *)appGrayTextColor{
    return UIColorFromRGB(0x616161);
}

+ (UIColor *)appBlueTextColor {
    return UIColorFromRGB(0x00c7c0);
}

//f0ff01
+ (UIColor *)appYellowTextColor {
    return UIColorFromRGB(0xf0ff01);
}
//FF6701
+ (UIColor *)appOrangeColor {
    return UIColorFromRGB(0xFF6701);
}

+ (UIColor *)appWihteColor{
    return [UIColor whiteColor];
}
//21c501
+ (UIColor *)appGreenColor{
    return UIColorFromRGB(0x21c501);
}

+(UIColor *)appGrayColor
{
    return UIColorFromRGB(0x999999);
}

+(UIColor *)appShallowOrangeColor
{
    return UIColorFromRGB(0xff9e73);
}

+(UIColor *)appLightGrayColor
{
    return UIColorFromRGB(0x666666);
}

+(UIColor *)appLineColor
{
    return UIColorFromRGB(0xcdcdcd);
}

+(UIColor *)appTableTitleColor
{
    return UIColorFromRGB(0x4b4a4a);
}

+(UIColor *)app6a6a6aColor
{
    return UIColorFromRGB(0x6a6a6a);
}

+(UIColor *)app797979Color
{
    return UIColorFromRGB(0x797979);
}

+(UIColor *)app5d5d5dColor
{
    return UIColorFromRGB(0x5d5d5d);
}

+(UIColor *)app7e7e7eColor
{
    return UIColorFromRGB(0x7e7e7e);
}

+(UIColor *)appfe3c00Color
{
    return UIColorFromRGB(0xfe3c00);
}

+(UIColor *)appb6b6b6Color
{
    return UIColorFromRGB(0xb6b6b6);
}

@end