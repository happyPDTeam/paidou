//
//  UIColor+AppSetting.h
//  wft
//
//  Created by JSen on 14/10/8.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AppSetting)

+ (UIColor *)appBackgroundColor ;

+ (UIColor *)appBlackTextColor ;

+ (UIColor *)appRedTextColor;

+ (UIColor *)appGrayTextColor;

+ (UIColor *)appBlueTextColor;

+ (UIColor *)appYellowTextColor;

+ (UIColor *)appOrangeColor ;

+ (UIColor *)appWihteColor;

+ (UIColor *)appGreenColor;

+(UIColor *)appGrayColor;

+(UIColor *)appShallowOrangeColor;

+(UIColor *)appLightGrayColor;

+(UIColor *)appLineColor;

+(UIColor *)appTableTitleColor;

+(UIColor *)app6a6a6aColor;

+(UIColor *)app797979Color;

+(UIColor *)app5d5d5dColor;

+(UIColor *)app7e7e7eColor;

+(UIColor *)appfe3c00Color;

+(UIColor *)appb6b6b6Color;

@end