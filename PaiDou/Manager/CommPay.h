//
//  CommPay.h
//  wft
//
//  Created by JSen on 14/11/6.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommPay : NSObject{
    SEL _resultSEL;
}

+(instancetype)sharedInstance;

+(void)payWithUID:(NSString *)uid OID:(NSString *)oid payChannel:(NSString*)payChannel;

+ (void)wxpay:(NSDictionary *)payParams;
@end
