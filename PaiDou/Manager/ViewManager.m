//
//  ViewManager.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ViewManager.h"

@implementation ViewManager

+(UIButton *)createButton:(CGRect)frame
                  bgImage:(NSString *)bgImageName
                    image:(NSString *)imageName
                    title:(NSString *)title
                   method:(SEL)method
                   target:(id)target;
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = frame;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button addTarget:target action:method forControlEvents:UIControlEventTouchUpInside];
    
    [button setBackgroundImage:[UIImage JSenImageNamed:bgImageName] forState:UIControlStateNormal];
    
    [button setImage:[UIImage JSenImageNamed:imageName] forState:UIControlStateNormal];
    
    return button;
}

+(UILabel *)createLabel:(CGRect)frame
                  title:(NSString *)title
                   font:(int)font
              textColor:(UIColor *)textColor
{
    UILabel * label = [[UILabel alloc] initWithFrame:frame];
    
    label.font = [UIFont systemFontOfSize:font];
    
    label.text = title;
    	
    label.textColor = textColor;
    
    return label;
}

-(UITableView *)createTableView:(CGRect)frame
                       deleagte:(id)delegate
                     dateSource:(id)dateSource
                          style:(UITableViewStyle)style
{
    UITableView * tableView = [[UITableView alloc] initWithFrame:frame style:style];
    
    tableView.delegate = delegate;
    
    tableView.dataSource = dateSource;
    
    return tableView;
}

+(UITextField *)createTextField:(CGRect)frame
                       delegate:(id)delegate
                    palceHolder:(NSString *)palceHolder
{
    UITextField * textField = [[UITextField alloc] initWithFrame:frame];
    
    textField.delegate = delegate;
    
    textField.placeholder = palceHolder;
    
    return textField;
}

+(UIImageView *)createImageView:(CGRect)frame imageName:(NSString *)imageName
{
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:frame];
    
    [imageView setImage:[UIImage imageNamed:imageName]];
    
    return imageView;
}

@end
