//
//  LocationMgr.m
//  PaiDou
//
//  Created by JSen on 14/12/17.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "LocationMgr.h"
#import "BMapKit.h"
#import "StationLine.h"

@implementation LocationMgr

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _locService = [[BMKLocationService alloc]init];
        _locService.delegate = self;
    }
    return self;
}

+ (instancetype) shared{
    static id _s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _s = [[self alloc] init];
    });
    return _s;
}


- (void)updateLocation{
    if ([CLLocationManager locationServicesEnabled]) {
        
        _locManager = [[CLLocationManager alloc] init];
        _locManager.delegate = self;
        
        //The desired location accuracy.
        _locManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        //Specifies the minimum update distance in meters.
        _locManager.distanceFilter = kCLDistanceFilterNone;
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"您尚未开启定位服务" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations{
    NSLog(@"%@",locations);
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"didChangeAuthorizationStatus---%u",status);
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"didChangeAuthorizationStatus----%@",error);
}

-(void)startLocate{
    [self needLocate];
    
    
//    if (_timer) {
//        [_timer invalidate];
//        _timer = nil;
//    }
//    
//    _timer = [NSTimer scheduledTimerWithTimeInterval:10  target:self selector:@selector(needLocate) userInfo:nil repeats:YES];
    
}

- (void)needLocate{
  [_locService startUserLocationService];
}

- (void)stopLocate{
    [_locService stopUserLocationService];
}
//MARK: baidu
/**
 *在图View将要启动定位时，会调用此函数
 *@param mapView 地图View
 */
- (void)willStartLocatingUser
{
    NSLog(@"start locate");
}

/**
 *用户方向更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation
{
   
    NSLog(@"heading is %@",userLocation.heading);
}

/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateUserLocation:(BMKUserLocation *)userLocation
{
    NSLog(@"didUpdateUserLocation lat %f,long %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
    
    [self tellServerLocation:userLocation];
    
    [self caculNearestStation:userLocation];
   
    [self stopLocate];
}

- (void)tellServerLocation:(BMKUserLocation *)userLocation{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSString *uid = [[UserManager sharedManager]uid];
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setValue:uid forKey:@"uid"];
    [param setValue:DEV_UUID forKey:@"device_id"];
    [param setValue:@(userLocation.location.coordinate.latitude) forKey:@"latitude"];
     [param setValue:@(userLocation.location.coordinate.longitude) forKey:@"longitude"];
    
    [manager POST:kAddUserLocationUrl parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"err_code"] intValue] == 0) {
            NSLog(@"保存用户地理位置信息成功");
        }else{
            NSLog(@"保存用户地理位置信息失败");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"保存用户地理位置信息失败");
    }];
}

- (void)caculNearestStation:(BMKUserLocation *)userLocation{
    if([[NSFileManager defaultManager] fileExistsAtPath:kMetroLinePath]){
        NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:kMetroLinePath];
        
        int m = 0, n = 0;
        for (int i = 0; i < array.count ; i++) {
            StationLine *line = array[i];
            
           double minDis = 0;
            for (int j = 0; j < line.station_list.count; j++) {
                Station *oneStation = line.station_list[j];
                double oneLa = [oneStation.latitude doubleValue];
                double oneLon = [oneStation.longitude doubleValue];
                
                double distance = [Utils distanceBetweenOrderBy:userLocation.location.coordinate.latitude :userLocation.location.coordinate.longitude :oneLa :oneLon];
                if (0 == minDis) {
                    minDis = distance;
                }else if(minDis >= distance){
                    minDis = distance;
                    m = i;
                    n = j;
                }
                
            }
        }
        StationLine *nearLine = array[m];
        Station *nearestStation = nearLine.station_list[n];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyFoundNearestStation object:nil userInfo:@{@"S":nearestStation}];
    }
    
}

/**
 *在地图View停止定位后，会调用此函数
 *@param mapView 地图View
 */
- (void)didStopLocatingUser
{
    NSLog(@"stop locate");
}

/**
 *定位失败后，会调用此函数
 *@param mapView 地图View
 *@param error 错误号，参考CLError.h中定义的错误号
 */
- (void)didFailToLocateUserWithError:(NSError *)error
{
    NSLog(@"location error");
}



@end
