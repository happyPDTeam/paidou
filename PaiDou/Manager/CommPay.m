//
//  CommPay.m
//  wft
//
//  Created by JSen on 14/11/6.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "CommPay.h"
#import "AFHTTPSessionManager.h"
#import "WXApi.h"
#import "WXApiObject.h"
#import "AlixLibService.h"
#import "MD5Util.h"
#import "AlixPayResult.h"
#import "DataSigner.h"
#import "DataVerifier.h"
#import "PartnerConfig.h"
#import "DataSigner.h"
#import "DataVerifier.h"

//支付宝公钥
#define AlipayPubKey   @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB"

@implementation CommPay
+(instancetype)sharedInstance{
    static id _s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _s = [[self alloc]init];
    });
    return _s;
}

- (instancetype)init{
    if (self = [super init]) {
        _resultSEL = @selector(paymentResult:);
    }
    return self;
}

-(void)paymentResultDelegate:(NSString *)result {
    [self paymentResult:result];
}
- (void)paymentResult:(NSString *)resultd {
    AlixPayResult *result  = [[AlixPayResult alloc] initWithString:resultd];
    
    if (result) {
        
        if (result.statusCode == 9000) {
            NSString *key = AlipayPubKey;
            id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(key);
            
            if ([verifier verifyString:result.resultString withSign:result.signString]) {
                NSLog(@"交易成功");
              //
                [[NSNotificationCenter defaultCenter] postNotificationName:kSafePaySuccessNotificatino object:nil];
            }
        }else {
            NSLog(@"交易失败");
            [[NSNotificationCenter defaultCenter] postNotificationName:kSafePayFailedNotification object:nil];
        }
        
    }else {
        NSLog(@"交易失败");
        [[NSNotificationCenter defaultCenter] postNotificationName:kSafePayFailedNotification object:nil];
    }
    
}

//
+(void)payWithUID:(NSString *)uid OID:(NSString *)oid payChannel:(NSString*)payChannel {
    if (!uid.length || !oid.length || !payChannel.length) {
        return;
    }
    
    AppDelegate *del = [UIApplication sharedApplication].delegate;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:del.window animated:YES];
    hud.labelText = @"正在联系服务器支付...";
    hud.minShowTime  = 1;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *sessionMgr = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:configuration];
    sessionMgr.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    NSDictionary *params = @{
                             @"uid":uid,
                             @"oid":oid,
                             @"pay_channel":payChannel
                             };
    NSURLSessionDataTask *dataTask = [sessionMgr GET:kGetPayParam parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        hud.hidden =YES;
        if ([responseObject[@"err_code"] intValue] == 0) {
            
            if ([payChannel isEqualToString:@"alipay"]) {
                
                [[self class] alipay:responseObject[@"alipay_params"]];
            }else if ([payChannel isEqualToString:@"wxpay"]) {
                NSDictionary *wxpayParam = responseObject[@"wxpay_params"];
                [[self class]  wxpay:wxpayParam];
            }
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
         hud.hidden =YES;
    }];
    [dataTask resume];

}

- (SEL)getSelector{
    return _resultSEL;
}

#pragma mark - ali pay
+(void)alipay:(NSString *)alipayInfo{
    
    NSString *signedStr = [[CommPay sharedInstance] doRsa:alipayInfo];
    
    NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                             alipayInfo, signedStr, @"RSA"];
    [AlixLibService payOrder:orderString AndScheme:kAliSchema seletor:[[CommPay sharedInstance] getSelector] target:[CommPay sharedInstance]];
}

-(NSString*)doRsa:(NSString*)orderInfo
{
    // 获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}
#pragma mark - wx pay
+ (void)wxpay:(NSDictionary *)payParams{
     BOOL isok = [WXApi registerApp:kWXAppID];
    
    PayReq *request = [[PayReq alloc] init];
    request.partnerId = payParams[@"wxpay_partnerid"];
    request.prepayId = payParams[@"wxpay_prepareid"];
    request.package =  @"Sign=WXPay";
    request.sign = payParams[@"wxpay_sign"];
    request.nonceStr = payParams[@"wxpay_noncestr"];
    request.timeStamp = [payParams[@"wxpay_timestamp"] integerValue];
    
    if ([WXApi safeSendReq:request]) {
        NSLog(@"启动微信支付成功");
    }else{
        NSLog(@"启动微信支付失败");
    }
}

+ (UInt32)getTimeStamp{
    return [NSDate timeIntervalSinceReferenceDate];
}

+ (NSString *)getRandomStr{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:ss"];
    NSString *dateStr = [formatter stringFromDate:[NSDate date] ];
    NSString *newDateStr = [NSString stringWithFormat:@"%@%d",dateStr,arc4random()%1000000];
    return [MD5Util md5Encrypt:newDateStr];
}
@end
