//
//  LocationMgr.h
//  PaiDou
//
//  Created by JSen on 14/12/17.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BMapKit.h"
#import <MapKit/MapKit.h>

@interface LocationMgr : NSObject<CLLocationManagerDelegate,BMKLocationServiceDelegate>{
    NSString * _currentLatitude;
    NSString * _currentLongitude;
    CLLocationManager *_locManager;
    
    
    BMKLocationService* _locService;
    
    NSTimer *_timer;
}
+ (instancetype) shared;
//- (void)updateLocation;
-(void)startLocate;

@end
