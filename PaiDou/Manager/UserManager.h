//
//  UserManager.h
//  wft
//
//  Created by JSen on 14/9/28.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

typedef void(^sb)(BOOL isOk);

@interface UserManager : NSObject{
    User *_currUser;
    
}
@property (nonatomic, assign) BOOL hasLogin;//登录
@property (nonatomic, assign) BOOL hasSignin;//签到

@property (assign) NSInteger appBadge;

+ (instancetype)sharedManager;

- (void)getUserInfo;

- (void) setLoginUser:(User *)m;

- (void)saveToDisk:(User *)m;

- (User *) readFromDisk;

- (void)autoLogin ;
- (NSString *)uid ;

- (NSInteger)score;

//sign in 签到
- (void)signIn:(void(^)(BOOL isSuccess,int score))block ;
//能否点击签到24h为期
- (BOOL)canSignin ;

- (void)decreaseDou:(NSInteger)dou;

- (void)update:(NSDictionary *)dict success:(sb)block;

- (void)updatePass:(NSDictionary *)dict ;

//登出
- (void)logOff;


- (void)showLoginAlertCancel:(void(^)())cancelBlock sure:(void(^)())sureBlock;
@end
