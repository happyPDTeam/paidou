//
//  UserManager.m
//  wft
//
//  Created by JSen on 14/9/28.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "UserManager.h"
#import "User.h"
#import "MD5Util.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "MiPushSDK.h"

//最近一次签到时间
static NSString *const kLattestSignInTime = @"lattest sing in time";

@implementation UserManager

+ (instancetype)sharedManager {
    static UserManager *_s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _s = [[self alloc]init];
        // 添加通知添加支付成功处置
        [[NSNotificationCenter defaultCenter] addObserver:_s selector:@selector(updateUserInfo) name:kSafePaySuccessNotificatino object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:_s selector:@selector(updateUserInfo) name:kNotifyNeedReloadTaskList object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:_s selector:@selector(updateUserInfo) name:kNotifyNeedUpdateUserInfo object:nil];
    });
    return _s;
}
#pragma mark - 支付完成更新用户信息,从任务
- (void)updateUserInfo{
    [[UserManager sharedManager] _updateUserInfo:_currUser];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (void)getUserInfo {
    
}

- (NSString*) getCurrUserFile{
    return [NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),@"/Library/Caches/CurrUser"];
}

- (void) setLoginUser:(User *)m{
    _currUser = m;
    [self saveToDisk:m];
}

- (void)saveToDisk:(User *)m{
    if (_currUser != m) _currUser = m;
    
    //为推送设置别名
    [MiPushSDK setAlias:m.uid];
    [MiPushSDK subscribe:@"client"];
    
    NSString *f = [self getCurrUserFile];
    NSLog(@"current user file path %@",f);
    BOOL isOk = [NSKeyedArchiver archiveRootObject:m toFile:f];
    if (!isOk) {
        NSLog(@"write to file %@ fail ",f);
    }else{
        NSLog(@"write to file %@ success !",f);
    }
}

- (User *) readFromDisk{
    NSString *f = [self getCurrUserFile];
    if(![[NSFileManager defaultManager]fileExistsAtPath:f])
    {
        //bu cun zai
//        _currUser = [[User alloc]init];
//        return _currUser;
        return nil;
    }
    NSMutableData *data = [NSMutableData dataWithContentsOfFile:[self getCurrUserFile]];
    User *u = [NSKeyedUnarchiver unarchiveObjectWithData:data];
   
    
   // User *u = [NSKeyedUnarchiver unarchiveObjectWithFile:f];
    if(_currUser == nil) _currUser = u;
    return u;
}

- (void)autoLogin {
    User *user = [self readFromDisk];
    if (user) {
        _hasLogin = YES;
    }
    if (user) {
        [self _updateUserInfo:user];
    }
}
- (void)_updateUserInfo:(User *)user {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *dict = @{@"mobile":user.mobile,
                           @"password":[Utils processedPassWordString:user.passWord phoneNumber:user.mobile]
                           };
    [manager POST:kLoginURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@ %@",operation,responseObject);
        NSDictionary *resDict = (NSDictionary *)responseObject;
        if ([resDict[@"err_code"] intValue] == 0) {
            NSError *error;
            User *updatedUser = [[User alloc]initWithDictionary:responseObject[@"user"] error:&error];
         
            if (error) {
                NSLog(@"%@",error);
               // return ;
            }
            updatedUser.passWord = user.passWord;
            [[UserManager sharedManager] setLoginUser:updatedUser];
            [UserManager sharedManager].hasLogin  = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyLoginSuccess object:nil];
        }
       
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@ %@",operation,error);
   
    }];

}

- (NSString *)uid {
    return _currUser.uid.length ? _currUser.uid:@"";
}

- (NSInteger)score{
    return _currUser.score;
}
- (void)signIn:(void(^)(BOOL isSuccess,int score))block {
    if (!_hasLogin) {
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *param = @{
                            @"type":@"sign_in",
                            @"uid":[self uid],
                            @"data":@""
                   };
    [manager POST:kTaskComplete parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"err_code"] intValue] == 0) {
            int s = [responseObject[@"score"] intValue];
            User *user = [[UserManager sharedManager]readFromDisk];
            
            //save last sign in time
            [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:kLattestSignInTime];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            user.score += s;
            _hasSignin = YES;
            if (block) {
                block(YES,s);
            }
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        _hasSignin = NO;
        if (block) {
            block(NO,-1);
        }

    }];
    
}

- (BOOL)canSignin {
    NSDate *lastDate = [(NSDate *)[NSUserDefaults standardUserDefaults] valueForKey:kLattestSignInTime];
    if (!lastDate) {
        return YES;
    }
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    //check year
    [formatter setDateFormat:@"yyyy"];
    int lastYear = [[formatter stringFromDate:lastDate]intValue];
    int currYear = [[formatter stringFromDate:currentDate] intValue];
    if ((currYear - lastYear) >= 1) {
        return YES;
    }
    
    //check month
    [formatter setDateFormat:@"MM"];
    int lastMonth = [[formatter stringFromDate:lastDate] intValue];
    int currMonth = [[formatter stringFromDate:currentDate] intValue];
    if ((currMonth - lastMonth) >= 1) {
        return YES;
    }
    
    //check day
    [formatter setDateFormat:@"dd"];
    int lastDay = [[formatter stringFromDate:lastDate] intValue];
    int today = [[formatter stringFromDate:currentDate] intValue];
    if ((today - lastDay) >= 1){
        return YES;
    }
    
    
    return NO;
}

- (void)decreaseDou:(NSInteger)dou{
    User *user = [[UserManager sharedManager] readFromDisk];
    user.score = user.score - dou;
    [[UserManager sharedManager] setLoginUser:user];
}

- (void)update:(NSDictionary *)dict success:(sb)block{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
     AppDelegate *del = [[UIApplication sharedApplication] delegate];
     MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:del.window animated:YES];
    NSArray *keys = [dict allKeys];
    
    if ([keys containsObject:@"avatar"]) {
       
     
        
        [manager POST:kModifyUserInfo parameters:@{@"uid":dict[@"uid"]} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            NSTimeInterval interval = [NSDate timeIntervalSinceReferenceDate];
        
            NSString *name = [NSString stringWithFormat:@"headImage%f",interval];
            [formData appendPartWithFileData:dict[@"avatar"] name:@"avatar" fileName:name mimeType:@"image/jpg"];
            
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@",responseObject);
            [hud hide:YES];
            if ([responseObject[@"err_code"]intValue] == 0  ) {
                
                [self showHUD:@"修改成功"];
               
                [self updateInfo:responseObject];
                if (block) {
                    block(YES);
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [hud hide:YES];
            [self showHUD:@"修改失败"];
            NSLog(@"%@",error);
            if (block) {
                block(NO);
            }
        }];
        return;
    }
    
    
    
    [manager POST:kModifyUserInfo parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
         [hud hide:YES];
        if ([responseObject[@"err_code"]intValue] == 0  ) {
           
            [self showHUD:@"修改成功"];
            if (block) {
                block(YES);
            }
            [self updateInfo:responseObject];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [hud hide:YES];
         [self showHUD:@"修改失败"];
        if (block) {
            block(NO);
        }
        NSLog(@"%@",error);
    }];
}
//最新信息重新写入文件
- (void)updateInfo:(NSDictionary *)responseObject{
    NSError *error;
    User *updatedUser = [[User alloc]initWithDictionary:responseObject[@"user"] error:&error];
    if (error) {
        NSLog(@"%@",error);
    }else{
        updatedUser.passWord = _currUser.passWord;
        [[UserManager sharedManager] setLoginUser:updatedUser];
        [UserManager sharedManager].hasLogin  = YES;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyLoginSuccess object:nil];
}

- (void)showHUD:(NSString *)text{
    AppDelegate *del = [UIApplication sharedApplication].delegate;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:del.window animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = text;
    hud.userInteractionEnabled = NO;
    hud.minShowTime = 1.0f;
    [hud hide:YES afterDelay:1.0f];
    hud.margin = 10;
}

- (void)updatePass:(NSDictionary *)dict{
    NSArray *keys = [dict allKeys];
    if (![keys containsObject:@"new_password"]) {
        return;
    }
    NSString *old = [Utils processedPassWordString:dict[@"old_password"] phoneNumber:_currUser.mobile];
    NSString *new = [Utils processedPassWordString:dict[@"new_password"] phoneNumber:_currUser.mobile];
    NSDictionary *param = @{
                            @"uid":_currUser.uid,
                            @"old_password":old,
                            @"new_password":new
                            };
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:kChangePass parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"err_code"]intValue] == 0  ) {
            [self showHUD:@"修改密码成功"];
            
            _currUser.passWord = dict[@"new_password"];
            [self setLoginUser:_currUser];
//            [self updatePassWord:dict[@"new_password"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self showHUD:@"修改密码失败"];
        NSLog(@"%@",error);
    }];
}

//- (void)updatePassWord:(NSString *)pass{
//    NSMutableData *data = [NSMutableData dataWithContentsOfFile:[self getCurrUserFile]];
//    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc ]initForWritingWithMutableData:data];
//   // [archiver encodeObject:pass forKey:@"password"];
//    [archiver setValue:pass forKey:@"password"];
//    [archiver finishEncoding];
//    NSError *error;
//    [NSKeyedArchiver archiveRootObject:<#(id)#> toFile:<#(NSString *)#>]
//   BOOL isok  = [data writeToFile:[self getCurrUserFile] options:NSDataWritingAtomic error:&error];
//    isok? NSLog(@"修改密码写入成功"):NSLog(@"写入新密码失败");
//}

- (void)logOff{
    NSString *path = [self getCurrUserFile];
    NSFileManager *manager = [NSFileManager defaultManager];
    NSError *error;
    if([manager removeItemAtPath:path error:&error]){
        NSLog(@"删除用户成功");
    }else{
        NSLog(@"删除失败 %@",error);
    }
    _hasLogin = NO;
    _hasSignin = NO;
    _currUser = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyLogoff object:nil];
}
- (void)showLoginAlertCancel:(void(^)())cancelBlock sure:(void(^)())sureBlock{
    RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消" action:cancelBlock];
    RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:sureBlock];
    UIAlertView *alert = [[UIAlertView alloc ]initWithTitle:@"温馨提示" message:@"您尚未登录，请先登录" cancelButtonItem:cancel otherButtonItems:sure, nil];
    [alert show];
}



@end
