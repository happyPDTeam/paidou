//
//  ViewManager.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewManager : NSObject

+(UIButton *)createButton:(CGRect)frame
                  bgImage:(NSString *)bgImageName
                    image:(NSString *)imageName
                    title:(NSString *)title
                   method:(SEL)method
                   target:(id)target;

+(UILabel *)createLabel:(CGRect)frame
                  title:(NSString *)title
                   font:(int)font
              textColor:(UIColor *)textColor;

+(UITableView *)createTableView:(CGRect)frame
                       deleagte:(id)delegate
                     dateSource:(id)dateSource
                          style:(UITableViewStyle)style;
+(UITextField *)createTextField:(CGRect)frame
                       delegate:(id)delegate
                    palceHolder:(NSString *)palceHolder;

+(UIImageView *)createImageView:(CGRect)frame
                      imageName:(NSString *)imageName;

@end
