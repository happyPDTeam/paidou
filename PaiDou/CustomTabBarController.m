//
//  CustomTabBarController.m
//  PaiDou
//
//  Created by JSen on 14/11/5.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "CustomTabBarController.h"
#import "UIImageSize.h"
#import "BaseNavigationController.h"
#import  "ScanQRCodeVC.h"
#import "QRPayViewController.h"


@interface CustomTabBarController (){
    ScanQRCodeVC *_scanvc;
}

@end

@implementation CustomTabBarController

- (void)loadViewControllersWithInfoDictionary:(NSDictionary *)firstDict,... NS_REQUIRES_NIL_TERMINATION{
    va_list args;
    va_start(args, firstDict);
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:firstDict];
    id arg = nil;
   
    while ((arg = va_arg(args, id))){
        if ([arg isKindOfClass:[NSDictionary class]]) {
            [array addObject:arg];
        }
    }
    va_end(args);
    if (array.count == 0) {
        return;
    }
    
    [self setViewControllers:array];
    
}

- (void)setViewControllers:(NSArray *)array{
    
    NSMutableArray *vcs = [NSMutableArray array];
    for (NSDictionary *oneDict in array) {
        NSString *clsName = oneDict[kTabClassName];
        Class cls = NSClassFromString(clsName);
        
        UIViewController *vc = [[cls alloc] init];
        BaseNavigationController *nav = [[BaseNavigationController alloc]initWithRootViewController:vc];
        nav.title = oneDict[kTabItemTitle];
        
        UIImage *normalImage = [[UIImage imageNamed:oneDict[kTabNormalImageName]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImage *highlightImage = [[UIImage imageNamed:oneDict[kTabHighlihtImageName]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UITabBarItem *item = [[UITabBarItem alloc]initWithTitle:oneDict[kTabItemTitle] image:normalImage selectedImage:highlightImage];
        
        NSDictionary *normalTitleAttributes = @{
                                          NSForegroundColorAttributeName:[UIColor appBlackTextColor],
                                          NSFontAttributeName:[UIFont systemFontOfSize:13],
                                        
                                          };
        NSDictionary *highlightTitleAttributes = @{
                                                NSForegroundColorAttributeName:[UIColor appBlueTextColor],
                                                NSFontAttributeName:[UIFont systemFontOfSize:13],
                                                
                                                };
        [item setTitleTextAttributes:normalTitleAttributes forState:UIControlStateNormal];
        [item setTitleTextAttributes:highlightTitleAttributes forState:UIControlStateHighlighted|UIControlStateSelected];
        nav.tabBarItem = item;
        
        [vcs addObject:nav];
    }
    [super setViewControllers:vcs];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   // [self.tabBar setTranslucent:NO];
    //self.tabBar.clipsToBounds = NO;
    // Do any additional setup after loading the view.
    
    self.tabBar.layer.borderColor = [UIColor clearColor].CGColor;
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
   
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
   } 

-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    button.tag = kCaremaTag;
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(cameraClick:) forControlEvents:UIControlEventTouchUpInside];
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0)
        button.center = self.tabBar.center;
    else
    {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0;
        button.center = center;
    }
    
    [self.view addSubview:button];
}

- (void)cameraClick:(UIButton *)btn{
    _scanvc = [[ScanQRCodeVC alloc ]init];
    
    __weak CustomTabBarController *weakSelf = self;
    [_scanvc handleFinish:^(NSString *str)  {
        NSLog(@"%@",str);
        if (str.length == 0) {
            return ;
        }
        QRPayViewController *qrpay = [[QRPayViewController alloc] init];
        qrpay.bizUrl = str;
        BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:qrpay];
        [weakSelf presentViewController:nav animated:NO completion:nil];
        
    }];
    [self presentViewController:_scanvc animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
