//
//  CustomTabBarController.h
//  PaiDou
//
//  Created by JSen on 14/11/5.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kTabClassName = @"tabbar class name";
static NSString *const kTabItemTitle = @"tabbar item title";
static NSString *const kTabNormalImageName = @"tabbar item normal image name";
static NSString *const kTabHighlihtImageName = @"tabbar item highlight image name";

#define kCaremaTag 18784992
@interface CustomTabBarController : UITabBarController

- (void)loadViewControllersWithInfoDictionary:(NSDictionary *)firstDict,... NS_REQUIRES_NIL_TERMINATION;

// Create a custom UIButton and add it to the center of our tab bar
-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage;


@end
