//
//  Define.h
//  AFNetworking iOS Example
//
//  Created by JSen on 14/9/24.
//  Copyright (c) 2014年 Gowalla. All rights reserved.
//


static NSString *const AFHttpHandlerBaseUrlString = @"http://paidoutest.sinaapp.com/api/";

static NSString *const kInternetConnectionActiveNotification = @"InternetConnectionActive";
static NSString *const kInternetConnectionDownNotification = @"InternetConnectionTearDown";

static NSString *const kSafePaySuccessNotificatino = @"AliSafePaySuccess";
static NSString *const kSafePayFailedNotification = @"AliSafePayFailed";

//登录，退出都需要重新加载任务列表
static NSString *const kNotifyNeedReloadTaskList = @"you need to reload task list";

static NSString *const kNotifyNeedLoginVCDismiss = @"login vc need dismiss,not pop";
static NSString *const kSafePayResultKey = @"safePayResult";

static NSInteger const kHotSelectVCPosterHeight = 150;

//兑换码兑还成功更新用户信息
static NSString *const kNotifyNeedUpdateUserInfo = @"you need to reload UserInfo";

//首页左右滚动高度
static NSInteger const kMyProfileHeaderHeight = 160;

//placeholder image name
static NSString *const kPlaceHolderImageName = @"placeholderImage.jpg";
static NSString *const kPlaceHolderText = @"加载中...";

#define kHolderImage [UIImage JSenImageNamed:kPlaceHolderImageName]

//商品支付类型
static NSString *const kProductTypeCash = @"cash";
static NSString *const kProductTypeScore = @"score";
static NSString *const kProductTypeMixed = @"mixed";

//订单状态
//wait_for_pay、payed、consumed、refunding、refunded
static NSString *const kOrderStatusWait_for_pay = @"wait_for_pay";
static NSString *const kOrderStatusPayed = @"payed";
static NSString *const kOrderStatusConsumed = @"consumed";
static NSString *const kOrderStatusRefunding = @"refunding";
static NSString *const kOrderStatusRefunded = @"refunded";

//兑换码状态
static NSString *const kCodeStatusConsumed = @"consumed";//未被消费、使用的
static NSString *const kCodeStatusUnconsumed = @"unconsumed";//已被消费、使用过。
static NSString *const kCodeStatusRefunding = @"refunding";//退款处理中
static NSString *const kCodeStatusRefunded = @"refunded";   //退款完成

//sign
static NSString *const kSignAppID = @"1002";
static NSString *const kSignAppKEY = @"49AU6oSDJrsjE8nM";

//任务类型
static NSString *const kTaskTypeExtend = @"extend";
static NSString *const kTaskTypeInfo = @"info";
static NSString *const kTaskTypeSign = @"sign";

//任务状态available|complete|soldout

static NSString *const kTaskStatusAva = @"available";
static NSString *const kTaskStatusComplete = @"complete";
static NSString *const kTaskStatusSoldout = @"soldout";

//支付类型
static NSString *const kPayMethodAli = @"alipay";
static NSString *const kPayMethodWeiXin = @"wxpay";

//登录通知
static NSString *const kNotifyLoginSuccess = @"i am in";
static NSString *const kNotifyLogoff = @"i am out";

//选折某个地铁站通知
static NSString *const kNotifyDidSelectStation = @"i selected a station";

//找到距离最近的地铁站通知
static NSString *const kNotifyFoundNearestStation = @"found a nearest station";

static NSString *const kNotifyWriteMetroLineSuccess = @"i write metro line data to file";

//itunes app id
static NSString *const APPID = @"944910117";

static NSString *const ShareSDKAppkey = @"4f5e04132cc6";

#define kWXAppID @"wx71ee9539ea8a79aa"

#define kAliSchema @"alibaba"

#define kBaiDuMapKey @"hOMsGhIjT8bOyK9KpIz7WHYx"

#define CONTENT @"ShareSDK不仅集成简单、支持如QQ好友、微信、新浪微博、腾讯微博等所有社交平台，而且还有强大的统计分析管理后台，实时了解用户、信息流、回流率、传播效应等数据，详情见官网http://sharesdk.cn @ShareSDK"

#define DEV_UUID [[[UIDevice currentDevice] identifierForVendor] UUIDString]

#define PropertyString(p) @property(nonatomic,copy) NSString *(p)
#define PropertyFloat(p) @property (nonatomic, assign) CGFloat (p)
#define PropertyInt(p) @property (nonatomic, assign) NSInteger (p)
#define PropertyUInt(p) @property (nonatomic, assign) NSUInteger (p)
#define PropertyBool(p) @property (nonatomic,assign) BOOL (p)
#define PropertyNumber(p) @property (nonatomic,retain) NSNumber *(p)


#define BUNDLE_CELL(bundleName) [[[NSBundle mainBundle] loadNibNamed:bundleName owner:self options:nil] lastObject]

//存放地铁线路的文件路径
#define kMetroLinePath [NSString stringWithFormat:@"%@/metroLine", NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0]]


//内部版本号
#define BUILD_VERSION [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] intValue]
//外部版本号
#define LOCAL_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

//-------
#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{4,16}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,32}"
//#define REGEX_PHONE_DEFAULT @"[0-9]{3}\\[0-9]{4}\\[0-9]{4}"
#define REGEX_PHONE_DEFAULT @"[0-9]{11}"
#define REGEX_RANDOMCODE @"[0-9]{6}"
#define debugUID @"U926C5B7E6F7F84E21AEE49D44DDFA483"

#define kBaseUrl(str) [NSString stringWithFormat:@"%@%@",AFHttpHandlerBaseUrlString,str]

//获取短信验证码 post
#define kRandomVaildateCodeUrl  kBaseUrl(@"sms/send?")

//验证短信验证码 post
#define kVerifyVaildateCodeUrl kBaseUrl(@"sms/verify?")


//用户注册
#define kRegisterURL kBaseUrl(@"user/register?")

//获取用户基本信息 get
#define kUserBasicInfoUrl kBaseUrl(@"user/item?")

//登录 post
#define kLoginURL kBaseUrl(@"user/login?")
//5.5获取用户的积分收支列表（分页）
#define kScoreListURL kBaseUrl(@"user/score_list?")

//5.6.	 更改用户信息
#define kModifyUserInfo kBaseUrl(@"user/update")

//5.7 修改登录密码
#define kChangePass kBaseUrl(@"user/update_password")
/*
 6 商户
 */
//6.3 查找商户列表（分页）
#define kBizsListURL kBaseUrl(@"bizs/list?")

//6.4获取指定商户的详细信息
#define kBizsDetailURL kBaseUrl(@"bizs/item?")
/*
 7 产品
 */
//7.5 获取产品列表（分页）
#define kProductsListSearchURL kBaseUrl(@"products/search?")

//7.3 获取指定商户的产品列表（不分页,带bid为获取商户下的产品列表，不带为所有产品列表）
//sort	“time|sold|comment”(string)
//sort字段可不填 不填时为默认排序。time：按截至日期排序/sold 按卖出最多排序	/comment按评论最多排序
#define kProductsListUrl kBaseUrl(@"products/list")

//7.4 获取指定产品的详细信息
#define kProductDetailURL kBaseUrl(@"products/item?")

/*
 8 订单
 */
#define kCreateOrderURL kBaseUrl(@"orders/new_order?")

//订单详情
#define kGetOrderDetailURL kBaseUrl(@"orders/item?")

//获取支付参数
#define kGetPayParam kBaseUrl(@"orders/get_pay_params")

//8.5获取某用户的订单列表（分页）
#define kGetUserOrdersURL kBaseUrl(@"orders/list?")

#define kDeleteUserOrderURL kBaseUrl(@"orders/del_order")

//8.7 申请退单
#define kRefoundUserOrderURL kBaseUrl(@"orders/apply_refund?")

//8.10 获取用户的兑换码（验证码）
#define kGetVerifyCodesURL kBaseUrl(@"orders/verify_codes?")

//http://jifenmall.sinaapp.com/api/redeem/list?uid=111&oid=123
#define kGetCommodityCodesURL kBaseUrl(@"redeem/list?")

/*十二 任务
 12.1.	 获取任务列表（分页）
 */

#define kGetTaskListURL kBaseUrl(@"task/list")

//2. 获得赚派豆详情信息
#define kGetTaskDetailURL kBaseUrl(@"task/item")

//12.2.	 完成任务回调通知

#define kTaskNotifyURL kBaseUrl(@"task/notify")

//12.3.	获取用户已完成的任务列表（分页
#define kTaskFinishedURL kBaseUrl(@"task/my_list")


/*十三 广告
 13.1 获取广告信息
 */
#define kAdvertisementURL kBaseUrl(@"ad/all?")

//13.2  广告图片
#define kAdvertisementImageURL kBaseUrl(@"ad/banner")

//13.3 推荐任务
#define kAdvertisementTaskURL kBaseUrl(@"ad/task")

//13.4 推荐商品
#define kAdvertisementProductURL kBaseUrl(@"ad/product")
/*
 十四）积分任务
 */
//14.1 更新任务
#define kTaskComplete kBaseUrl(@"task/complete?")

//14.2 获取用户任务完成情况。
#define kTaskMy kBaseUrl(@"task/my?")

//14.3 获取用户任务列表。
#define kTaskList kBaseUrl(@"task/list?")

#define kAlipayNotifyURL kBaseUrl(@"payment/alipay_notify")

//反馈 /feedback/new
#define kFeedbackURL kBaseUrl(@"feedback/new")

//升级
#define kCheckVersionURL kBaseUrl(@"upgrade/item")

#define kTestUpdateHeadicon kBaseUrl(@"user/update_avatar")


/*
 十六 地理位置
 */

#define kGetSubwayStations kBaseUrl(@"metas/subway")

//1.添加地理位置信息
#define kAddUserLocationUrl kBaseUrl(@"location/add")

/*

 十七 专题页面
 */

#define kTopicURL kBaseUrl(@"topic/item")

/*
 十八 推送消息
 */
#define kGetPushMsgURL kBaseUrl(@"push/list")

/*
 十九 派豆兑换码
 */

#define kScoreCode kBaseUrl(@"score_code/validate")
