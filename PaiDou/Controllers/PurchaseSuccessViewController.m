//
//  PurchaseSuccessViewController.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "PurchaseSuccessViewController.h"
#import "OrderModel.h"
#import "TabSecondViewController.h"
#import "VerifyCodeVC.h"
#import "CodeCell.h"

@interface PurchaseSuccessViewController ()
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,retain) OrderModel *dOrder;

@property (nonatomic, retain) NSMutableArray * codeArray;

@end

@implementation PurchaseSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor appBackgroundColor];
    
    self.tableView.layer.borderWidth = 0.5;
    
    self.tableView.layer.borderColor = [UIColor colorWithRed:228/255.0 green:228/255.0 blue:228/255.0 alpha:1].CGColor;
    
//    _footerView.size = CGSizeMake(SCREEN_WIDTH, 100);
//    _tableView.tableFooterView = _footerView;
    
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"兑换成功"];
    
    self.checkButton.tag = 1;
    
//    self.checkButton.layer.masksToBounds = YES;
//    
//    self.continueButton.layer.cornerRadius = 3;
    
    [_checkButton setRoundedCornerWithRadius:3];
    
    [self.checkButton addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    
    self.continueButton.tag = 2;
     [_continueButton setRoundedCornerWithRadius:3];
    
    [self.continueButton addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    
    [self loadOrderDetail];
    
    //UIBarButtonItem *nItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:nil action:nil];
    
    self.navigationItem.hidesBackButton = YES;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)loadOrderDetail{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *param = @{
                            @"oid":_orderModel.oid,
                            @"uid":[[UserManager sharedManager]uid]
                            };
    [manager GET:kGetOrderDetailURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue] == 0) {
            NSError *error = nil;
            _dOrder = [[OrderModel alloc] initWithDictionary:responseObject[@"order_info"] error:&error];
            
            //NSDictionary * dic = responseObject[@"order_info"][@"verify_codes"][0];
            
            [self setArray:responseObject];
            
            //[self setTableViewFrame:responseObject[@"order_info"][@"verify_codes"]];
            
            //[self showCode:dic];
            
            if (error) {
                NSLog(@"%@",error);
            }
            else
            {
                
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.codeArray.count + 2;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        UITableViewCell * cell = [[UITableViewCell alloc] init];
        
        cell.textLabel.text = [NSString stringWithFormat:@"商家：%@", _productModel.biz.name];
        
        cell.textLabel.textColor = [UIColor appb6b6b6Color];
        
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        
        return cell;
    }
    else if (indexPath.row == 1) {
        UITableViewCell * cell = [[UITableViewCell alloc] init];
        
        [self setCellTitle:cell];
        
        //cell.textLabel.textColor = [UIColor appb6b6b6Color];
        
        //cell.textLabel.font = [UIFont systemFontOfSize:16];
        
        return cell;
    }
    else
    {
        CodeCell * cell = [tableView dequeueReusableCellWithIdentifier:@"id"];
        
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"CodeCell" owner:self options:nil] firstObject];
        }
        
        cell.codeLabel.text = self.codeArray[indexPath.row - 2][@"code"];
        
        return cell;
    }
    return nil;
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(void)setCellTitle:(UITableViewCell *)cell
{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"兑换码(可在我的订单里查看)"];
    
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, 3)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(3, 11)];
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appb6b6b6Color] range:NSMakeRange(0, str.length)];
    
    cell.textLabel.attributedText = str;
}

-(void)setArray:(id)responseObject
{
    self.codeArray = [NSMutableArray arrayWithCapacity:0];
    
    for (NSDictionary * dic in responseObject[@"order_info"][@"verify_codes"]) {
        [self.codeArray addObject:dic];
    }
    
    [_tableView reloadData];
}

-(void)setTableViewFrame:(NSArray *)array
{
    CGRect frame = self.tableView.frame;
    
    frame.size.height = 88 + 30 * array.count;
    
    self.tableView.frame = frame;
    
    CGRect frame1 = _checkButton.frame;
    
    frame1.origin.y = frame.origin.y + frame.size.height + 28;
    
    _checkButton.frame = frame1;
    
    CGRect frame2 = _continueButton.frame;
    
    frame2.origin.y = frame.origin.y + frame.size.height + 28;
    
    _checkButton.frame = frame2;
}

-(void)showCode:(NSDictionary *)dic
{
    
}

-(NSString *)numberWithCode:(UserVerifyCode *)oneCode
{
    return [NSString stringWithFormat:@"兑换码: %@",oneCode.code];
}

-(void)click:(UIButton *)button
{
    switch (button.tag) {
        case 1:
        {
            VerifyCodeVC *vc = [[VerifyCodeVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
//            TabSecondViewController *tabSecond = [TabSecondViewController shared];
            
//            if (tabSecond) {
//                [self.navigationController pushViewController:tabSecond animated:YES];
//            }
//            else
//            {
               UIViewController *vc = self.navigationController.viewControllers[1];
                [self.navigationController popToViewController:vc animated:YES];
            //}
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
