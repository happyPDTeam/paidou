//
//  CommitOrderVC.h
//  PaiDou
//
//  Created by JSen on 14/11/11.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
@class ProductDetailMD;

@interface CommitOrderVC : BaseViewController

@property (nonatomic, retain) ProductDetailMD *detailModel;

@end
