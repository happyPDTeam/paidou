//
//  FreeGoodsCell.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrikeLabel.h"
#import "ProductSummaryMD.h"

@interface FreeGoodsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UIImageView *freeImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currenPrice;
@property (weak, nonatomic) IBOutlet UILabel *savePrice;
@property (weak, nonatomic) IBOutlet UILabel * countLabel;
@property (weak, nonatomic) IBOutlet UILabel *shopLabel;

@property (strong, nonatomic) StrikeLabel * _strikeLabel;

-(void)config:(ProductSummaryMD *)model;

@end
