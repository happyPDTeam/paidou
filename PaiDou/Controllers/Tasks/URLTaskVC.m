//
//  URLTaskVC.m
//  PaiDou
//
//  Created by JSen on 14/11/12.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "URLTaskVC.h"
#import "TaskModel.h"
#import "CustomTabBarController.h"

@interface URLTaskVC ()<UIWebViewDelegate>{
    MBProgressHUD *_hud;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation URLTaskVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed =YES;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:_webView];
   
   

    NSURL *req_url = nil;
    if (_tmodel) {
        req_url =[NSURL URLWithString:[_tmodel.task_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }else if (_url.length){
        req_url = [NSURL URLWithString:_url];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:req_url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    
    
    [_webView loadRequest:request];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
    
    _hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    _hud.userInteractionEnabled = NO;
    _hud.mode = MBProgressHUDModeIndeterminate;
    
   
}

- (void)back{
    [_hud hide:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyNeedReloadTaskList object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [_hud show:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:title];
    [_hud hide:YES];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if ([error code] != NSURLErrorCancelled) {
        //show error alert, etc.
        _hud.labelText = @"加载失败，请重试";
        [_hud hide:YES afterDelay:1];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
