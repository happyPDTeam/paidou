//
//  TaskDetail01VC.m
//  PaiDou
//
//  Created by JSen on 14/11/12.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "TaskDetail01VC.h"
#import "TaskModel.h"

@interface TaskDetail01VC ()

@end

@implementation TaskDetail01VC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed =YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
      self.navigationItem.titleView = [Utils titleLabelWithTitle:@"签到"];
   
//    if (_tmodel) {
//        [self loadData];
//    }
    
    // Do any additional setup after loading the view from its nib.
}
//MARK: no need any more
- (void)loadData{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *paramDict = @{
                                @"tid":_tmodel.tid
                                };
    
    [manager GET:kGetTaskDetailURL parameters:paramDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
