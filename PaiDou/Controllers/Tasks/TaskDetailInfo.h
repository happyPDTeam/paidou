//
//  TaskDetailInfo.h
//  PaiDou
//
//  Created by JSen on 14/11/12.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
@class TaskModel;
@interface TaskDetailInfo : BaseViewController


@property (nonatomic, retain) TaskModel *tmodel;
@end
