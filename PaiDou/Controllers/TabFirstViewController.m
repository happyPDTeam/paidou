//
//  TaskViewController.m
//  PaiDou
//
//  Created by JSen on 14/11/5.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "TabFirstViewController.h"
#import "MyScoreProfileVC.h"
#import "ScoreTaskCell.h"
#import "RegVC.h"
#import "LoginViewController.h"
#import "TaskModel.h"
#import "URLTaskVC.h"
#import "TaskDetail01VC.h"
#import "TaskDetailInfo.h"
#import "BaseNavigationController.h"
#import "SVWebViewController.h"
#import "UIViewController+RESideMenu.h"
#import "ShowBeansView.h"
#import "ScoreRecordVC.h"
#import "ProductCell.h"
#import "SVWebViewController.h"
@interface TabFirstViewController ()<UITableViewDataSource,UITableViewDelegate>{
    MyScoreProfileVC *_myScoreVC;
    
    ShowBeansView * _beanView;
    
    UIView * _tableHeaderview;
}


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrayData;
@end

@implementation TabFirstViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _myScoreVC = [[MyScoreProfileVC alloc] init];
    _arrayData = [NSMutableArray new];
    
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"派豆"];
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
//    self.tableView.tableHeaderView = _myScoreVC.view;
    
    [_myScoreVC handleClick:^(UIView *sender, BOOL isReg) {
//        if (!isReg) {
//            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SettingsStory" bundle:nil];
//            BaseViewController *vc = [story instantiateViewControllerWithIdentifier:@"servieceID"];
//            [vc hideCarema];
//            [self.navigationController pushViewController:vc    animated:YES];
//        }
        if (sender.tag == 1000) {
            //立即注册
//            [self reg:nil];
            [self login:nil];
        }else if (sender.tag == 900) {
            //help
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SettingsStory" bundle:nil];
            UIViewController *vc = [story instantiateViewControllerWithIdentifier:@"servieceID"];
            [self.navigationController pushViewController:vc animated:YES];
            
        }else if (sender.tag == 800) {
            //点击banner
//            [self login:nil];
            [self reg:nil];
        }
    }];
    
    [self createTableHeaderView];
    
    [self createBeanView];
    
//    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithTitle:@"注册" style:UIBarButtonItemStylePlain target:self action:@selector(reg:)];
//    self.navigationItem.leftBarButtonItem = left;
//    
   
//    self.navigationItem.rightBarButtonItem =  [Utils rightbuttonItemWithImage:@"nav_btn_person_normal.png" highlightedImage:@"nav_btn_person_highlight.png" target:self action:@selector(rightClick) ];
   
    [self loadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadList) name:kNotifyNeedReloadTaskList object:nil];
    
    [[NSNotificationCenter defaultCenter ]addObserverForName:kNotifyLoginSuccess object:nil queue:[NSOperationQueue currentQueue] usingBlock:^(NSNotification *note) {
        [_myScoreVC reloadMyProfile];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotifyLogoff object:nil queue:[NSOperationQueue currentQueue] usingBlock:^(NSNotification *note) {
        [self loadData];
    }];
    // Do any additional setup after loading the view from its nib.
}

//添加tableHeaderView
-(void)createTableHeaderView
{
    _tableHeaderview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    
    _tableHeaderview.backgroundColor = [UIColor whiteColor];
    
    self.tableView.tableHeaderView = _tableHeaderview;
}

//显示派豆
-(void)createBeanView
{
    //派豆剩余
    _beanView = [[ShowBeansView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    
    __weak TabFirstViewController * vc = self;
    
    _beanView.block = ^()
    {
        [vc.navigationController pushViewController:[[ScoreRecordVC alloc] init] animated:YES];
    };
    
    [_tableHeaderview addSubview:_beanView];
}

//设置派豆数量
-(void)setBeanView
{
    User *user = [[UserManager sharedManager] readFromDisk];
    
    NSString * str1;
    
    NSString * str2;
    
    if ([[UserManager sharedManager] hasLogin])
    {
        str1 = [NSString stringWithFormat:@"已省：%@元",[Utils fenToYuan:user.score_money]];
        
        str2 = [NSString stringWithFormat:@"剩余：%d派豆",user.score];
    }
    
    else
    {
        str1 = [NSString stringWithFormat:@"已省：0元"];
        
        str2 = [NSString stringWithFormat:@"剩余：0派豆"];
    }
    
    [_beanView setTitle:str1 secondTitle:str2 buttonTitle:@"派豆明细》"];
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)reloadList{
    [self loadData];
}
- (void)rightClick{
    [self presentRightMenuViewController:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [_myScoreVC reloadMyProfile];
    [self setBeanView];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
   
}

- (void)loadData{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    NSString *uid = [[UserManager sharedManager] uid];
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    [paramDic setValue:uid forKey:@"uid"];
    [paramDic setValue:@1000 forKey:@"limit"];
    [paramDic setValue:@1 forKey:@"page"];
    
    [manager GET:kTaskList parameters:paramDic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"] intValue] == 0) {
            NSError *error;
            NSArray *arr = [TaskModel arrayOfModelsFromDictionaries:responseObject[@"task_list"] error:&error];
            if (error) {
                NSLog(@"%@",error);
                return ;
            }else{
                if (_arrayData.count ) {
                    [_arrayData removeAllObjects];
                }
                [_arrayData addObjectsFromArray:arr];
                [_tableView reloadData];
            }
            
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)login:(id)sender{
    LoginViewController *Login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:Login];
    [self presentViewController:nav animated:YES completion:nil];
   // [self.navigationController pushViewController:Login animated:YES];
}

- (void)reg:(id)sender{
    RegVC *reg = [[RegVC alloc]init];
   
//    BaseNavigationController *nav = [[BaseNavigationController alloc ]initWithRootViewController:reg];
//    [self presentViewController:nav animated:YES completion:nil];
    [self.navigationController pushViewController:reg animated:YES];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrayData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"Cellid";
    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:self options:nil] lastObject];
       
    }
    TaskModel *model = _arrayData[indexPath.section];
    [cell config:model];
    
    cell.layer.borderWidth = 0.5;
    
    cell.layer.borderColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1].CGColor;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (void)showloginAlert{
    RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消"];
    RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
        [self login:nil];
    }];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您尚未登录，请先登录" cancelButtonItem:cancel otherButtonItems:sure, nil];
    [alert show];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (![[UserManager sharedManager] hasLogin])  {
        [self showloginAlert];
        return;
    }
    
    TaskModel *model = _arrayData[indexPath.section];
    if ([model.status isEqualToString:kTaskStatusComplete]) {
        NSString *s = [NSString stringWithFormat:@"您已完成 %@",model.title];
        [self showHUDText:s  xOffset:0 yOffset:0];
        
        return;
    }
    
    URLTaskVC *task = [[URLTaskVC alloc ]initWithNibName:@"URLTaskVC" bundle:nil];
    task.tmodel = model;
    [self.navigationController pushViewController:task animated:YES];
    
    
    return;
    if ([model.type isEqualToString:kTaskTypeInfo]) {
        TaskDetailInfo *info = [[TaskDetailInfo alloc] initWithNibName:@"TaskDetailInfo" bundle:nil];
        info.tmodel = model;
        [self.navigationController pushViewController:info animated:YES];
        
    }else if ([model.type isEqualToString:kTaskTypeSign]) {
        TaskDetail01VC *detail = [[TaskDetail01VC alloc] initWithNibName:@"TaskDetail01VC" bundle:nil];
        detail.tmodel = model;
        [self.navigationController pushViewController:detail animated:YES];
    }else if ([model.type isEqualToString:kTaskTypeExtend]) {
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 102;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
