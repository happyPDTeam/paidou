//
//  LeftProfileViewController.h
//  PaiDou
//
//  Created by JSen on 14/11/6.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"

@interface SideMenuViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentBGView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)willShow;
@end
