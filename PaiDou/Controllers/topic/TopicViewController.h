//
//  TopicViewController.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/24.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"

@interface TopicViewController : BaseViewController

@property (nonatomic, copy) NSString * title;

@property (nonatomic, copy) NSString * pid;

@end
