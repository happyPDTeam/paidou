//
//  TopicViewController.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/24.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "TopicViewController.h"
#import "ProductDetailMD.h"
#import "FreeGoodsCell.h"
#import "ProductSummaryMD.h"
#import "CommodityDetailVC.h"

@interface TopicViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray * productArray;

@end

@implementation TopicViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed =YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData:self.pid];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    
    self.navigationItem.titleView = [Utils titleLabelWithTitle:self.title];
}

-(void)loadData:(NSString *)str
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *dict = @{
                           @"id":str,
                           };
    [manager GET:kTopicURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if (![responseObject[@"contents"] isKindOfClass:[NSNull class]]) {
                
                self.title = responseObject[@"title"];
                
                self.navigationItem.titleView = [Utils titleLabelWithTitle:self.title];
                
                NSError *err ;
                
                NSArray * arr = responseObject[@"contents"];
                
                [self createProductModel:arr];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    
                }
                
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)back:(UIBarButtonItem *)item
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createProductModel:(NSArray *)array
{
    self.productArray = [NSMutableArray arrayWithCapacity:0];
    
    for (NSDictionary * dic in array) {
        if ([dic[@"type"] isEqualToString:@"product"]) {
            NSDictionary * dict = dic[@"data"];
            
            ProductSummaryMD * model  = [[ProductSummaryMD alloc] init];
            
            [model setValuesForKeysWithDictionary:dict];
            
            [self.productArray addObject:model];
        }
    }
    
    [_tableView reloadData];
    
    //[self setTableViewFrame];
}

-(void)setTableViewFrame
{
    CGRect frame = _tableView.frame;
    
    frame.size.height = self.productArray.count * 80;
    
    _tableView.frame = frame;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.productArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    FreeGoodsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"free"];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"FreeGoodsCell" owner:self options:nil] firstObject];
    }
    
    ProductSummaryMD * model = self.productArray[indexPath.row];
    
    [cell config:model];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 104;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommodityDetailVC *detailvc = [[CommodityDetailVC alloc] init];
    detailvc.productSummoryMD = self.productArray[indexPath.row];
    [self.navigationController pushViewController:detailvc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
