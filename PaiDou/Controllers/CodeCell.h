//
//  CodeCell.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/24.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CodeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;

@end
