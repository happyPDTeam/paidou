//
//  ScoreTaskCell.h
//  PaiDou
//
//  Created by JSen on 14/11/10.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TaskModel;
@interface ScoreTaskCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (weak, nonatomic) IBOutlet UILabel *lb_price;
@property (weak, nonatomic) IBOutlet UILabel *lb_status;
//@property (weak, nonatomic) IBOutlet UIButton *btn_getScore;
@property (weak, nonatomic) IBOutlet UILabel *lb_getCount;
@property (weak, nonatomic) IBOutlet UILabel *lb_time;
//- (IBAction)btn_getScoreAct:(UIButton *)sender;
- (void)setModel:(TaskModel *)taskModel;
@property (weak, nonatomic) IBOutlet UILabel *lb_huoDou;

@end
