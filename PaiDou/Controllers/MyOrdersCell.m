//
//  MyOrdersCell.m
//  wft
//
//  Created by JSen on 14/10/24.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "MyOrdersCell.h"

@implementation MyOrdersCell

- (void)awakeFromNib {
    // Initialization code
//    _iconImage = (UIImageView *)[self.contentView viewWithTag:100];
//    _lb_name = (UILabel *)[self.contentView viewWithTag:101];
//    _lb_count = (UILabel*)[self.contentView viewWithTag:102];
//    _lb_price = (UILabel *)[self.contentView viewWithTag:103];
//    _lb_time = (UILabel *)[self.contentView viewWithTag:104];
//    _lb_status = (UILabel *)[self.contentView viewWithTag:105];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(OrderSummaryMD *)model {
    _model = model;
    [_iconImage sd_setImageWithURL:[NSURL URLWithString:[model.avatar stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage JSenImageNamed:kPlaceHolderImageName]];
    _lb_name.text = model.name;
   
    _lb_price.text = [NSString stringWithFormat:@"¥%@",[Utils fenToYuan:_model.total_price]];
    _lb_doushu.text = [NSString stringWithFormat:@"%d派豆",_model.total_score];
    
  //  NSString *price = [Utils fenToYuan:model.total_price];
//    if ([model.type isEqualToString:kProductTypeCash]) {
//        _lb_price.text =[NSString stringWithFormat:@"%@元",[Utils fenToYuan:model.total_price]];
//    }else if ([model.type isEqualToString:kProductTypeScore]) {
//        _lb_price.text = [NSString stringWithFormat:@"%d豆币",model.total_score];
//    }else if ([model.type isEqualToString:kProductTypeMixed]) {
//        _lb_price.text = [NSString stringWithFormat:@"%d分 + %d豆币",model.total_price,model.total_score];
//    }
  //  _lb_price.text = [NSString stringWithFormat:@"总价:%d豆币",model.total_price];
  
    _lb_status.text = [Utils orderStatusToChinese:_model.status];
    if ([_model.status isEqualToString:kOrderStatusWait_for_pay]) {
        _lb_status.textColor = [UIColor appBlueTextColor];
    }else if([_model.status isEqualToString:kOrderStatusRefunding]){
        _lb_status.textColor = [UIColor appGrayTextColor];
    }else if ([_model.status isEqualToString:kOrderStatusRefunded]) {
        _lb_status.textColor = [UIColor appRedTextColor];
    }
}

@end
