//
//  CommitFreeOrderVC.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
#import "ProductDetailMD.h"

@interface CommitFreeOrderVC : BaseViewController

@property (nonatomic, retain) ProductDetailMD *detailModel;

@end
