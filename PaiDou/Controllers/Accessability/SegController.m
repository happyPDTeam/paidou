//
//  SegController.m
//  PaiDou
//
//  Created by JSen on 14/11/11.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "SegController.h"

@interface SegController (){
    NSMutableArray *_muArr;
}

@end

@implementation SegController


- (id)initWithSegmentWithTitles:(NSString *)first,... NS_REQUIRES_NIL_TERMINATION{
    if(self = [super init]){
        va_list args;
        va_start(args, first);
        NSMutableArray *array = [NSMutableArray array];
        [array addObject:first];
        id arg = nil;
        
        while ((arg = va_arg(args, id))){
            if ([arg isKindOfClass:[NSString class]]) {
                [array addObject:arg];
            }
        }
        va_end(args);
        if (array.count == 0) {
            return nil;
        }
        
        if (_muArr){
            [_muArr removeAllObjects];
            [_muArr addObjectsFromArray:array];
        }else{
            _muArr = [NSMutableArray arrayWithArray:array];
        }
    }
    return self;
    
    
}

- (void)loadView{
    [super loadView];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    view.backgroundColor = [UIColor whiteColor];
    
  
    self.view = view;

}

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSArray *array = @[@"全部",@"0元购",@"截止时间"];
    int arrLength = _muArr.count;
    for (int i = 0; i < arrLength; i++) {
        CGRect rect = CGRectMake(i*(self.view.size.width/arrLength), 0, self.view.size.width/arrLength, self.view.height);
        SegView *segView = [[SegView alloc ] initWithTitle:_muArr[i] frame:rect];        segView.tag = i;
        [segView clickAction:^(int tag) {
            for (UIView  *oneView in self.view.subviews) {
                
                if ([oneView isKindOfClass:[SegView class]]) {
                    SegView *seg= (SegView *)oneView;
                    if (oneView == segView) {
                       seg.selected = YES;
                    }else{
                    seg.selected = NO;
                }
                   
            }
            }
            if (_block) {
                 _block(tag, segView);
            }
           
        }];
        [self.view addSubview:segView];
        
        
    }
        // Do any additional setup after loading the view from its nib.
}

- (void)handleClick:(void (^)(int index, UIView *view))actionBlock{
    _block = actionBlock;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"%s",__func__);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end


@implementation SegView

- (instancetype)initWithTitle:(NSString *)segTitle frame:(CGRect )frame  {
    self = [super initWithFrame:frame];
    if (self) {
        _segTitle = segTitle;
     
        self.userInteractionEnabled = YES;
        [self initialize];
    }
    return self;
}

- (void)initialize{
    UILabel *label = [[UILabel alloc ]initWithFrame:self.bounds];
    label.backgroundColor = [UIColor clearColor];
    label.userInteractionEnabled = YES;
    label.font = [UIFont appCellTitleFont];
    label.textColor = UIColorFromRGB(0xa6aaae);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = _segTitle;
   
    [self addSubview:label];
    
    _overlay = [[UIView alloc] initWithFrame:self.bounds];
    _overlay.backgroundColor = UIColorFromRGB(0xeeeeee);
    _overlay.alpha = 1;
    [self addSubview:_overlay];
    _overlay.hidden = YES;
    

}

- (void)clickAction:(void (^)(int))block{
    _clickBlock = [block copy];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    _overlay.hidden = NO;
    [self bringSubviewToFront:_overlay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    _overlay.hidden = YES;
    [self sendSubviewToBack:_overlay];
    if (_clickBlock) {
         _clickBlock(self.tag);
    }
   
}

- (void)setSelected:(BOOL)selected{
    if (selected) {
        _overlay.hidden = NO;
    }else{
        _overlay.hidden = YES;
    }
}

@end
