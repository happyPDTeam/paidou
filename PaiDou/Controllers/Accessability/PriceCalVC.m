//
//  PriceCalVC.m
//  PaiDou
//
//  Created by JSen on 14/11/12.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "PriceCalVC.h"
#import "CustomStepper.h"
#import "ProductDetailMD.h"

@interface PriceCalVC ()<UITextFieldDelegate>{
    CustomStepper *_stepper;
    BOOL _canChangeText;
}

@property (weak, nonatomic) IBOutlet UITextField *tf;
- (IBAction)tfAction:(UITextField *)sender;
@end

@implementation PriceCalVC

/*
original_price: 1800,							//原价 int 以分为单位
sale_price: 1000,							//派豆价 int 以分为单位
score_max: 80,								//最多可用的派豆数 int
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    _canChangeText = YES;
    //self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 120);
    _stepper = [[CustomStepper alloc] init];
    
    [_stepper textDidChanged:^(int number) {
        
    }];
    
    UIView *stepView = [_stepper getCustomStepper];
    
    stepView.origin = CGPointMake(185, 80);
    
    [self.view addSubview:stepView];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //取得当前用的的积分
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"replacementString--> %@",string);
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}// called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [_stepper removeObserver];
}

- (IBAction)tfAction:(UITextField *)sender {
    NSLog(@"%@",sender.text);
    
    
}

- (BOOL)canChangeText{
    CGFloat tfPrice = [_tf.text floatValue];
    if (tfPrice >= _dmodel.sale_price) {
      return  NO;
    }else{
        return YES;
    }
    return YES;
}
@end
