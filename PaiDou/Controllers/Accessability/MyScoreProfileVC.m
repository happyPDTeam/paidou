//
//  MyScoreProfileVC.m
//  PaiDou
//
//  Created by JSen on 14/11/10.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//


#import "MyScoreProfileVC.h"
#import "PNChart.h"
#import "UICountingLabel.h"
#import "BaseNavigationController.h"
#import "ClickableUIImageView.h"

@interface MyScoreProfileVC ()<UIScrollViewDelegate,PNChartDelegate>

@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong,nonatomic) UIView *lineChartBGView;
@property (weak, nonatomic) IBOutlet UIScrollView *scorllView;


@property (strong, nonatomic) IBOutlet UIView *profileView;

@property (strong, nonatomic) IBOutlet UIView *nologinBgView;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)loginBtnAction:(UIButton *)sender;

//登录后显示的详情
@property (weak, nonatomic) IBOutlet UILabel *_lb_des01;
@property (weak, nonatomic) IBOutlet UICountingLabel *_lb_scoreLeft;
@property (weak, nonatomic) IBOutlet UILabel *lb_value;
@property (weak, nonatomic) IBOutlet UILabel *lb_returnRate;
- (IBAction)btnHelpAction:(UIButton *)sender;
@end

@implementation MyScoreProfileVC

- (void)handleClick:(buttonClick)block{
    _block = [block copy];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _scorllView.scrollEnabled  = NO;
    
    _loginBtn.tag = 1000;
    
    _bgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kMyProfileHeaderHeight);
    [_bgView addSubview:_scorllView];
    
    
    _scorllView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kMyProfileHeaderHeight);
    _scorllView.contentSize = CGSizeMake(SCREEN_WIDTH*2, kMyProfileHeaderHeight);
    
    self.view = _bgView;
    
    //折线图
    _lineChartBGView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, kMyProfileHeaderHeight)];
    _lineChartBGView.backgroundColor = [UIColor whiteColor];
    [_scorllView addSubview:_lineChartBGView];
    
    //个人信息页
   // _profileView = [[UIView alloc ]initWithFrame:CGRectMake(0, 0, self.view.width, kMyProfileHeaderHeight)];
    _profileView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kMyProfileHeaderHeight);
    [_scorllView addSubview:_profileView];
    
    _nologinBgView.frame = _profileView.bounds;
    
    [_scorllView addSubview:_nologinBgView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginAction:) name:kNotifyLoginSuccess object:nil];
    
    //未注册时 添加banner图
    ClickableUIImageView *banView = [[ClickableUIImageView alloc] initWithFrame:_nologinBgView.bounds];
    banView.tag = 800;
    banView.image = [UIImage JSenImageNamed:@"zaocan.jpg"];
    
    [banView handleComplemetionBlock:^(ClickableUIImageView *view) {
        _block(view,NO);
    }];
    [_nologinBgView insertSubview:banView atIndex:0];
    
    _loginBtn.layer.borderColor = UIColorFromRGB(0xff6634).CGColor;
    _loginBtn.layer.borderWidth = 1;
    _loginBtn.layer.cornerRadius = 5;
    _loginBtn.layer.masksToBounds =YES;
}



- (void)loginAction:(NSNotification *)note{
    [self reloadMyProfile];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadMyProfile];
}

- (void)reloadMyProfile{
    [self changeStatus];
    User *user = [[UserManager sharedManager] readFromDisk];
    __lb_des01.text = [NSString stringWithFormat:@"赚了%@元",[Utils fenToYuan:user.score_money]];
    __lb_scoreLeft.format = @"%d";
    [__lb_scoreLeft countFrom:0 to:user.score withDuration:2.5];
    _lb_value.text = [NSString stringWithFormat:@"价值:%@",[Utils douToYuan:user.score]];
    _lb_returnRate.text = [NSString stringWithFormat:@"投资回报收益率%@%%",[Utils rateFormFloat:user.invest_rate]];
    [_profileView layoutSubviews];
    
}

- (void)changeStatus{
  
    
    if ([[UserManager sharedManager] hasLogin]) {
        _nologinBgView.hidden = YES;
        _scorllView.scrollEnabled = NO;
    }else{
        _nologinBgView.hidden =NO;
        _scorllView.scrollEnabled = NO;
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
     [self loadLineChart];
}

- (void)loadLineChart{
    PNLineChart * lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0, 10, _lineChartBGView.width, _lineChartBGView.height-10)];
    lineChart.yLabelFormat = @"%1.1f";
    lineChart.backgroundColor = [UIColor clearColor];
    [lineChart setXLabels:@[@"SEP 1",@"SEP 2",@"SEP 3",@"SEP 4",@"SEP 5",@"SEP 6",@"SEP 7"]];
    lineChart.showCoordinateAxis = YES;
    
    // Line Chart Nr.1
//    NSArray * data01Array = @[@60.1, @160.1, @126.4, @262.2, @186.2, @127.2, @176.2];
//    PNLineChartData *data01 = [PNLineChartData new];
//    data01.color = PNFreshGreen;
//    data01.itemCount = lineChart.xLabels.count;
//    data01.inflexionPointStyle = PNLineChartPointStyleCycle;
//    data01.getData = ^(NSUInteger index) {
//        CGFloat yValue = [data01Array[index] floatValue];
//        return [PNLineChartDataItem dataItemWithY:yValue];
//    };
    
    // Line Chart Nr.2
    NSArray * data02Array = @[@20.1, @180.1, @26.4, @202.2, @126.2, @167.2, @276.2];
    PNLineChartData *data02 = [PNLineChartData new];
    data02.color = PNTwitterColor;
    data02.itemCount = lineChart.xLabels.count;
    data02.inflexionPointStyle = PNLineChartPointStyleSquare;
    data02.getData = ^(NSUInteger index) {
        CGFloat yValue = [data02Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    lineChart.chartData = @[ data02];
    [lineChart strokeChart];
    
    lineChart.delegate = self;
    [_lineChartBGView addSubview:lineChart];
}

#pragma mark - lineChart delegate
/**
 * When user click on the chart line
 *
 */
- (void)userClickedOnLinePoint:(CGPoint)point lineIndex:(NSInteger)lineIndex{
    NSLog(@"%@ %d",NSStringFromCGPoint(point),lineIndex);
}

/**
 * When user click on the chart line key point
 *
 */
- (void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex andPointIndex:(NSInteger)pointIndex{
    NSLog(@"%@ %d %d",NSStringFromCGPoint(point),lineIndex,pointIndex);
}

/**
 * When user click on a chart bar
 *
 */
- (void)userClickedOnBarCharIndex:(NSInteger)barIndex{
    NSLog(@"%d",barIndex);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginBtnAction:(UIButton *)sender {
    _block(sender, YES);
}
- (IBAction)btnHelpAction:(UIButton *)sender {
 
    _block(sender,NO);
    
 
}
@end
