//
//  SegController.h
//  PaiDou
//
//  Created by JSen on 14/11/11.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//


@class SegView;
@interface SegController : UIViewController{
    void (^_block)(int index,UIView *view) ;
}

- (id)initWithSegmentWithTitles:(NSString *)first,... NS_REQUIRES_NIL_TERMINATION;

- (void)handleClick:(void (^)(int index, UIView *view))actionBlock;

@end


@interface SegView : UIView{
    NSString *_segTitle;
    UIView *_overlay;
    void (^_clickBlock)(int tag );
}

@property (nonatomic, assign) BOOL selected;
- (instancetype)initWithTitle:(NSString *)segTitle frame:(CGRect)frame ;

- (void)clickAction:(void (^)(int tag))block;


//@property (nonatomic, retain) NSString *segTitle;

@end