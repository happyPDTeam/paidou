//
//  MyScoreProfileVC.h
//  PaiDou
//
//  Created by JSen on 14/11/10.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^buttonClick)(UIView *sender, BOOL isReg);

@interface MyScoreProfileVC : BaseViewController{
    buttonClick _block;
}

- (void)handleClick:(buttonClick)block;

- (void)reloadMyProfile;
@end
