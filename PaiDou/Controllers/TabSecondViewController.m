//
//  TabSecondViewController.m
//  PaiDou
//  积分兑换
//  Created by JSen on 14/11/6.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "TabSecondViewController.h"
#import "ScoreExchangeCell.h"
#import "ProductSummaryMD.h"
#import "CommodityDetailVC.h"
#import "SegController.h"
#import "SVPullToRefresh.h"
#import "ClickableUIView.h"
#import "StationSelectViewController.h"
#import "StationLine.h"
#import "ShowBeansView.h"
#import "JOLImageSlider.h"
#import "ScoreRecordVC.h"
#import "FreeGoodsCell.h"
#import "TaskModel.h"
#import "URLTaskVC.h"
#import "TopicViewController.h"
#import "TaskDetailInfo.h"
#import "TaskDetail01VC.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"

@interface TabSecondViewController ()<UITableViewDelegate,UITableViewDataSource, JOLImageSliderDelegate>{
  
    SegController *_segController;
    
    ClickableUIView *_bgView;
    
    StationSelectViewController *_stationVC;
    
    ShowBeansView * _beanView;
    
    UIView * _tableHeaderview;
    
    JOLImageSlider * _joliImageSlider;
}
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,assign) NSInteger limit;
@property (strong, nonatomic)  UIView *segBGView;
@property (strong, nonatomic)  UITableView *tableView;
@property (strong,nonatomic) NSMutableArray *arrData;
@property (strong,nonatomic) NSMutableArray *arrDuiHuan;
@property (strong,nonatomic) NSMutableArray *arrEndTime;
@property (strong,nonatomic) NSMutableArray *arrComment;

@property (strong,nonatomic) NSMutableArray *manageArray;
@property (assign,nonatomic) NSInteger currIndex;
@property (strong, nonatomic) IBOutlet ClickableUIView *titleView;
@property (weak, nonatomic) IBOutlet UILabel *lb_title;

@property (strong, nonatomic) TaskModel * taskModel;

@end

@implementation TabSecondViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

//+ (TabSecondViewController *)shared
//{
//    static TabSecondViewController * vc = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        vc = [[TabSecondViewController alloc] init];
//    });
//    return vc;
//}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
  
    [self setBeanView];
    
    NSIndexPath * selected = [self.tableView indexPathForSelectedRow];
    
    if(selected)
    {
        [self.tableView deselectRowAtIndexPath:selected animated:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadTitleView];
}
- (void)loadTitleView{
    //new-----
    
    _titleView.size = CGSizeMake(150, 40);
    self.navigationItem.titleView = _titleView;
    
    _stationVC = [[StationSelectViewController alloc ]init];
    
    _bgView = [[ClickableUIView alloc ]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _bgView.backgroundColor = [UIColor clearColor];
    [_bgView handleComplemetionBlock:^(ClickableUIView *view) {
        _bgView.hidden = !_bgView.hidden;
    }];
    _stationVC.view.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    [_bgView addSubview:_stationVC.view];
    
    AppDelegate *del = [[UIApplication sharedApplication]delegate];
    [del.window addSubview:_bgView];
    _bgView.hidden = YES;
    
    [_titleView handleComplemetionBlock:^(ClickableUIView *view) {
        _bgView.hidden = !_bgView.hidden;
    }];
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotifyDidSelectStation object:nil queue:[NSOperationQueue currentQueue] usingBlock:^(NSNotification *note) {
        Station *station = note.userInfo[@"S"];
        _lb_title.text = station.station_name;
        _currStation = station;
        _bgView.hidden = YES;
        [self loadProductsDataNeedClear:YES];
    }];

}

- (void)viewDidLoad {
    [super viewDidLoad];
  
    _limit = 10;
    _page = 1;
    _currIndex = 0;
    _arrData = [NSMutableArray array];
    _arrDuiHuan = [NSMutableArray new];
    _arrEndTime = [NSMutableArray new];
    _arrComment = [NSMutableArray new];
    _manageArray = [NSMutableArray arrayWithObjects:_arrData,_arrDuiHuan,_arrEndTime,_arrComment, nil];
    _lb_title.text = _currStation.station_name;
    
    
    _segController = [[SegController alloc] initWithSegmentWithTitles:@"全部",@"0元购",@"截止时间", nil];
    
    [self.view addSubview:_segController.view];
    
    [_segController handleClick:^(int index, UIView *view) {
        NSLog(@"%d",index);
        if (index != self.currIndex) {
            _currIndex = index;
            [_tableView reloadData];
        }
  
    }];
    
//      [self loadTitleView];
    
    _tableView = [[UITableView alloc ] initWithFrame:CGRectMake(0, _segController.view.bottom, SCREEN_WIDTH, SCREEN_HEIGHT-64-_segController.view.height) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1];
    [self.view addSubview:_tableView];
    
    [self createTableHeaderView];
    
    [self createJOLImageSlider:self.bannerArray];
    
    [self createBeanView];
    
   
    __weak TabSecondViewController *weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        weakSelf.page++;
        [weakSelf loadProductsDataNeedClear:NO];
        
    }];
    
    [self showLoadingAnimated:YES];
    [self loadProductsDataNeedClear:NO];
    
     self.view.backgroundColor = [UIColor appBackgroundColor];
    _tableView.backgroundColor =[UIColor appBackgroundColor];
//     self.navigationItem.titleView = [Utils titleLabelWithTitle:@"派豆"];
    
//self.navigationItem.rightBarButtonItem =  [Utils rightbuttonItemWithImage:@"nav_btn_person_normal.png" highlightedImage:@"nav_btn_person_highlight.png" target:self action:@selector(rightClick) ];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
}

-(void)createTableHeaderView
{
    _tableHeaderview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 180)];
    
    _tableHeaderview.backgroundColor = [UIColor whiteColor];
    
    _tableHeaderview.layer.borderWidth = 0.5;
    
    _tableHeaderview.layer.borderColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1].CGColor;
    
    _tableView.tableHeaderView = _tableHeaderview;
}

//加载banner数据
-(void)loadBannerData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *dict = @{
                           @"station":@(_currStation.station_id),
                           @"locate":@"product"
                           };
    [manager GET:kAdvertisementImageURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if (![responseObject[@"banners"] isKindOfClass:[NSNull class]]) {
                
                
                NSError *err ;
                
                NSArray * array = responseObject[@"banners"];
                
                [self createJOLImageSlider:array];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    
                }
                
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)createJOLImageSlider:(NSArray *)array
{
    NSMutableArray * jArray = [NSMutableArray arrayWithCapacity:0];
    
    self.bannerArray = [NSMutableArray arrayWithArray:array];
    
    for (int i = 0; i < array.count; i++) {
        JOLImageSlide * joliImageSlide = [[JOLImageSlide alloc] init];
        
        NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:array[i]];
        
        NSString * encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault, (CFStringRef)dic[@"image_url"], NULL, NULL,  kCFStringEncodingUTF8 ));
        
        [dic setValue:encodedString forKey:@"image_url"];
        
        joliImageSlide.infoDict = dic;
        
        joliImageSlide.image = dic[@"image_url"];
        
        joliImageSlide.title = @"";
        
        [jArray addObject:joliImageSlide];
        
        if ([dic[@"type"] isEqualToString:@"task"]) {
            [self loadTaskDetailData:dic];
        }
    }
    
    _joliImageSlider = [[JOLImageSlider alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 140) andSlides:jArray];
    
    _joliImageSlider.backgroundColor = [UIColor blackColor];
    
    _joliImageSlider.delegate = self;
    
    [_tableHeaderview addSubview:_joliImageSlider];

}

-(void)loadTaskDetailData:(NSDictionary *)dic
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *dict = @{
                           @"tid":dic[@"relate_id"]
                           };
    [manager GET:kGetTaskDetailURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if (![responseObject[@"task"] isKindOfClass:[NSNull class]]) {
                
                NSError *err ;
                
                NSDictionary * dic = responseObject[@"task"];
                
                [self createTaskDetailModel:dic];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    
                }
                
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
    
}


-(void)createTaskDetailModel:(NSDictionary *)dic
{
    self.taskModel = [[TaskModel alloc] init];
    
    [self.taskModel setValuesForKeysWithDictionary:dic];
}


-(void)createBeanView
{
    //派豆剩余
    _beanView = [[ShowBeansView alloc] initWithFrame:CGRectMake(0, 140, SCREEN_WIDTH, 40)];
    
    __weak TabSecondViewController * vc = self;
    
    _beanView.block = ^()
    {
        [vc.navigationController pushViewController:[[ScoreRecordVC alloc] init] animated:YES];
    };
    
    [_tableHeaderview addSubview:_beanView];
}

-(void)setBeanView
{
    User *user = [[UserManager sharedManager] readFromDisk];
    
    NSString * str1;
    
    NSString * str2;
    
    if ([[UserManager sharedManager] hasLogin])
    {
        str1 = [NSString stringWithFormat:@"已省：%@元",[Utils fenToYuan:user.score_money]];
        
        str2 = [NSString stringWithFormat:@"剩余：%d派豆",user.score];
    }
    
    else
    {
        str1 = [NSString stringWithFormat:@"已省：0元"];
        
        str2 = [NSString stringWithFormat:@"剩余：0派豆"];
    }
    
    [_beanView setTitle:str1 secondTitle:str2 buttonTitle:@"派豆明细》"];
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)rightClick{
    [self presentRightMenuViewController:nil];
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
//    NSLog(@"%@  change --> %@",keyPath,change);
//    
//}


- (void)loadProductsDataNeedClear:(BOOL)yesOrNo{
    if (self.tableView.infiniteScrollingView.state == (SVPullToRefreshStateLoading|SVPullToRefreshStateTriggered)) {
        return;
    }
//     [self showLoadingAnimated:YES];
    NSDictionary *paramDict = @{
                                @"limit":@(_limit),
                                @"page":@(_page),
                                @"station":@(_currStation.station_id)
                                };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager GET:kProductsListUrl parameters:paramDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"] intValue] == 0) {
            NSError *err;
            NSArray *array = [ProductSummaryMD arrayOfModelsFromDictionaries:responseObject[@"products_list"] error:&err];
            
            if (err) {
                NSLog(@"parse json model:%@",err);
                return ;
            }else{
                
            }
            
            if (yesOrNo) {
                [_arrData removeAllObjects];
                [_arrComment removeAllObjects];
                [_arrDuiHuan removeAllObjects];
                [_arrEndTime removeAllObjects];
            }
            
            
            [_arrData addObjectsFromArray:array];
            [_tableView reloadData];
            
            //这里是排序，不是筛选
            [self sortArray:_arrData];
            //
            int count = [responseObject[@"count"] intValue];
            int total = [responseObject[@"total"] intValue];
            
            if ( count < self.limit) {
                self.tableView.showsInfiniteScrolling = NO;
            }else if(count > self.limit){
                self.tableView.showsInfiniteScrolling = YES;
            }else if (count == self.limit) {
                self.tableView.showsInfiniteScrolling = total - _arrData.count > 0 ? 
                YES:NO;
            }
            
            
        }
        [self.tableView.infiniteScrollingView stopAnimating];
        [self hideLoadingViewAnimated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [self.tableView.infiniteScrollingView stopAnimating];
         [self hideLoadingViewAnimated:YES];
    }];
}

- (void)sortArray:(NSArray *)array{
    if (array.count == 0) {
        return;
    }
    //兑换最多
//    NSArray *duihuanMost = [array sortedArrayWithOptions:NSSortConcurrent usingComparator:^NSComparisonResult(id obj1, id obj2) {
//        ProductSummaryMD *m1 = (ProductSummaryMD *)obj1;
//        ProductSummaryMD *m2 = (ProductSummaryMD *)obj2;
//        if (m1.sold_count > m2.sold_count) {
//            return NSOrderedAscending;
//        }else if (m1.sold_count < m2.sold_count) {
//            return  NSOrderedDescending;
//        }else{
//            return NSOrderedSame;
//        }
//        
//    }];
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sale_price BETWEEN %@",@[@0, @2000]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sale_price == 0"];
    NSArray *duihuanMost = [array filteredArrayUsingPredicate:predicate];
    
    if (_arrDuiHuan.count) {
        [_arrDuiHuan removeAllObjects];
    }
    [_arrDuiHuan addObjectsFromArray:duihuanMost];
//    NSLog(@"%@",duihuanMost);
    //
    //截止时间
    NSArray *endTimeArr = [array sortedArrayWithOptions:NSSortConcurrent usingComparator:^NSComparisonResult(id obj1, id obj2) {
        ProductSummaryMD *m1 = (ProductSummaryMD *)obj1;
        ProductSummaryMD *m2 = (ProductSummaryMD *)obj2;
        if (m1.end_time > m2.end_time) {
            return NSOrderedAscending;
        }else if (m1.end_time < m2.end_time) {
            return  NSOrderedDescending;
        }else{
            return NSOrderedSame;
        }
        
    }];
//    NSLog(@"%@",endTimeArr);
    if (_arrEndTime.count) {
        [_arrEndTime removeAllObjects];
    }
    [_arrEndTime addObjectsFromArray:endTimeArr];
    
    //评论最多
    NSArray *commentMost = [array sortedArrayWithOptions:NSSortConcurrent usingComparator:^NSComparisonResult(id obj1, id obj2) {
        ProductSummaryMD *m1 = (ProductSummaryMD *)obj1;
        ProductSummaryMD *m2 = (ProductSummaryMD *)obj2;
        if (m1.comment_count > m2.comment_count) {
            return NSOrderedAscending;
        }else if (m1.comment_count < m2.comment_count) {
            return  NSOrderedDescending;
        }else{
            return NSOrderedSame;
        }
    }];
    if (_arrComment.count) {
        [_arrComment removeAllObjects];
    }
    [_arrComment addObjectsFromArray:commentMost];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSMutableArray *arr = _manageArray[self.currIndex];
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellid";
    FreeGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = BUNDLE_CELL(@"FreeGoodsCell");
    }
    NSMutableArray *arr = _manageArray[self.currIndex];
    ProductSummaryMD *model = arr[indexPath.row];
    [cell config:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray *arr = _manageArray[self.currIndex];
    ProductSummaryMD *sumModel = arr[indexPath.row];
    CommodityDetailVC *detailvc = [[CommodityDetailVC alloc] init];
    detailvc.productSummoryMD = sumModel;
    [self.navigationController pushViewController:detailvc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 107;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 104;
}

-(void)imagePager:(JOLImageSlider *)imagePager didSelectImageAtIndex:(NSUInteger)index
{
    NSDictionary * dic = self.bannerArray[index];
    
    if ([dic[@"type"] isEqualToString:@"product"]) {
        CommodityDetailVC *detailvc = [[CommodityDetailVC alloc] init];
        
        detailvc.releated_id = dic[@"relate_id"];
        
        [self.navigationController pushViewController:detailvc animated:YES];
    }
    else if ([dic[@"type"] isEqualToString:@"task"])
    {
        if (![[UserManager sharedManager] hasLogin])  {
            [self showloginAlert];
            return;
        }
        
        if ([self.taskModel.status isEqualToString:kTaskStatusComplete]) {
            NSString *s = [NSString stringWithFormat:@"您已完成 %@",self.taskModel.title];
            [self showHUDText:s  xOffset:0 yOffset:0];
            
            return;
        }
        
        URLTaskVC *task = [[URLTaskVC alloc ]initWithNibName:@"URLTaskVC" bundle:nil];
        task.tmodel = self.taskModel;
        [self.navigationController pushViewController:task animated:YES];
        
        
        return;
        if ([self.taskModel.type isEqualToString:kTaskTypeInfo]) {
            TaskDetailInfo *info = [[TaskDetailInfo alloc] initWithNibName:@"TaskDetailInfo" bundle:nil];
            info.tmodel = self.taskModel;
            [self.navigationController pushViewController:info animated:YES];
            
        }else if ([self.taskModel.type isEqualToString:kTaskTypeSign]) {
            TaskDetail01VC *detail = [[TaskDetail01VC alloc] initWithNibName:@"TaskDetail01VC" bundle:nil];
            detail.tmodel = self.taskModel;
            [self.navigationController pushViewController:detail animated:YES];
        }else if ([self.taskModel.type isEqualToString:kTaskTypeExtend]) {
            
        }
    }
    else
    {
        TopicViewController * vc = [[TopicViewController alloc] init];
        
        vc.pid = dic[@"relate_id"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)showloginAlert{
    RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消"];
    RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
        [self login:nil];
    }];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您尚未登录，请先登录" cancelButtonItem:cancel otherButtonItems:sure, nil];
    [alert show];
}

- (void)login:(id)sender{
    LoginViewController *Login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:Login];
    [self presentViewController:nav animated:YES completion:nil];
    // [self.navigationController pushViewController:Login animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
//    [self removeObserver:self forKeyPath:@"_currIndex"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
