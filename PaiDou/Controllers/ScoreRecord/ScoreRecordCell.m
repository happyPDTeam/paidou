//
//  ScoreRecordCell.m
//  PaiDou
//
//  Created by JSen on 14/11/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ScoreRecordCell.h"
#import "ScoreLogModel.h"
@implementation ScoreRecordCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) setModel:(ScoreLogModel *)model{
    NSString *title = nil;
    if (model.title.length) {
        title = model.title;
    }else if (model.p_name.length) {
        title = model.p_name;
    }else if (model.biz_name.length) {
        title = model.biz_name;
    }
    _lb_title.text = title.length == 0 ? @"暂无":title;
    
    NSString *strPre = nil;
    if (model.score >= 0) {
        strPre = @"赚取";
    }else{
        strPre = @"消费";
    }
    NSString *strScore = [NSString stringWithFormat:@"%d",abs(model.score)];
    
    _lb_pre.text = strPre;
    _lb_score.text = [NSString stringWithFormat:@"%@派豆",strScore];
    
    _lb_money.text = [NSString stringWithFormat:@"%@元",[Utils douToYuan:model.score]];
    
    _lb_time.text = [NSString stringWithFormat:@"%@",[Utils dateFromTimeInterval:model.create_time]];
}

//- (void)layoutSubviews{
//    [super layoutSubviews];
////    CGSize optimumSize = [self.lb_detail optimumSize];
////   
////    CGRect frame = [self.lb_detail frame];
////    frame.size.width = optimumSize.width;
////    [self.lb_detail setFrame:frame];
//}
@end
