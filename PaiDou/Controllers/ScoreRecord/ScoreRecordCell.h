//
//  ScoreRecordCell.h
//  PaiDou
//
//  Created by JSen on 14/11/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ScoreLogModel;

@interface ScoreRecordCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lb_title;
@property (weak, nonatomic) IBOutlet UILabel *lb_pre;
@property (weak, nonatomic) IBOutlet UILabel *lb_score;

@property (weak, nonatomic) IBOutlet UILabel *lb_time;

@property (weak, nonatomic) IBOutlet UILabel *lb_money;

- (void)setModel:(ScoreLogModel *)model;
@end
