//
//  ScoreRecordVC.m
//  PaiDou
//
//  Created by JSen on 14/11/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ScoreRecordVC.h"
#import "ScoreLogModel.h"
#import "ScoreRecordCell.h"
#import "SVPullToRefresh.h"

@interface ScoreRecordVC ()<UITableViewDataSource,UITableViewDelegate>
@property (retain,nonatomic) NSMutableArray *array;
@property (retain,nonatomic) NSMutableArray *arrIn;//赚取
@property (retain,nonatomic) NSMutableArray *arrOut;//消费

@property (strong, nonatomic) IBOutlet UISegmentedControl *seg;
- (IBAction)segAct:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tview;

@property (nonatomic,assign) NSInteger page;
@property (nonatomic,assign) NSInteger limit;

@property (strong, nonatomic) IBOutlet UIView *tHeader;
@property (weak, nonatomic) IBOutlet UILabel *lb_h_paidouNum;
@property (weak, nonatomic) IBOutlet UILabel *lb_h_zhuan;
@property (weak, nonatomic) IBOutlet UILabel *lb_h_left;
@property (weak, nonatomic) IBOutlet UILabel *lb_h_leftValue;
@end

@implementation ScoreRecordVC

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _tview.separatorInset = UIEdgeInsetsZero;
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.hidesBottomBarWhenPushed = YES;
    _limit = 10;
    _page = 1;
      [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    _array = [NSMutableArray new];
    _arrIn = [NSMutableArray new];
    _arrOut = [NSMutableArray new];
    
    self.navigationItem.titleView = _seg;
    
    [self loadData];
    
    __weak ScoreRecordVC *weakSelf = self;
    [self.tview addInfiniteScrollingWithActionHandler:^{
        weakSelf.page++;
        [weakSelf loadData];
        
    }];
    
    User *user = [[UserManager sharedManager] readFromDisk];
    _lb_h_paidouNum.text = [NSString stringWithFormat:@"%d",user.earned_score];
    _lb_h_zhuan.text = [NSString stringWithFormat:@"¥%@",[Utils fenToYuan:user.earned_score_money]];
    _lb_h_left.text = [NSString stringWithFormat:@"%d",user.score];
    _lb_h_leftValue.text= [NSString stringWithFormat:@"¥%@",[Utils fenToYuan:user.score_money]];
    
    _tHeader.height = 40;
    
    _tHeader.layer.borderWidth = 0.5;
    
    _tHeader.layer.borderColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1].CGColor;
    
    self.tview.tableHeaderView = _tHeader;
    [self cleanFooter:_tview];
    
    
}

- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)loadData{
    if (self.tview.infiniteScrollingView.state == (SVPullToRefreshStateLoading|SVPullToRefreshStateTriggered)) {
        return;
    }
    //uid=xxxx&type=”in|out|all”&limit=20&page=1
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *param = @{
                            @"uid":[[UserManager sharedManager]uid],
                            @"type":@"all",
                            @"page":@(self.page),
                            @"limit":@(self.limit)
                            };
    
    [manager GET:kScoreListURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"] intValue] == 0) {
            NSError *error;
            NSArray *arr = [ScoreLogModel arrayOfModelsFromDictionaries:responseObject[@"score_list"] error:&error];
            if (error) {
                NSLog(@"%@",error);
            }else{
                 [self devideArray:arr];
                
                [_array addObjectsFromArray:[arr copy]];
               
                [_tview reloadData];
                
                int count = [responseObject[@"count"] intValue];
                int total = [responseObject[@"total"] intValue];
                if ( count < self.limit) {
                    self.tview.showsInfiniteScrolling = NO;
                }else if(count > self.limit){
                    self.tview.showsInfiniteScrolling = YES;
                }else if (count == self.limit) {
                    self.tview.showsInfiniteScrolling = total - _array.count > 0 ?
                    YES:NO;
                }
            }
        }
        [self.tview.infiniteScrollingView stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [self.tview.infiniteScrollingView stopAnimating];
    }];
}

//数组拆分
- (void)devideArray:(NSArray *)array {
    if (!_arrIn || !_arrOut) {
        return;
    }
    
    for (ScoreLogModel *m in array) {
        if (m.score > 0) {
            [self.arrIn addObject:m];
        }else if (m.score < 0) {
            [self.arrOut addObject:m];
        }
    }
//    __weak ScoreRecordVC *weakSelf = self;
//    [array enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//      __weak  ScoreLogModel *m = (ScoreLogModel *)obj;
//        if (m.score > 0) {
//            [weakSelf.arrIn addObject:m];
//        }else if (m.score < 0) {
//            [weakSelf.arrOut addObject:m];
//        }
//    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_seg.selectedSegmentIndex == 0) {
        return _arrIn.count;
    }else{
        return _arrOut.count;
    }
    return 0;
   
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellid";
    ScoreRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle ] loadNibNamed:@"ScoreRecordCell" owner:self options:nil] lastObject];
    }
    ScoreLogModel *m = _seg.selectedSegmentIndex == 0? _arrIn[indexPath.row]:_arrOut[indexPath.row];
    [cell setModel:m];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc{
    [_array removeAllObjects];
    _array = nil;
    [_arrIn removeAllObjects];
    _arrIn = nil;
    [_arrOut removeAllObjects];
    _arrOut = nil;
}

- (IBAction)segAct:(UISegmentedControl *)sender {
    if (_arrIn.count || _arrOut.count) {
        [_tview reloadData];
    }
 }
@end
