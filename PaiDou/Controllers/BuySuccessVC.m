//
//  BuySuccessVC.m
//  PaiDou
//
//  Created by JSen on 14/11/17.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BuySuccessVC.h"
#import "OrderSummaryMD.h"
#import "ProductDetailMD.h"
#import "OrderModel.h"
#import "VerifyCodeVC.h"

@interface BuySuccessVC ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *lb_header;
@property (weak, nonatomic) IBOutlet UITableView *tView;

@property (strong, nonatomic) IBOutlet UITableViewCell *cell01;
@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (weak, nonatomic) IBOutlet UILabel *lb_price;
@property (weak, nonatomic) IBOutlet UILabel *lb_haoDou;
@property (strong, nonatomic) IBOutlet UITableViewCell *cell02;
@property (weak, nonatomic) IBOutlet UILabel *_lb_02_name;
@property (strong, nonatomic) IBOutlet UITableViewCell *cell03;
@property (weak, nonatomic) IBOutlet UILabel *lb_03;
@property (strong, nonatomic) IBOutlet UITableViewCell *cell04;
@property (weak, nonatomic) IBOutlet UILabel *lb_04;
@property (strong, nonatomic) IBOutlet UITableViewCell *cell05;
@property (strong, nonatomic) IBOutlet UITableViewCell *cell07;
@property (weak, nonatomic) IBOutlet UILabel *lb_07;
@property (strong, nonatomic) IBOutlet UITableViewCell *cell08;
@property (strong, nonatomic) IBOutlet UITableViewCell *cell09;

@property (weak, nonatomic) IBOutlet UILabel *lb_quhuoTime;
@property (weak, nonatomic) IBOutlet UILabel *lb_xuqiu;
@property (strong, nonatomic) IBOutlet UIView *footerView;
//查看兑换券
- (IBAction)btn_quan:(UIButton *)sender;
//继续兑换
- (IBAction)btn_goon:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *m_btn_quan;
@property (weak, nonatomic) IBOutlet UIButton *m_btn_goon;

@property (nonatomic,retain) OrderModel *dOrder;
@end

@implementation BuySuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView =[Utils titleLabelWithTitle:@"交易详情"];
    
    _headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 60);
    _lb_header.text = [NSString stringWithFormat:@"支付成功！获%d派豆",_orderModel.award_score];
    _tView.tableHeaderView = _headerView;
    
    _footerView.size = CGSizeMake(SCREEN_WIDTH, 100);
    _tView.tableFooterView = _footerView;
    [_m_btn_quan setRoundedCornerWithRadius:2];
    [_m_btn_goon setRoundedCornerWithRadius:2];
    
    _lb_name.text = [NSString stringWithFormat:@"%@ x %d",_orderModel.name,_orderModel.count];
    _lb_price.text = [NSString stringWithFormat:@"¥%@",[Utils fenToYuan:_orderModel.total_price]];
    _lb_haoDou.text = [NSString stringWithFormat:@"%d", _orderModel.total_score];
    __lb_02_name.text = [NSString stringWithFormat:@"商家名称:%@",_productModel.biz.name];
    
    NSString *payMethod = nil;
    if ([_orderModel.pay_channel isEqualToString:@"alipay"]) {
        payMethod = @"支付宝支付";
    }else if ([_orderModel.pay_channel isEqualToString:@"wxpay"]) {
        payMethod = @"微信支付";
    }
    _lb_03.text = [NSString stringWithFormat:@"支付方式 : %@",(payMethod.length ? payMethod:@"待定")];
    _lb_04.text = [NSString stringWithFormat:@"交易时间: %@",[Utils dateFromTimeInterval:_orderModel.create_time]];
    
    NSInteger dous = [[Utils moneyToDou:_orderModel.total_price] intValue];
    _lb_07.text = [NSString stringWithFormat:@"立返%d派豆，价值¥%@",dous,[Utils douToYuan:dous]];
    
    [self loadOrderDetail];
    
    if (_timeStr.length && _xuqiuStr.length) {
        _lb_xuqiu.text = _xuqiuStr;
        _lb_quhuoTime.text = _timeStr;
    }
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
// Do any additional setup after loading the view from its nib.
}

- (void)back{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)loadOrderDetail{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *param = @{
                            @"oid":_orderModel.oid,
                            @"uid":[[UserManager sharedManager]uid]
                            };
    [manager GET:kGetOrderDetailURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue] == 0) {
            NSError *error = nil;
            _dOrder = [[OrderModel alloc] initWithDictionary:responseObject[@"order_info"] error:&error];
            if (error) {
                NSLog(@"%@",error);
            }else{
                [self refreshContent:_dOrder];
                NSIndexPath *index = [NSIndexPath indexPathForRow:5 inSection:0];
                [_tView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
    
    
}

- (void)refreshContent:(OrderModel *)model{
    NSString *payMethod = nil;
    if ([model.pay_channel isEqualToString:@"alipay"]) {
        payMethod = @"支付宝支付";
    }else if ([model.pay_channel isEqualToString:@"wxpay"]) {
        payMethod = @"微信支付";
    }
    _lb_03.text = [NSString stringWithFormat:@"支付方式 : %@",(payMethod.length ? payMethod:@"待定")];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 7;
    }else{
        return 2;
    }
    return 0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
            {
                return _cell01;
            }
            case 1:{
                return _cell02;
            }
            case 2:{
                return _cell03;
            }
            case 3:{
                return _cell04;
            }
            case 4:{
                return _cell05;
            }
            case 5:{
                //兑换码
                UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
                if (_dOrder == nil) {
                    return cell;
                }
                
                [_dOrder.verify_codes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    UserVerifyCode *oneCode = (UserVerifyCode *)obj;
                    UILabel *label = [self labelWithCode:oneCode index:idx];
                    [cell.contentView addSubview:label];

                }];
                return cell;
                
                
            }
                
            case 6:{
                return _cell07;
            }
                
                
            default:return nil;
                break;
        }
        
        
    }else if (indexPath.section ==1 ) {
        if (indexPath.row == 0) {
            return _cell08;
        }else if (indexPath.row == 1) {
            return _cell09;
        }
        
    }
    return nil;
}

- (UILabel *)labelWithCode:(UserVerifyCode *)oneCode index:(NSInteger)idx{
    CGRect rect = CGRectMake(5, idx *20+3, SCREEN_WIDTH-10, 20);
    UILabel *lb = [[UILabel alloc] initWithFrame:rect];
    lb.backgroundColor = [UIColor clearColor];
    lb.font = [UIFont systemFontOfSize:13];
    lb.textColor = [UIColor appGrayTextColor];
    lb.text = [NSString stringWithFormat:@"兑换码: %@",oneCode.code];
    return lb;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 70;
        }else if ( indexPath.row == 5) {
            //刷新兑换码
            //计算高度
            if (_dOrder) {
                return _dOrder.verify_codes.count*20+5+5;
            }else{
                return 10;
            }
            
        }
        
        
    }
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_quan:(UIButton *)sender {
    VerifyCodeVC *vc = [[VerifyCodeVC alloc ]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btn_goon:(UIButton *)sender {
    UIViewController *vc = self.navigationController.viewControllers[1];
    [self.navigationController popToViewController:vc animated:YES];
}
@end
