//
//  VerifyCodeCell.h
//  PaiDou
//
//  Created by JSen on 14/11/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserVerifyCode;

@interface VerifyCodeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *lb_shopname;
@property (weak, nonatomic) IBOutlet UILabel *lb_status;
@property (weak, nonatomic) IBOutlet UILabel *lb_duiHuanMa;
@property (weak, nonatomic) IBOutlet UILabel *lb_endTime;

- (void)setModel:(UserVerifyCode *)model;
@end
