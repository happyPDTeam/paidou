//
//  OrderDetailVC.m
//  PaiDou
//
//  Created by JSen on 14/11/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "OrderDetailVC.h"
#import "MyOrdersCell.h"
#import "OrderSummaryMD.h"
#import "OrderModel.h"
@interface OrderDetailVC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,retain) OrderModel *dOrder;

@property (strong, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UITableView *tview;
@property (strong, nonatomic) IBOutlet UITableViewCell *endTimeCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_endTime;

@property (retain,nonatomic) NSArray *dArray;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *btn_pay;

- (IBAction)btn_pay_act:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lb_title;
@end

@implementation OrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dArray = @[
                @"订单详情",
                [NSString stringWithFormat:@"商品名称 : %@",_order.name],
                [NSString stringWithFormat:@"购买数量 : %d",_order.count],
                [NSString stringWithFormat:@"总价 : ¥%@ + %d豆",[Utils fenToYuan:_order.total_price],_order.total_score]
                ];
    _lb_endTime.text = [NSString stringWithFormat:@"%@",[Utils dateFromTimeInterval:_order.create_time]];
    
    if ( ![_order.status isEqualToString:kOrderStatusWait_for_pay] ) {
        //只要是付过款的都有兑换码
        [self loadOrderDetail];
    }
    
    //去掉
//    if ([_order.status isEqualToString:kOrderStatusWait_for_pay]) {
//        //添加立即支付按钮
//        [self addFooterView];
//    }
    if ([_order.status isEqualToString:kOrderStatusRefunding]) {
        [self addHeaderView];
    }
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"订单详情"];
    
    [self cleanFooter:_tview];
    
    self.tview.separatorColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1];
    // Do any additional setup after loading the view from its nib.
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addFooterView{
    self.tview.tableFooterView = _footerView;
}
- (void)addHeaderView{
    self.tview.tableHeaderView = _headView;
    _lb_title.text = @"退单申请中......";
}
- (void)loadOrderDetail{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *param = @{
                            @"oid":_order.oid,
                            @"uid":[[UserManager sharedManager]uid]
                            };
    [manager GET:kGetOrderDetailURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue] == 0) {
            NSError *error = nil;
            _dOrder = [[OrderModel alloc] initWithDictionary:responseObject[@"order_info"] error:&error];
            if (error) {
                NSLog(@"%@",error);
            }
            if (_dOrder) {
                [self refresh];
            }
//            NSIndexPath *index = [NSIndexPath indexPathForRow:5 inSection:0];
//            [_tview reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
    
    
}

- (void)refresh{
    NSIndexPath *index = [NSIndexPath indexPathForRow:4 inSection:1];
    [_tview reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }else if (section == 1) {
        return 5;
    }
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            MyOrdersCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"MyOrdersCell" owner:self options:nil] lastObject];
            [cell setModel:_order];
            return cell;
        }else if(indexPath.row == 1) {
            return _endTimeCell;
        }
    }
    else if (indexPath.section == 1) {
        
        if (indexPath.row >= 0 && indexPath.row <= 3) {
            UITableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"OrderDetailExCell" owner:self options:nil] firstObject];
            UILabel *label = (UILabel *)[cell.contentView viewWithTag:100];
            label.text = _dArray[indexPath.row];
            if (indexPath.row == 0) {
                label.textColor = [UIColor appBlackTextColor];
            }else{
                label.textColor = [UIColor appGrayTextColor];
            }
            return cell;
        }
        if (indexPath.row == 4) {
            
            if ( ![_order.status isEqualToString:kOrderStatusWait_for_pay]) {
                
                //有兑换码
                UITableViewCell *cell = [[UITableViewCell alloc  ] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
                if (_dOrder == nil) {
                    return cell;
                }
                
                [_dOrder.verify_codes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    UserVerifyCode *oneCode = (UserVerifyCode *)obj;
                    UILabel *label = [self labelWithCode:oneCode index:idx];
                    [cell.contentView addSubview:label];
                    
                }];
                return cell;

            }else{
                //未支付,无兑换码
                UITableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"OrderDetailExCell" owner:self options:nil] lastObject];
                UILabel *label = (UILabel *)[cell.contentView viewWithTag:200];
                NSString *doushu =[Utils fenToYuan:_order.total_price];
                label.text =[NSString stringWithFormat:@"支付立返%@派豆，价值¥%@", doushu,[Utils douToYuan:[doushu intValue]]];
                return cell;
            }
        }
    }
    return nil;
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (UILabel *)labelWithCode:(UserVerifyCode *)oneCode index:(NSInteger)idx{
    CGRect rect = CGRectMake(5, idx *20+3, SCREEN_WIDTH-10, 20);
    UILabel *lb = [[UILabel alloc] initWithFrame:rect];
    lb.backgroundColor = [UIColor clearColor];
    lb.font = [UIFont systemFontOfSize:18];
    lb.textColor = [UIColor appBlueTextColor];
    lb.text = [NSString stringWithFormat:@"兑换码: %@",oneCode.code];
    return lb;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return  indexPath.row == 0 ? 100:30;
    }else if (indexPath.section == 1) {
        if (indexPath.row == 4) {
            if (_dOrder) {
                return _dOrder.verify_codes.count * 20 + 10;
            }
        }
        
        return 44;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  10;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_pay_act:(UIButton *)sender {
}
@end
