//
//  OrderDetailVC.h
//  PaiDou
//
//  Created by JSen on 14/11/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
@class OrderSummaryMD;
@interface OrderDetailVC : BaseViewController

@property (nonatomic, retain) OrderSummaryMD *order;

@end
