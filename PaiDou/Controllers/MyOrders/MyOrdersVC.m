//
//  MyOrdersVC.m
//  PaiDou
//
//  Created by JSen on 14/11/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "MyOrdersVC.h"
#import "MyOrdersCell.h"
#import "OrderDetailVC.h"
#import "VerifyCodeVC.h"
#import "SVPullToRefresh.h"
#import "SegController.h"

@interface MyOrdersVC ()<UITableViewDataSource,UITableViewDelegate>
@property (retain, nonatomic) IBOutlet UITableView *tview;
@property (retain,nonatomic) NSMutableArray *array;
@property (retain,nonatomic) NSMutableArray *arrayPayed;
@property (retain,nonatomic) NSMutableArray *arrayUnpayed;
@property (retain,nonatomic) NSMutableArray *manageArray;

@property (assign) NSInteger limit;
@property (assign) NSInteger page;
@end

@implementation MyOrdersVC{
    SegController *_segVC;
    UIView *_segView;
    int _currIndex;
}

-(void)viewWillAppear:(BOOL)animated
{
    NSIndexPath * selected = [self.tview indexPathForSelectedRow];
    
    if(selected)
    {
        [self.tview deselectRowAtIndexPath:selected animated:NO];
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
     self.hidesBottomBarWhenPushed = YES;
    _currIndex = 0;
    
    _tview = [[UITableView alloc ]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tview.delegate = self;
    _tview.dataSource = self;
    
    [self.view addSubview:_tview];
    [self.view removeConstraints:_tview.constraints];
    _page = 1;
    _limit = 10;
    
    _array = [NSMutableArray new];
    _arrayPayed = [NSMutableArray new];
    _arrayUnpayed = [NSMutableArray new];
    _manageArray = [@[_array,_arrayPayed,_arrayUnpayed] mutableCopy];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"我的订单"];
   
   //取消兑换券
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"兑换券" style:UIBarButtonItemStylePlain target:self action:@selector(duiHuanQuan:)];
//    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor appWihteColor]} forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem = rightItem;
    
    __weak MyOrdersVC *weakSelf = self;
    [weakSelf.tview addInfiniteScrollingWithActionHandler:^{
         weakSelf.page++;
        [self loadData];
    }];
    [self loadData];
    
    [self cleanFooter:_tview];
    
    _segVC = [[SegController alloc ]initWithSegmentWithTitles:@"全部订单",@"已支付",@"未支付", nil];
    _segView = _segVC.view;
    [_segVC handleClick:^(int index, UIView *view) {
        _currIndex = index;
        [_tview reloadData];
    }];
    [self.view addSubview:_segView];
    
    _segView.translatesAutoresizingMaskIntoConstraints = NO;
    _tview.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    
    NSDictionary *viewDict = NSDictionaryOfVariableBindings(_segView,_tview);
    
    NSArray *hArrSegView = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_segView]-0-|" options:0 metrics:nil views:viewDict];
    NSArray *hArrTView = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_tview]-0-|" options:0 metrics:nil views:viewDict];
    NSArray *vArr = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_segView(40)]-0-[_tview]-0-|" options:0 metrics:nil views:viewDict];
    [self.view addConstraints:hArrSegView];
    [self.view addConstraints:hArrTView];
    [self.view addConstraints:vArr];
   
}

//- (void)duiHuanQuan:(UIBarButtonItem *)itme{
//    VerifyCodeVC *vc = [[VerifyCodeVC alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
//}

- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)loadData {
    if (self.tview.infiniteScrollingView.state == (SVPullToRefreshStateLoading|SVPullToRefreshStateTriggered)) {
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    User *user = [[UserManager sharedManager] readFromDisk];
    //@"sdddd"
    NSDictionary *param = @{
                            @"uid":user.uid	,
                            @"page":@(self.page),
                            @"limit":@(self.limit)
                            
                            };
    //uid status limit page
    [manager GET:kGetUserOrdersURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"] intValue] == 0 && [responseObject[@"count"] intValue] > 0) {
            NSError *error ;
            NSArray *arr = [OrderSummaryMD arrayOfModelsFromDictionaries:responseObject[@"orders_list"] error:&error];
            if (error) {
                NSLog(@"%@",error);
                return ;
            }
            
            
                [ _array addObjectsFromArray:arr];
//                [_tview reloadData];
            
                 [self p_devideModels:_array];
            
                int count = [responseObject[@"count"] intValue];
                int total = [responseObject[@"total"] intValue];
                
                if ( count < self.limit) {
                    self.tview.showsInfiniteScrolling = NO;
                }else if(count > self.limit){
                    self.tview.showsInfiniteScrolling = YES;
                }else if (count == self.limit) {
                    self.tview.showsInfiniteScrolling = total - _array.count > 0 ?
                    YES:NO;
                }
            
            
        }
        [self.tview.infiniteScrollingView stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [self.tview.infiniteScrollingView stopAnimating];
    }];
}

- (void)p_devideModels:(NSArray *)array{
    if (array.count == 0) {
        return;
    }
    NSPredicate *preUnpay = [NSPredicate predicateWithFormat:@"status = 'wait_for_pay'"];
    NSArray *unarr = [array filteredArrayUsingPredicate:preUnpay];
    if (_arrayUnpayed.count) {
        [_arrayUnpayed removeAllObjects];
        
    }
    [_arrayUnpayed addObjectsFromArray:unarr];
    
    NSPredicate *prepayed = [NSPredicate predicateWithFormat:@"status = 'payed'"];
    NSArray *payarr = [array filteredArrayUsingPredicate:prepayed];
    if (_arrayPayed.count) {
        [_arrayPayed removeAllObjects];
       
    }
     [_arrayPayed addObjectsFromArray:payarr];
    [_tview reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_manageArray[_currIndex] count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellid";
    MyOrdersCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyOrdersCell" owner:self options:nil] lastObject];
    }
    NSArray *arr = _manageArray[_currIndex];
    [cell setModel:arr[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *arr = _manageArray[indexPath.row];
    OrderSummaryMD *order = arr[indexPath.row];
    
    OrderDetailVC *od = [[OrderDetailVC alloc] init];
    od.order = order;
    [self.navigationController pushViewController:od animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
