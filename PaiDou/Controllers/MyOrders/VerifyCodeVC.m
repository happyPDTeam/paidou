//
//  VerifyCodeVC.m
//  PaiDou
//
//  Created by JSen on 14/11/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "VerifyCodeVC.h"
#import "UserVerifyCode.h"
#import "VerifyCodeCell.h"
#import "SVPullToRefresh.h"
#import "QuickResponseCodeVC.h"

@interface VerifyCodeVC ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *_arraySource;
}

@property (weak, nonatomic) IBOutlet UITableView *tview;
@property (assign) NSInteger limit;
@property (assign) NSInteger page;
@end

@implementation VerifyCodeVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    NSIndexPath * selected = [self.tview indexPathForSelectedRow];
    
    if(selected)
    {
        [self.tview deselectRowAtIndexPath:selected animated:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _page = 1;
    _limit = 10;
    _arraySource = [NSMutableArray new];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"我的兑换码"];
    UIBarButtonItem *leftItem = [Utils leftbuttonItemWithImage:@"nav_left.png" highlightedImage:nil target:self action:@selector(backClick:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    __weak VerifyCodeVC *weakSelf = self;
    [weakSelf.tview addInfiniteScrollingWithActionHandler:^{
        weakSelf.page++;
        [self loadData];
    }];
    [self loadData];
    
    [self cleanFooter:_tview];
    // Do any additional setup after loading the view from its nib.
}

- (void)loadData{
    if (self.tview.infiniteScrollingView.state == (SVPullToRefreshStateLoading|SVPullToRefreshStateTriggered)) {
        return;
    }

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    if (![UserManager sharedManager].hasLogin) {
        return;
    }
    
    NSDictionary *param = @{
                            @"uid":[UserManager sharedManager].uid,
                            @"page":@(self.page) ,
                            @"limit":@(self.limit),
                            @"status":@"consumed|unconsumed"
                            };
    
    [manager GET:kGetVerifyCodesURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"err_code"] intValue] == 0) {
            
            NSArray *arr = [UserVerifyCode arrayOfModelsFromDictionaries:responseObject[@"verify_codes"]];
            [_arraySource addObjectsFromArray:arr];
            [self.tview reloadData];
            
            
            //[self addToSourceArray:responseObject[@"verify_codes"]];
            
            
            int count = [responseObject[@"count"] intValue];
            int total = [responseObject[@"total"] intValue];
            
            if ( count < self.limit) {
                self.tview.showsInfiniteScrolling = NO;
            }else if(count > self.limit){
                self.tview.showsInfiniteScrolling = YES;
            }else if (count == self.limit) {
                self.tview.showsInfiniteScrolling = total - _arraySource.count > 0 ?
                YES:NO;
            }
        }else{
            [self showHUDText:responseObject[@"err_msg"] xOffset:0 yOffset:0];
        }
        [self.tview.infiniteScrollingView stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [self.tview.infiniteScrollingView stopAnimating];
    }];
    
}
- (void)backClick:(UIBarButtonItem *)item {
    [self .navigationController popViewControllerAnimated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arraySource.count;
}

-(void)addToSourceArray:(NSArray *)array
{
    for (NSDictionary * dic in array) {
        UserVerifyCode * model = [[UserVerifyCode alloc] init];
        
        NSMutableDictionary * mdic = [NSMutableDictionary dictionaryWithDictionary:dic];
        
        if (!mdic[@"redeem_time"]) {
            [mdic setValue:@"0" forKey:@"redeem_time"];
        }
        
        if (mdic[@"redeem_end_time"] == 0) {
            [mdic setValue:[NSNumber numberWithInt:1] forKey:@"redeem_end_time"];
        }
        
        [model setValuesForKeysWithDictionary:mdic];
        
        [_arraySource addObject:model];
    }
    [_tview reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellid";
    VerifyCodeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"VerifyCodeCell" owner:self options:nil] lastObject];
    }
    [cell setModel:_arraySource[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    QuickResponseCodeVC * vc = [[QuickResponseCodeVC alloc] init];
    
    UserVerifyCode * model = _arraySource[indexPath.row];
    
    vc.codeNumber = model.code;
    
    vc.shopName = model.biz_name;
    
    [self.navigationController pushViewController:vc animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
