//
//  VerifyCodeCell.m
//  PaiDou
//
//  Created by JSen on 14/11/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "VerifyCodeCell.h"
#import "UserVerifyCode.h"
@implementation VerifyCodeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(UserVerifyCode *)model{
    _lb_name.text = model.product_name;
    [_icon sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:kHolderImage];
    _lb_shopname.text = model.biz_name;
    _lb_duiHuanMa.text = model.code;
    _lb_endTime.text =[NSString stringWithFormat:@"截止时间:%@",[Utils dateFromTimeInterval:model.redeem_end_time]];
    _lb_status.text = [Utils orderStatusToChinese:model.status];
    
    UIColor *color = nil;
    NSString *status = nil;
    if ([model.status isEqualToString:kCodeStatusConsumed]) {
        color = [UIColor appGrayTextColor];
        status = @"已使用";
    }else if ([model.status isEqualToString:kCodeStatusUnconsumed]) {
        color = [UIColor appBlueTextColor];
        status = @"未使用";
    }else if ([model.status isEqualToString:kCodeStatusRefunded]){
        color = [UIColor appGreenColor];
        status = @"已退单";
    }else if ([model.status isEqualToString:kCodeStatusRefunding]){
        color = [UIColor appRedTextColor];
        status = @"退单中";
    }
    _lb_status.textColor = color;
    _lb_status.text = status;
}

@end
