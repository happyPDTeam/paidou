//
//  MessageVC.m
//  PaiDou
//
//  Created by JSen on 14/12/26.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "MessageVC.h"
#import "MessageCell.h"
#import "MessageModel.h"

@interface MessageVC ()<UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *_array;
}

@property (weak, nonatomic) IBOutlet UITableView *tview;
@end

@implementation MessageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _array = [NSMutableArray new];
    
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)loadData{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *param = @{
                            @"uid":[[UserManager sharedManager]uid]
                            };
    [manager GET:kGetPushMsgURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue] == 0) {
            NSError *error = nil;
            NSArray *arr = [MessageModel arrayOfModelsFromDictionaries:responseObject[@"infos"] error:&error];
            if (error) {
                NSLog(@"%@",error);
            }else{
                [_array addObjectsFromArray:arr];
                [_tview reloadData];
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _array.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellid = @"cell";
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil] lastObject];
    }
    [cell setModel:_array[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    CGFloat height = [MessageModel getHeight:_array[indexPath.row]];
    MessageModel *model = _array[indexPath.row];
    
    UITextView *tv = [[UITextView alloc ]initWithFrame:CGRectZero];
    tv.text =model.content;
    CGSize size = [tv sizeThatFits:CGSizeMake(SCREEN_WIDTH, MAXFLOAT)];
    return size.height+20;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
