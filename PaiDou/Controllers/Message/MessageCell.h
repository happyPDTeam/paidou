//
//  MessageCell.h
//  PaiDou
//
//  Created by JSen on 14/12/26.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MessageModel;
@interface MessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *lb_time;


- (void)setModel:(MessageModel *)model;


@end
