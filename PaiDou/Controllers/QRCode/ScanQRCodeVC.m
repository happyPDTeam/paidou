//
//  ScanQRCodeVC.m
//  wft
//
//  Created by JSen on 14/10/9.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ScanQRCodeVC.h"
#import "QRPayViewController.h"
#import "BaseNavigationController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVCaptureDevice.h>
#import "UIAlertView+Blocks.h"
#import "GuideView.h"
#import "ClickableUIImageView.h"
@interface ScanQRCodeVC ()
{
    GuideView * _guideView;
    ClickableUIImageView *_overlayIView;
    
    BOOL * _isClicked;
}

@end

static NSString *const kScanQRCodeNotFirstShow = @"the page is not first appear???";

@implementation ScanQRCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.view.backgroundColor = [UIColor grayColor];
    UIButton * scanButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [scanButton setTitle:@"取消" forState:UIControlStateNormal];
    scanButton.frame = CGRectMake(SCREEN_WIDTH - 60,30, 50, 40);
//    [scanButton setBackgroundImage:[UIImage JSenImageNamed:@"btn_bg_normal.png"] forState:UIControlStateNormal];
    [scanButton setTitleColor:[UIColor appWihteColor] forState:UIControlStateNormal];
    [scanButton setBackgroundColor:[UIColor clearColor]];
    
    [scanButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
   // [self.view addSubview:scanButton];
    
    UILabel * labIntroudction= [[UILabel alloc] initWithFrame:CGRectMake(15, 40, 290, 50)];
    labIntroudction.backgroundColor = [UIColor clearColor];
    labIntroudction.numberOfLines=2;
    labIntroudction.textColor=[UIColor whiteColor];
    labIntroudction.text=@"将二维码图像置于矩形方框内，离手机摄像头10CM左右，系统会自动识别。";
    //[self.view addSubview:labIntroudction];
    
    

    

    
    AVAuthorizationStatus status =[AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (status == AVAuthorizationStatusDenied) {
        RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定"];
            UIAlertView *alert = [[UIAlertView alloc ]initWithTitle:@"提示" message:@"请前往 设置 修改摄像头权限" cancelButtonItem:nil otherButtonItems:sure, nil];
        [alert show];
        return;
    }
    
    
    
    
    _overlayIView = [[ClickableUIImageView alloc] initWithImage:[UIImage JSenImageNamed:@"sao.png"]];
    _overlayIView.size = CGSizeMake(SCREEN_WIDTH-20, SCREEN_HEIGHT*0.5);
    _overlayIView.center = self.view.center;
    [_overlayIView handleComplemetionBlock:^(ClickableUIImageView *view) {
        view.hidden = !view.hidden;
        UIButton *btn = (UIButton*)[self.view viewWithTag:999];
        btn.selected = !btn.selected;
        
    }];
    _overlayIView.hidden = YES;
    [self.navigationController.view addSubview:_overlayIView];
    
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:_overlayIView.frame];
    imageView.image = [UIImage imageNamed:@"pick_bg"];
    [self.view addSubview:imageView];
    
    upOrdown = NO;
    num =0;
    _line = [[UIImageView alloc] initWithFrame:CGRectMake(_overlayIView.left, _overlayIView.top, _overlayIView.width, 2)];
    _line.image = [UIImage imageNamed:@"scanLine.png"];
    [self.view addSubview:_line];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.02 target:self selector:@selector(animation1) userInfo:nil repeats:YES];
    
    BOOL notFirstShow = [[NSUserDefaults standardUserDefaults ]boolForKey:kScanQRCodeNotFirstShow];
    if (!notFirstShow) {
        _overlayIView.hidden = NO;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kScanQRCodeNotFirstShow];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self setupCamera];
    
    [self createButton];
}

-(void)createButton
{
    UIButton * button = [ViewManager createButton:CGRectMake(SCREEN_WIDTH - 51, 50, 36, 36) bgImage:@"help" image:@"heip_hov" title:nil method:@selector(click:) target:self];
    button.tag = 999;
    [button setImage:[UIImage imageNamed:@"del_hov"] forState:UIControlStateHighlighted];
    
    [button setImage:[UIImage imageNamed:@"del_hov"] forState:UIControlStateSelected];
    
    [self.view addSubview:button];
}

-(void)animation1
{
    if (upOrdown == NO) {
        num ++;
        _line.frame = CGRectMake(_overlayIView.left, _overlayIView.top+2*num, _overlayIView.width, 2);
        if (2*num == _overlayIView.height) {
            upOrdown = YES;
        }
    }
    else {
        num --;
        _line.frame = CGRectMake(_overlayIView.left, _overlayIView.top+2*num, _overlayIView.width, 2);
        if (num == 0) {
            upOrdown = NO;
        }
    }
    
}
-(void)backAction
{
    
//    [self dismissViewControllerAnimated:YES completion:^{
//        [timer invalidate];
//        timer = nil;
//    }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [timer fire];
    if (timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:.02 target:self selector:@selector(animation1) userInfo:nil repeats:YES];
    }
    UIButton *btn = (UIButton *)[self.view viewWithTag:999];
    btn.selected = !_overlayIView.hidden;
    // Start
    [_session startRunning];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_session stopRunning];
    [timer invalidate];
    timer = nil;
}
- (void)setupCamera
{
    // Device
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Input
    _input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    
    // Output
    _output = [[AVCaptureMetadataOutput alloc]init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    // Session
    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    if ([_session canAddInput:self.input])
    {
        [_session addInput:self.input];
    }
    
    if ([_session canAddOutput:self.output])
    {
        [_session addOutput:self.output];
    }
    
    // 条码类型 AVMetadataObjectTypeQRCode
    _output.metadataObjectTypes =@[AVMetadataObjectTypeQRCode];
    
    // Preview
    _preview =[AVCaptureVideoPreviewLayer layerWithSession:self.session];
    _preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
//    _preview.frame = [_preview convertRect:CGRectMake(self.view.center.x, self.view.center.y, SCREEN_WIDTH-20, SCREEN_HEIGHT*0.5) toLayer:_preview];
//    _preview.frame =CGRectMake(20,110,SCREEN_WIDTH-20*2,SCREEN_WIDTH-20, SCREEN_HEIGHT*0.5);
    _preview.frame = _overlayIView.frame;
    [self.view.layer insertSublayer:self.preview atIndex:0];

    
}
#pragma mark -   摄像头的回掉方法
#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
//    [NSThread sleepForTimeInterval:1];
    
    
    NSString *stringValue;
    
    if ([metadataObjects count] >0)
    {
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
    }
    NSRange range = [stringValue rangeOfString:@"http://app.apaidou.com/r/b/" options:NSCaseInsensitiveSearch|NSLiteralSearch];
    if (range.length != 0) {
        
      [_session stopRunning];
        [timer invalidate];
        //http://paidou-media.stor.sinaapp.com/biz/qrcode
        NSLog(@"%@",stringValue);
        
        {
            if (stringValue.length == 0) {
                return ;
            }
            QRPayViewController *qrpay = [[QRPayViewController alloc] init];
            qrpay.bizUrl = stringValue;
            BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:qrpay];
            [self presentViewController:nav animated:NO completion:nil];
        }
        return;
        
        if (_block) {
            _block(stringValue);
        }
//        [self dismissViewControllerAnimated:YES completion:^
//         {
//             
//             
//             
//         }];
        
    }else{
        MBProgressHUD  *_hud = nil;
        _hud = [MBProgressHUD HUDForView:self.view];
        if (_hud == nil) {
             _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            _hud.mode = MBProgressHUDModeText;
            _hud.userInteractionEnabled = NO;
            _hud.margin = 10;
            _hud.labelText = @"暂不支持该二维码";
            _hud.xOffset = 0;
            _hud.yOffset = 130;
            _hud.minShowTime = 2;
            [_hud show:YES];
            [_hud hide:YES afterDelay:2];
        }else{
            [_hud show:YES];
            [_hud hide:YES afterDelay:2];
        }
        
       
    }
   
   
}

-(void)click:(UIButton *)button
{
    _overlayIView.hidden = !_overlayIView.hidden;
    
    button.selected = !button.selected;
//    AppDelegate * del = [[UIApplication sharedApplication]delegate];
//    
//    _guideView = [[GuideView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) array:@[@"引导页1.jpg"]];
//    
//    [_guideView becomeFirstResponder];
//    
//    [del.window addSubview:_guideView];
}

- (void)handleFinish:(void (^)(NSString *))block {
    _block  = block;
}
@end
