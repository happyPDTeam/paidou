//
//  QRPaySuccessVC.h
//  PaiDou
//
//  Created by JSen on 14/11/25.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
@class OrderSummaryMD;
@interface QRPaySuccessVC : BaseViewController

@property (nonatomic, retain) OrderSummaryMD *order;

@end
