//
//  QRPaySuccessVC.m
//  PaiDou
//
//  Created by JSen on 14/11/25.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "QRPaySuccessVC.h"
#import "OrderModel.h"
#import "OrderSummaryMD.h"
@interface QRPaySuccessVC ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tview;
@property (strong, nonatomic) IBOutlet UITableViewCell *payMethodCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_method;
@property (strong, nonatomic) IBOutlet UITableViewCell *timeCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_time;
@property (strong, nonatomic) IBOutlet UIView *tHeader;
@property (weak, nonatomic) IBOutlet UILabel *lb_header;

@property (strong, nonatomic) IBOutlet UITableViewCell *sellCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_seller;

@property (retain,nonatomic) OrderModel *dOrder;
@end

@implementation QRPaySuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];

    _tHeader.height = 50;
    self.tview.tableHeaderView = _tHeader;
    
    _lb_header.text = [NSString stringWithFormat:@"支付成功,支付%@元",[Utils fenToYuan:_order.total_price]];
    
    _lb_seller.text = [NSString stringWithFormat:@"商户名称: %@",_order.biz_name];
    
    _lb_time.text = [NSString stringWithFormat:@"交易时间: %@",[Utils dateFromTimeInterval:_order.create_time]];
    _lb_method.text = [NSString stringWithFormat:@"支付方式: "];
    if (_order) {
        [self loadOrderDetail];
    }
    [self cleanFooter:_tview];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadOrderDetail{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *param = @{
                            @"oid":_order.oid,
                            @"uid":[[UserManager sharedManager]uid]
                            };
    [manager GET:kGetOrderDetailURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue] == 0) {
            NSError *error = nil;
            _dOrder = [[OrderModel alloc] initWithDictionary:responseObject[@"order_info"] error:&error];
            if (error) {
                NSLog(@"%@",error);
            }else{
                [self refreshContent:_dOrder];
             
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
    
    
}

- (void)refreshContent:(OrderModel *)model{
    if ([model.pay_channel isEqualToString:@"alipay"]) {
        _lb_method.text = @"支付方式：支付宝支付";
    }else if ([model.pay_channel isEqualToString:@"wxpay"]) {
         _lb_method.text = @"支付方式：微信支付";
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return _sellCell;
    }else if (indexPath.row == 1) {
        return _payMethodCell;
    }else if (indexPath.row == 2) {
        return _timeCell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
