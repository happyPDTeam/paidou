//
//  QRPayViewController.m
//  PaiDou
//
//  Created by JSen on 14/11/21.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "QRPayViewController.h"
#import "CustomStepper.h"
#import "SellerDetailMD.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "TextFieldValidator.h"
#import "CommPay.h"
#import "OrderSummaryMD.h"
#import "QRPaySuccessVC.h"
#import "WXApi.h"

@interface QRPayViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tview;
@property (retain,nonatomic) NSMutableDictionary *orderInfoDict;
@property (retain,nonatomic) OrderSummaryMD *order;

//@property (retain,nonatomic)CustomStepper *stepper;

@property (strong, nonatomic) IBOutlet UITableViewCell *nameCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (strong, nonatomic) IBOutlet UITableViewCell *oriPrice;
@property (weak, nonatomic) IBOutlet TextFieldValidator *tf_price;
- (IBAction)tf_changed:(TextFieldValidator *)sender;
@property (strong, nonatomic) IBOutlet UITableViewCell *numCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *swCell;
@property (weak, nonatomic) IBOutlet UISwitch *sw;
@property (weak, nonatomic) IBOutlet UILabel *lb_des;
- (IBAction)sw_act:(UISwitch *)sender;
@property (strong, nonatomic) IBOutlet UITableViewCell *payMethodCell;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btns_pay;
- (IBAction)btn_pay_act:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITableViewCell *lastPriCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_last_price;

@property (retain,nonatomic) SellerDetailMD *sellerDetailMd;

@property (copy,nonatomic) NSString *qr_bid;


@end

@implementation QRPayViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    
    User *user = [[UserManager sharedManager] readFromDisk];
    if ([[UserManager sharedManager] hasLogin]) {
        //截取bid
        NSRange range = [_bizUrl rangeOfString:@"/" options:NSBackwardsSearch|NSLiteralSearch];
        if (range.length) {
            NSString *bid = [_bizUrl substringFromIndex:range.location+1];
            if (bid.length) {
                _qr_bid = bid;
                [self loadData:bid];
            }
        }
        
        
    }else{
        RIButtonItem *itemCalcel = [RIButtonItem itemWithLabel:@"取消" action:^{
            [self back:nil];
        }];
        RIButtonItem *itemSure = [RIButtonItem itemWithLabel:@"确定" action:^{
            LoginViewController *login = [[LoginViewController alloc] init];
            BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:login];
            [self presentViewController:nav animated:YES completion:nil];
        }];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录" cancelButtonItem:itemCalcel otherButtonItems:itemSure, nil];
        [alert show];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    _stepper = [[CustomStepper alloc] init];
//    UIView *calcView = [_stepper getCustomStepper];
//    
//    calcView.origin = CGPointMake(SCREEN_WIDTH-130, 7);
//    [_numCell.contentView addSubview:calcView];
//    
//    
//    [_stepper textDidChanged:^(int number) {
//       
//        [self change:number];
//        
//    }];
    
    // http://app.apaidou.com/r/b/B3A8C4BEFE68496A521564C74B1110E43
    
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"确认支付"];
     [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
     [self _addTableFooterView];
    
    for (UIButton *btn in _btns_pay) {
        if (btn.tag == 100) {
            btn.selected = YES;
        }
        
        
    }
   
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notiAction) name:kSafePaySuccessNotificatino object:nil];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - 支付通知
- (void)notiAction{
    QRPaySuccessVC *pay = [[QRPaySuccessVC alloc] init];
    
    pay.order = _order;
    [self.navigationController pushViewController:pay animated:YES];
}

- (void)back:(UIBarButtonItem *)item{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)loadData:(NSString *)bid{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    hud.minShowTime = 1;
    hud.userInteractionEnabled = NO;
    hud.labelText= @"正在获取商家信息...";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *param = @{
                            @"bid":bid
                            };
    
    [manager GET:kBizsDetailURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        hud.hidden = YES;
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"] intValue] == 0 && ![responseObject[@"biz"] isKindOfClass:[NSNull class]]) {
            [self _refreshContent:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         hud.hidden = YES;
    }];
}

#pragma mark - refresh content
- (void)_refreshContent:(NSDictionary *)dict{
   
    NSError *error;
    _sellerDetailMd = [[SellerDetailMD alloc] initWithDictionary:dict[@"biz"] error:&error];
    if (error) {
        NSLog(@"%@",error);
        return;
    }
    _lb_name.text = _sellerDetailMd.name;
    
    //策略-- maxScore > myScore 显示myscore
    // maxScore <= myScore 显示maxScore
    int score = 0;
    if (_sellerDetailMd.max_score > [[UserManager sharedManager] score]) {
        score = [[UserManager sharedManager] score];
    }else if (_sellerDetailMd.max_score <= [[UserManager sharedManager]score]){
        score = _sellerDetailMd.max_score;
    }
    NSString *des = [NSString stringWithFormat:@"可使用积分%d,抵用%@元",score,[Utils douToYuan:score]];
    _lb_des.text = des;
    
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }else if (section == 1) {
        return 1;
    }else if (section == 2) {
        return 1;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0) {
                return _nameCell;
            }else if (indexPath.row == 1) {
                return _oriPrice;
            }
        }
        case 1:{
            return _payMethodCell;
        }
        case 2:{
            return _lastPriCell;
        }
            
            
        default:
            return nil;
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.1;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return 70;
    }
    return 44;
}

- (void)_addTableFooterView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tview.width, 50)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(10, 0, view.width-20, view.height);
    NSString *title = @"确认购买";
    [btn setTitleColor:[UIColor appWihteColor] forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    
    [btn setBackgroundImage:[UIImage JSenImageNamed:@"btn_yellow.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buyClick:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    
    [view addSubview:btn];
    self.tview.tableFooterView = view;
    
    
}

#pragma mark - tf delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == _tf_price) {
        //删除按钮
        if (string.length == 0) {
            return YES;
        }
       
        
        //最多输入4位整数
        if (textField.text.floatValue <= 999.99) {
            NSMutableString *newtxt = [NSMutableString stringWithString:textField.text];
            [newtxt replaceCharactersInRange:range withString:string];
            return newtxt.length < 6;
            
        }else{
            return NO;
        }
        
        
        
    }
    return YES;
}// return NO to not change text

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}// called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}// called when 'return' key pressed. return NO to ignore

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

#pragma mark - 点击购买
- (void)buyClick:(UIButton *)btn{
    if (![_tf_price validate]) {
        [self showHUDText:@"请输入金额" xOffset:0 yOffset:-20];
        return;
    }
    if (![CustomReachability networkConnectionAvailable]) {
        [self showHUDText:@"网络连接失败,请重试!" xOffset:0 yOffset:0];
        return;
    }
    [self.view endEditing:YES];
    NSLog(@"%@",_tf_price.text);
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.userInteractionEnabled = NO;
    HUD.minShowTime = 1.0f;
    HUD.labelText = @"正在创建订单";
    
    NSString *payType = nil;
    for (UIButton *btn in _btns_pay) {
        if (btn.selected ) {
            payType =  (btn.tag == 100) ? kPayMethodAli:kPayMethodWeiXin;
            
        }
        
    }
    //是否安装微信
    if ([payType isEqualToString:kPayMethodWeiXin]) {
        //
        if (![WXApi isWXAppInstalled]) {
            RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消"];
            RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[WXApi getWXAppInstallUrl]]];
            }];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"请安装微信服务" cancelButtonItem:cancel otherButtonItems:sure, nil];
            [alert show];
            return;
        }
    }
    _orderInfoDict = nil;
    //step 1 生成订单信息
    
    NSDecimalNumber *toFen = [NSDecimalNumber decimalNumberWithString:@"100"];
    
    //用分表示的商品价格
    NSDecimalNumber *fenTF = [NSDecimalNumber decimalNumberWithString:_tf_price.text];
    NSDecimalNumber *fenPrice = [fenTF decimalNumberByMultiplyingBy:toFen];
    
    
    NSDictionary *infoDic = @{
                              @"uid":[[UserManager sharedManager]uid],
                              @"bid":self.qr_bid,
                              @"type":@"scene",
                              @"pid":@"",
                              @"unit_price":@0,
                              @"count":@0,
                              @"total_price":fenPrice,
                              @"unit_score":@0,
                              @"total_score":@0
                              };
    
    _orderInfoDict = [NSMutableDictionary dictionaryWithDictionary:infoDic];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:kCreateOrderURL parameters:_orderInfoDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        HUD.hidden = YES;
        if ([responseObject[@"err_code"]intValue] == 0) {
            HUD.labelText = @"订单创建成功";
            NSError *error = nil;
            OrderSummaryMD *order = [[OrderSummaryMD alloc]initWithDictionary:responseObject[@"order_brief_info"] error:&error];
            if (error) {
                NSLog(@"%@",error);
                return;
            }
            _order = order;
            [CommPay payWithUID:[[UserManager sharedManager]uid] OID:_order.oid payChannel:payType];
            
        }else{
            HUD.labelText = @"创建订单失败";
            
        }
        [HUD hide:YES afterDelay:0.9];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        HUD.labelText = @"操作失败";
        [HUD hide:YES afterDelay:0.9];
    }];

    
}

- (IBAction)sw_act:(UISwitch *)sender {
    
}
- (IBAction)btn_pay_act:(UIButton *)sender {
    
    if (sender.selected) {
        return;
    }
    for (UIButton *oneBtn in _btns_pay) {
        if (sender == oneBtn) {
            oneBtn.selected = YES;
        }else{
            oneBtn.selected = NO;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)tf_changed:(UITextField *)sender {
    NSLog(@"%@",sender.text);
}
//- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view{
//    [_tf_price resignFirstResponder];
//    return YES;
//}
//
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    [_tf_price resignFirstResponder];
//}
@end
