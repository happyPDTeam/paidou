//
//  PurchaseSuccessViewController.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
#import "OrderSummaryMD.h"
#import "ProductDetailMD.h"

@interface PurchaseSuccessViewController : BaseViewController

@property (nonatomic, retain) OrderSummaryMD *orderModel;

@property (nonatomic, retain) ProductDetailMD *productModel;
@end
