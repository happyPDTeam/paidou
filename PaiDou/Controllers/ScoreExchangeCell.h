//
//  ScoreExchangeCell.h
//  PaiDou
//
//  Created by JSen on 14/11/10.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductSummaryMD.h"
#import "StrikeLabel.h"
@interface ScoreExchangeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *_lb_name;
@property (weak, nonatomic) IBOutlet UILabel *_lb_salePrice;
@property (weak, nonatomic) IBOutlet UILabel *_lb_getBackMoney;
@property (weak, nonatomic) IBOutlet UILabel *_lb_needScore;
@property (weak, nonatomic) IBOutlet StrikeLabel *_lb_oriPirce;
@property (weak, nonatomic) IBOutlet UILabel *_lb_duihuanNumber;
@property (weak, nonatomic) IBOutlet UILabel *_lb_endTime;
@property (weak, nonatomic) IBOutlet UILabel *_lb_redeemDes;
@property (weak, nonatomic) IBOutlet UILabel *lb_shopName;

- (void)setModel:(ProductSummaryMD *)model;

@end
