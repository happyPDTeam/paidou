//
//  CommodityDetailVC.h
//  PaiDou
//
//  Created by JSen on 14/11/11.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
@class ProductSummaryMD;

@interface CommodityDetailVC : BaseViewController

@property (nonatomic, strong) ProductSummaryMD *productSummoryMD ;

@property (nonatomic, copy) NSString * releated_id;

@end
