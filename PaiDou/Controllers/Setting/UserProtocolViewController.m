//
//  UserProtocolViewController.m
//  PaiDou
//
//  Created by JSen on 14/11/21.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "UserProtocolViewController.h"

@interface UserProtocolViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@end

@implementation UserProtocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"服务条款"];
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
    // Do any additional setup after loading the view.
    
    _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
