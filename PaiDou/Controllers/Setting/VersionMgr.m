//
//  VersionMgr.m
//  PaiDou
//
//  Created by JSen on 14/11/25.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

/*CFBundleVersion
 (Recommended) The build-version-number string for the bundle. See CFBundleVersion for details.
 
 
 CFBundleShortVersionString
 (Localizable) The release-version-number string for the bundle. See CFBundleShortVersionString for details.
 
 */


//#define BUILD_VERSION [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] intValue]

#import "VersionMgr.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
///upgrade/item?app_id=0

@implementation VersionMgr

+ (instancetype)share{
    static id _s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _s = [[self alloc] init];
    });
    return _s;
}

- (void)checkVersion:(finish)block{
    _block = [block copy];
    
    AppDelegate *del = [[UIApplication sharedApplication] delegate];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:del.window animated:YES];
    hud.userInteractionEnabled = NO;
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager GET:kCheckVersionURL parameters:@{@"app_id":@1} success:^(AFHTTPRequestOperation *operation, id responseObject) {
       NSLog(@"%@",responseObject);
        [hud hide:YES];
        if ([responseObject[@"err_code"]intValue] == 0) {
            NSError *error;
            UpdateInfoModel *model = [[UpdateInfoModel alloc ] initWithDictionary:responseObject[@"info"] error:&error];
            if (error) {
                NSLog(@"%@",error);
            }else{
                [[VersionMgr share] deal:model];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [hud hide:YES];
    }];
}

- (void)deal:(UpdateInfoModel *)model{
    NSLog(@"%d  %@",BUILD_VERSION,LOCAL_VERSION);
    RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消" action:^{
        
    }];
    RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
        NSString *url = nil;
        if (![model.apk_url hasPrefix:@"http://"]) {
            url = [NSString stringWithFormat:@"http://%@",model.apk_url];
        }else{
            url = model.apk_url;
        }
        if (_block) {
            _block(url);
        }
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//        exit(0);
    }];
    if (BUILD_VERSION < model.version_mini) {
        NSString *info = [NSString stringWithFormat:@"您的版本已过时，请更新至最新版本%@ \\n%@",model.version_code,model.upgrade_point];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:info cancelButtonItem:nil otherButtonItems:sure, nil];
        [alert show];
    }else if (BUILD_VERSION >= model.version_mini && BUILD_VERSION <model.version_id) {
        NSString *title = [NSString stringWithFormat:@"最新版本%@",model.version_code];
        NSString *content = [NSString stringWithFormat:@"%@",model.upgrade_point];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:content cancelButtonItem:cancel otherButtonItems:sure, nil];
        [alert show];
    }else if(BUILD_VERSION == model.version_id){
        RIButtonItem *sure02 = [RIButtonItem itemWithLabel:@"确定"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"已经是最新版本啦" cancelButtonItem:nil otherButtonItems:sure02, nil];
        [alert show];
    }
}
@end


@implementation UpdateInfoModel


@end
