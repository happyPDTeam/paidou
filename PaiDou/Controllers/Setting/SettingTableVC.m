//
//  SettingTableVC.m
//  PaiDou
//
//  Created by JSen on 14/11/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "SettingTableVC.h"
#import "VersionMgr.h"
#import "MyWebViewController.h"

@interface SettingTableVC ()

@property (weak, nonatomic) IBOutlet UILabel *lb_verNum;
@end

@implementation SettingTableVC

//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        self.hidesBottomBarWhenPushed = YES;
//    }
//    return self;
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance =NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"设置"];
    
    UIBarButtonItem *leftItem = [Utils leftbuttonItemWithImage:@"nav_left.png" highlightedImage:nil target:self action:@selector(back:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    
    [self cleanFooter:self.tableView];
//    if ([[UserManager sharedManager] hasLogin]) {
//        [self _addTableFooterView];
//    }
    
    
    _lb_verNum.text =LOCAL_VERSION;
    
}

- (void)_addTableFooterView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.width, 80)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(10, 20, view.width-20, 50);
    NSString *title = @"退出当前账号";
    
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor appWihteColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage JSenImageNamed:@"btn_yellow.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(logOff:) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    
    [view addSubview:btn];
    self.tableView.tableFooterView = view;
    
    
}

- (void)logOff:(UIButton *)btn{
    [[UserManager sharedManager] logOff];
//    [[NSNotificationCenter  defaultCenter] postNotificationName:kNotifyNeedReloadTaskList object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cleanFooter: (UITableView *)tableView
{
    UIView *view =[ [UIView alloc]init];
    view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    [tableView setTableFooterView:view];
}


- (void)back:(UIBarButtonItem *)imte{
    
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2) {
        //检查跟新
        [[VersionMgr share] checkVersion:^(NSString *urlStr) {
//            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SettingsStory" bundle:nil];
//            MyWebViewController *vc = (MyWebViewController *)[story instantiateViewControllerWithIdentifier:@"myWebView"];
//            vc.urlString  = urlStr;
//            [self.navigationController pushViewController:vc animated:YES];
//             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
