//
//  VersionMgr.h
//  PaiDou
//
//  Created by JSen on 14/11/25.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^finish)(NSString *urlStr);
@interface VersionMgr : NSObject{
    finish _block;
}

+ (instancetype)share;


- (void)checkVersion:(finish)block;

@end

/*
 "apk_url" = "www.baidu.com";
 "app_id" = 1;
 "create_time" = 1416813466;
 "upgrade_point" = "\U8bf7\U5347\U7ea7";
 "version_code" = "1.2";
 "version_id" = 2;
 "version_mini" = 1;
 */
@interface UpdateInfoModel : JSONModel
@property (nonatomic, copy) NSString<Optional> *apk_url;
@property (nonatomic, assign) NSInteger app_id;
@property (nonatomic, assign) NSInteger create_time;
@property (nonatomic, copy) NSString<Optional> *upgrade_point;
@property (nonatomic, copy) NSString<Optional> *version_code; //展示给用户看的
@property (nonatomic, assign) NSInteger version_id; //内部版本号
@property (nonatomic, assign) NSInteger version_mini;   //最小支持版本号
@end