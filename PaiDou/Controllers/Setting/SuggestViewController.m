//
//  SuggestViewController.m
//  PaiDou
//
//  Created by JSen on 14/11/21.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "SuggestViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"

@interface SuggestViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation SuggestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"意见反馈"];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(sure:)];
    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor appWihteColor]} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
    // Do any additional setup after loading the view.
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sure:(UIBarButtonItem *)item{
    NSString *text = [_textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (text.length == 0) {
        [self showHUDText:@"请输入内容" xOffset:0 yOffset:0];
        return;
    }
    if (![[UserManager sharedManager] hasLogin]) {
        RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消"];
        __weak SuggestViewController *weakSelf = self;
        RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
            LoginViewController *login = [[LoginViewController alloc] init];
            BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:login];
            [weakSelf presentViewController:nav animated:NO completion:nil];
        }];
        UIAlertView *alert = [[UIAlertView alloc ]initWithTitle:@"提示" message:@"请先登录" cancelButtonItem:cancel otherButtonItems:sure, nil];
        [alert show];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    NSDictionary *param = @{
                            @"uid":[[UserManager sharedManager]uid],
                            @"message":_textView.text
                            };
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.userInteractionEnabled = NO;
    
    [manager POST:kFeedbackURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"err_code"] intValue] == 0) {
            hud.mode = MBProgressHUDModeText;
            hud.labelText = @"提交成功!";
        }
        [hud hide:YES afterDelay:0.9f];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         hud.mode = MBProgressHUDModeText;
        hud.labelText = @"提交失败!";
        [hud hide:YES afterDelay:0.9f];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
