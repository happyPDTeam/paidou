//
//  MyWebViewController.h
//  PaiDou
//
//  Created by JSen on 14/12/3.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"

@interface MyWebViewController : BaseViewController

@property (nonatomic, copy) NSString *urlString;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
