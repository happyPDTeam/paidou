//
//  MyWebViewController.m
//  PaiDou
//
//  Created by JSen on 14/12/3.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "MyWebViewController.h"

@interface MyWebViewController ()<UIWebViewDelegate>{
    MBProgressHUD *_hud;
}

@end

@implementation MyWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"升级"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:_urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    
    
    [_webView loadRequest:request];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
    
    // Do any additional setup after loading the view.
}

- (void)back{
    [_hud hide:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    _hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [_hud hide:YES];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    _hud.labelText = @"加载失败";
    [_hud hide:YES afterDelay:2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
