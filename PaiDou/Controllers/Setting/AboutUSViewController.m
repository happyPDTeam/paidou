//
//  AboutUSViewController.m
//  PaiDou
//
//  Created by JSen on 14/11/21.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "AboutUSViewController.h"

@interface AboutUSViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@end

@implementation AboutUSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"关于我们"];
    [self showLeftBackBarbuttonItemWithSelector:@selector(back)];
    _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    // Do any additional setup after loading the view.
    
    
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
