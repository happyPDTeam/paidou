//
//  PrifleVC.m
//  PaiDou
//
//  Created by JSen on 14/11/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "PrifleVC.h"
#import "NicknameVC.h"
#import "PhoneViewController.h"
#import "PassWordVC.h"
#import "UIImageSize.h"
#import <AVFoundation/AVCaptureDevice.h>
@import AVFoundation;

@interface PrifleVC ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tview;

@property (strong, nonatomic) IBOutlet UITableViewCell *iconCell;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (strong, nonatomic) IBOutlet UITableViewCell *nickCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_nick;
@property (strong, nonatomic) IBOutlet UITableViewCell *phoneCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_phone;
@property (strong, nonatomic) IBOutlet UITableViewCell *passCell;

@property (retain,nonatomic) NSArray *array;

@end

@implementation PrifleVC

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    
    NSIndexPath * selected = [self.tview indexPathForSelectedRow];
    
    if(selected)
    {
        [self.tview deselectRowAtIndexPath:selected animated:NO];
    }
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
     self.hidesBottomBarWhenPushed = YES;
    _array = @[_iconCell,_nickCell,_phoneCell, _passCell];
    
    [self cleanFooter:self.tview];
    
    User *user = [[UserManager sharedManager] readFromDisk];
    _lb_phone.text = user.mobile;
    _lb_nick.text = user.name.length ? user.name:user.mobile;
    [_iconImage sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage JSenImageNamed:@"touxiang_holder.png"] options:SDWebImageRefreshCached];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"个人信息"];
    
   // [self _addTableFooterView];
    
}



- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return _array[indexPath.row];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 80;
    }
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self showAlert];
    }else if (indexPath.row == 1) {
        NicknameVC *nick = [[NicknameVC alloc] init];
        [nick finishBlock:^(NSString *string) {
            _lb_nick.text = string;
        }];
        [self.navigationController pushViewController:nick animated:YES];
    }else if (indexPath.row == 2) {
        PhoneViewController *pvc = [[PhoneViewController alloc] init];
        [pvc handleFinish:^(NSString *string) {
            _lb_phone.text = string;
        }];
        [self.navigationController pushViewController:pvc animated:YES];
    }else if (indexPath.row == 3){
        PassWordVC *pass = [[PassWordVC alloc] init];
        [self.navigationController pushViewController:pass animated:YES];
    }
}

- (void)showAlert{
    
    
    
    
    UIActionSheet *s = [[UIActionSheet alloc]initWithTitle:nil delegate:nil cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"本地相册", nil];
    
    [s showInView:self.view withCompletionHandler:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            
              AVAuthorizationStatus status =[AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (status == AVAuthorizationStatusDenied) {
                RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定"];
                UIAlertView *alert = [[UIAlertView alloc ]initWithTitle:@"提示" message:@"请前往 设置 修改摄像头权限" cancelButtonItem:nil otherButtonItems:sure, nil];
                [alert show];
                return ;
            }
            
            //拍摄照片
            // UIImagePickerControllerCameraDeviceRear,
            //UIImagePickerControllerCameraDeviceFront
            BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
            if (!isCamera) {
                NSLog(@"no Camera");
                [self showHUDText:@"摄像头不可用" xOffset:0 yOffset:0];
                return ;
            }
            UIImagePickerController *phoPic = [[UIImagePickerController alloc]init];
            phoPic.sourceType = UIImagePickerControllerSourceTypeCamera;
            phoPic.videoQuality = UIImagePickerControllerQualityTypeLow;
            phoPic.delegate  = self;
            phoPic.allowsEditing = YES;
            
            [self presentViewController:phoPic animated:YES completion:nil];
            
            
        }else if (buttonIndex == 1){
            //本地相册
            UIImagePickerController *pic = [[UIImagePickerController alloc]init];
            pic.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            pic.delegate = self;
            [self presentViewController:pic animated:YES completion:nil];
        }
    }];

}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
   // UIImage *scalImage = [UIImageSize scaleImage:image maxWidth:100 maxHeight:100];
    NSData *data = UIImageJPEGRepresentation(image, 0.1);
    
    NSDictionary *parm = @{
                           @"uid":[[UserManager sharedManager]uid],
                           @"avatar":data
                               
                               };
    [[UserManager sharedManager] update:parm success:^(BOOL isOk) {
        if (isOk) {
            _iconImage.image = image;
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyLoginSuccess object:nil];
        }
        
    }];
    
    [self dismissViewControllerAnimated:YES completion:^(void){
       // [self updateHeadIcon:image];
    }];
}

- (void)updateHeadIcon:(UIImage *)image{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
     NSData *data = UIImageJPEGRepresentation(image, 0.2);
    NSString *str= [NSString stringWithFormat:@"%@?uid=%@",kTestUpdateHeadicon,[[UserManager sharedManager]uid]];
    [manager POST:str parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:data name:@"oneImage" fileName:@"headImage" mimeType:@"image/jpg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue] == 0  ) {
            [self showHUDText:@"修改成功" xOffset:0 yOffset:0];
        [self updateInfo:responseObject];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       
         [self showHUDText:@"修改失败" xOffset:0 yOffset:0];
        NSLog(@"%@",error);
       
    }];
}
//最新信息重新写入文件
- (void)updateInfo:(NSDictionary *)responseObject{
    NSError *error;
    User *updatedUser = [[User alloc]initWithDictionary:responseObject[@"user"] error:&error];
    if (error) {
        NSLog(@"%@",error);
    }else{
        User *user = [[UserManager sharedManager] readFromDisk];
        updatedUser.passWord = user.passWord;
        [[UserManager sharedManager] setLoginUser:updatedUser];
        [UserManager sharedManager].hasLogin  = YES;
    }
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
