//
//  NicknameVC.m
//  PaiDou
//
//  Created by JSen on 14/11/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "NicknameVC.h"

@interface NicknameVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tf;

- (IBAction)tf_end_act:(UITextField *)sender;
@end

@implementation NicknameVC

- (void)finishBlock:(finish)block{
    _block = [block copy];
}

- (void)viewDidLoad {
    [super viewDidLoad];
     User *user = [[UserManager sharedManager] readFromDisk];
    _tf.placeholder = user.name.length ? user.name:@"  请输入昵称";
    
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"修改昵称"];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(save:)];
    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor appWihteColor]} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
   
}

- (void)save:(UIBarButtonItem *)item {
    User *user = [[UserManager sharedManager] readFromDisk];
    if (_tf.text.length == 0) {
        [self showHUDText:@"请输入昵称" xOffset:0 yOffset:0];
        return;
    }
    NSDictionary *param = @{
                            @"uid":[[UserManager sharedManager]uid],
                            @"username":_tf.text
                            };
    [[UserManager sharedManager]update:param success:^(BOOL isOk) {
        if (isOk) {
            if (_block) {
                _block(_tf.text);
            }
        }
    }];
}

- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tf_end_act:(UITextField *)sender {
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}// called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}// called when 'return' key pressed. return NO to ignore.

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_tf resignFirstResponder];
}
@end
