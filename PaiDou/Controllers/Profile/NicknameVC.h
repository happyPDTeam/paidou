//
//  NicknameVC.h
//  PaiDou
//
//  Created by JSen on 14/11/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^finish)(NSString *string);

@interface NicknameVC : BaseViewController
{
    finish _block;
}

- (void)finishBlock:(finish)block;

@end
