//
//  PassWordVC.m
//  PaiDou
//
//  Created by JSen on 14/11/20.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "PassWordVC.h"
#import "TextFieldValidator.h"

@interface PassWordVC ()
@property (weak, nonatomic) IBOutlet TextFieldValidator *tf_old;
@property (weak, nonatomic) IBOutlet TextFieldValidator *tf_new;
@property (weak, nonatomic) IBOutlet TextFieldValidator *tf_new_again;

@end

@implementation PassWordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"修改密码"];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(sure:)];
    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor appWihteColor]} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [_tf_old addRegx:REGEX_PASSWORD withMsg:@"输入6-32位旧密码"];
    
    [_tf_new addRegx:REGEX_PASSWORD withMsg:@"输入6-32位新密码"];
    
    [_tf_new_again addConfirmValidationTo:_tf_new withMsg:@"两次输入密码不一致"];
    
    _tf_old.layer.cornerRadius = 5;
    _tf_old.layer.masksToBounds = YES;
    
    _tf_new.layer.cornerRadius = 5;
    _tf_new.layer.masksToBounds = YES;
    
    _tf_new_again.layer.cornerRadius = 5;
    _tf_new_again.layer.masksToBounds = YES;
    // Do any additional setup after loading the view from its nib.
}

- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sure:(UIBarButtonItem *)imte{
    if (![_tf_old validate] || ![_tf_new validate] || ![_tf_new_again validate]) {
        [self showHUDText:@"请检查输入" xOffset:0 yOffset:0];
        return;
    }
    NSDictionary *param = @{
                            @"uid":[[UserManager sharedManager]uid],
                            @"old_password":_tf_old.text,
                            @"new_password":_tf_new_again.text
                            
                            };
    [[UserManager sharedManager] updatePass:param];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
