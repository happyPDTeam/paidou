//
//  PhoneViewController.h
//  PaiDou
//
//  Created by JSen on 14/11/20.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
typedef void(^finish)(NSString *string);

@interface PhoneViewController : BaseViewController
{
    finish _block;
}
- (void)handleFinish:(finish)block;

@end
