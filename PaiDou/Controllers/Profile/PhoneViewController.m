//
//  PhoneViewController.m
//  PaiDou
//
//  Created by JSen on 14/11/20.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "PhoneViewController.h"
#import "TextFieldValidator.h"



@interface PhoneViewController (){
    NSTimer *_timer;
}

@property (weak, nonatomic) IBOutlet UILabel *lb_pn;
@property (weak, nonatomic) IBOutlet TextFieldValidator *tf_phone;
@property (weak, nonatomic) IBOutlet TextFieldValidator *tf_code;
@property (weak, nonatomic) IBOutlet UIButton *btn_code;
- (IBAction)btn_code_act:(UIButton *)sender;

@end

@implementation PhoneViewController

- (void)handleFinish:(finish)block{
    _block = [block copy];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    User *user = [[UserManager sharedManager] readFromDisk];
    
    _lb_pn.text = [NSString stringWithFormat:@"您的手机号:%@",user.mobile];
    [_btn_code setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"修改手机号"];
    
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(sure:)];
    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor appWihteColor]} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
    
    _btn_code.layer.cornerRadius = 5;
    _btn_code.layer.masksToBounds = YES;
    _tf_code.layer.cornerRadius = 5;
    _tf_code.layer.masksToBounds = YES;
    _tf_phone.layer.cornerRadius = 5;
    _tf_phone.layer.masksToBounds = YES;
    
    
     [_tf_phone addRegx:REGEX_PHONE_DEFAULT withMsg:@"请输入11位手机号"];
    [_tf_code addRegx:REGEX_RANDOMCODE withMsg:@"请输入6位验证码"];
    // Do any additional setup after loading the view from its nib.
}

- (void)sure:(UIBarButtonItem *)imte{
    if (![_tf_phone validate] || ![_tf_code validate]) {
         [self showHUDText:@"请输入11位手机号和6位验证码" xOffset:0 yOffset:0];
        return;
        //先验证 🐴 是否正确🐂🐰🐶🐱🐵
    }
    
    User *user = [[UserManager sharedManager] readFromDisk];
    
    if ([_tf_phone.text isEqualToString:user.mobile]) {
        [self showHUDText:@"请输入其他电话号码" xOffset:0 yOffset:0];
        return;
    }
    
    NSDictionary *paramDic = @{
                               @"mobile":_tf_phone.text,
                               @"sms_code":_tf_code.text,
                               
                               };
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:kVerifyVaildateCodeUrl parameters:paramDic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        int status = [responseObject[@"err_code"]intValue];
        if ( status == 0 ) {
            
            NSDictionary *param = @{
                                    @"uid":user.uid,
                                    @"mobile":_tf_phone.text
                                    };
            [[UserManager sharedManager] update:param success:^(BOOL isOk) {
                if (isOk) {
                    _block(_tf_phone.text);
                }
            }];
//            [self.navigationController pushViewController:vc animated:YES];
            
        }else if (status == 2012){
            //sms code expired
            [self showHUDText:@"验证码超时,请获取新验证码" xOffset:0 yOffset:0];
        }else if (status == 2052){
            //register user exists!
            [self showHUDText:@"用户已注册" xOffset:0 yOffset:0];
        }else{
            [self showHUDText:responseObject[@"err_msg"] xOffset:0 yOffset:0];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",[error description]);
        
    }];

}

- (void)back:(UIBarButtonItem *)item{
    [self stopTimer];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_code_act:(UIButton *)sender {
    if (![_tf_phone validate]) {
        [self showHUDText:@"请输入11位手机号" xOffset:0 yOffset:0];
        return;
    }
    sender.enabled = NO;
    NSDictionary *parmDic = @{@"mobile":_tf_phone.text};
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:kRandomVaildateCodeUrl parameters:parmDic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"obj %@",responseObject);
        if ([responseObject[@"err_code"] integerValue] == 0) {
            
            [self stopTimer];
            _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(_refreshButtonTitle:) userInfo:nil repeats:YES];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",[error description]);
        sender.enabled = YES;
    }];

}

- (void)_refreshButtonTitle:(NSTimer *)timer{
    static int i = 60;
    if (i == 0) {
        _btn_code.enabled = YES;
        _btn_code.titleLabel.text = @"验证码";
        [self stopTimer];
        i = 60;
    }else{
        _btn_code.titleLabel.text = [NSString stringWithFormat:@"%d",i];
        
        i--;
    }
    
    
}
- (void)stopTimer {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}
@end
