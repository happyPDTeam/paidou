//
//  TabSecondViewController.h
//  PaiDou
//
//  Created by JSen on 14/11/6.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
@class Station;

@interface TabSecondViewController : BaseViewController


@property (strong) Station *currStation;

@property (copy, nonatomic) NSArray * bannerArray;

//+(TabSecondViewController *)shared;

@end
