//
//  MyOrdersCell.h
//  wft
//
//  Created by JSen on 14/10/24.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderSummaryMD.h"
@interface MyOrdersCell : UITableViewCell{
OrderSummaryMD *_model;
}

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *lb_name;
//@property (weak, nonatomic) IBOutlet UILabel *lb_count;
//@property (weak, nonatomic) IBOutlet UILabel *lb_time;
@property (weak, nonatomic) IBOutlet UILabel *lb_price;
@property (weak, nonatomic) IBOutlet UILabel *lb_status;
@property (weak, nonatomic) IBOutlet UILabel *lb_doushu;

- (void)setModel:(OrderSummaryMD *)model;
@end
