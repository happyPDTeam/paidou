//
//  ScoreTaskCell.m
//  PaiDou
//
//  Created by JSen on 14/11/10.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ScoreTaskCell.h"
#import "TaskModel.h"
@implementation ScoreTaskCell

- (void)awakeFromNib {
    // Initialization code
    _lb_status.layer.cornerRadius = 2;
    _lb_status.layer.masksToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btn_getScoreAct:(UIButton *)sender {
    
    
}

- (void)setModel:(TaskModel *)taskModel{
    NSString *iconurl = [taskModel.image_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [_icon sd_setImageWithURL:[NSURL URLWithString:iconurl] placeholderImage:[UIImage JSenImageNamed:kPlaceHolderImageName]];
    _lb_name.text = taskModel.title;
    _lb_price.text = [Utils fenToYuan:taskModel.money];
    
   
    NSString *btnTitle = [NSString stringWithFormat:@"%@豆",taskModel.score];
  
    _lb_huoDou.text = btnTitle;
    _lb_getCount.text = [NSString stringWithFormat:@"%d人已领取",taskModel.join_count];
    _lb_time.text = [NSString stringWithFormat:@"有效期:%@",[Utils dateFromTimeInterval:taskModel.end_time]];
    
    NSString *strStatus = nil;
    if ([taskModel.status isEqualToString:kTaskStatusAva]) {
        strStatus = @"未完成";
        _lb_status.backgroundColor = UIColorFromRGB(0x00c7c0);
    }else if ([taskModel.status isEqualToString:kTaskStatusComplete]) {
        strStatus = @"已完成";
         _lb_status.backgroundColor = UIColorFromRGB(0xdedede);
    }else if ([taskModel.status isEqualToString:kTaskStatusSoldout]) {
        strStatus = @"已卖光";
         _lb_status.backgroundColor = UIColorFromRGB(0xdedede);
    }
     _lb_status.text = [Utils getStatusText:taskModel.status];
}

@end
