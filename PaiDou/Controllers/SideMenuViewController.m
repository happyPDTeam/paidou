//
//  LeftProfileViewController.m
//  PaiDou
//
//  Created by JSen on 14/11/6.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "SideMenuViewController.h"
#import "RegVC.h"
#import "UIImageSize.h"
#import  "BaseNavigationController.h"
#import "Login&Register/LoginViewController.h"
#import "MyOrders/MyOrdersVC.h"
#import "RESideMenu.h"
#import "ScoreRecordVC.h"
#import "Profile/PrifleVC.h"
#import "SettingTableVC.h"

@interface SideMenuViewController ()

@property (retain,nonatomic) NSArray *arraySource;
@property (retain,nonatomic) NSArray *arrayImages;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UIButton *btn_log_reg;

- (IBAction)btn_log_reg_act:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lb_kouhao;
@property (weak, nonatomic) IBOutlet UILabel *lb_zhuanqu;
@property (weak, nonatomic) IBOutlet UILabel *lb_myPaidou;
@property (weak, nonatomic) IBOutlet UIView *logedBGView;

@end

@implementation SideMenuViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self cleanFooter:_tableView];
}

- (void)willShow{
    User *user = [[UserManager sharedManager] readFromDisk];
    
    if ([[UserManager sharedManager] hasLogin]) {
        _logedBGView.hidden = NO;
        [_icon sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage JSenImageNamed:@"touxiang_holder.png"]];
        _lb_zhuanqu.text = [NSString stringWithFormat:@"¥%@",[Utils fenToYuan:user.score_money]];
        _lb_myPaidou.text = [NSString stringWithFormat:@"%d",user.score];
    }else{
        _logedBGView.hidden = YES;
        _icon.image = [UIImage JSenImageNamed:@"touxiang_holder.png"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor clearColor];
    _arraySource = @[
                     @"我的订单",
                    // @"我的收藏",
                     @"派豆记录",
                    // @"我的银行卡",
                     @"个人信息",
                     @"设置"
                     ];
    _arrayImages = @[
                     @"cebian_order.png",
                     @"cb_jilu.png",
                     @"cb_geren.png",
                     @"cb_shezhi.png"
                     ];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login:) name:kNotifyLoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:kNotifyLogoff object:nil];
    
   
}

- (void)logout:(NSNotification *)noti{
    [self willShow];
}

- (void)login:(NSNotification *)noti{
    [self willShow];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arraySource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = _arraySource[indexPath.row];
   
    cell.image =[UIImage imageNamed:_arrayImages[indexPath.row ]];
//    cell.image =[UIImageSize thumbnailOfImage:[UIImage imageNamed:_arrayImages[indexPath.row ]] Size:CGSizeMake(20, 20) ];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != _arraySource.count-1) {
        if (![[UserManager sharedManager] hasLogin]) {
            
            [[UserManager sharedManager] showLoginAlertCancel:nil sure:^{
                LoginViewController *login =[[LoginViewController alloc ]init];
                BaseNavigationController *nav = [[BaseNavigationController alloc ]initWithRootViewController:login];
                [self presentViewController:nav animated:NO completion:nil];
            }];
            return;
        }
    }
    
    
    NSString *str = _arraySource[indexPath.row];
     UIViewController *vc = [self getMainVC];
    if ([str isEqualToString:@"我的订单"]) {
        
        if ([[UserManager sharedManager] hasLogin]) {
            
            MyOrdersVC *order = [[MyOrdersVC alloc ]init];
            BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:order];
           
            [vc presentViewController:nav animated:NO completion:nil];
            
        }else{
            [self showHUDText:@"请先登录" xOffset:0 yOffset:0];
        }
    }else if ([str isEqualToString:@"派豆记录"]) {
        ScoreRecordVC *score = [[ScoreRecordVC alloc] init];
        BaseNavigationController *ns = [[BaseNavigationController alloc] initWithRootViewController:score];
        [vc presentViewController:ns animated:NO completion:nil];
    }else if ([str isEqualToString:@"个人信息"]){
        PrifleVC *pvc = [[PrifleVC alloc] init];
        BaseNavigationController *np = [[BaseNavigationController alloc] initWithRootViewController:pvc];
        [vc presentViewController:np animated:NO completion:nil];
    }else if ([str isEqualToString:@"设置"]) {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"SettingsStory" bundle:nil];
        BaseNavigationController *nav = [story instantiateInitialViewController];
        [vc presentViewController:nav animated:NO completion:nil];
    }
   
}
- (UIViewController *)getMainVC{
    AppDelegate *del = [UIApplication sharedApplication].delegate;
    RESideMenu *menu = del.window.rootViewController;
    UITabBarController *tab = menu.contentViewController;
    UINavigationController *nav = tab.selectedViewController;
    UIViewController *vc = nav.topViewController;
   
    return vc;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_log_reg_act:(id)sender {
   // RegVC *reg = [[RegVC alloc] init];
    LoginViewController *login = [[LoginViewController alloc] init];
    
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:login];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
    
}
@end
