//
//  MyProfileVC.m
//  PaiDou
//
//  Created by JSen on 14/12/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "MyProfileVC.h"
#import "ClickableUIView.h"
#import "BaseNavigationController.h"
#import "LoginViewController.h"
#import "Profile/PrifleVC.h"
#import "SettingTableVC.h"
#import "Login&Register/LoginViewController.h"
#import "MyOrders/MyOrdersVC.h"
#import "RegVC.h"
#import "ScoreRecordVC.h"
#import "Profile/PrifleVC.h"
#import "JSBadgeView.h"
#import "Message/MessageVC.h"
#import "VerifyCodeVC.h"
#import "PDAlertView.h"
#import "ZSYPopoverListView.h"

@interface MyProfileVC ()<UITableViewDataSource,UITableViewDelegate>
{
    JSBadgeView *_badgeView;
    UIImageView *_badgeIcon;
    PDAlertView * _pdAlertView;
}
@property (retain,nonatomic) NSArray *arraySource;
@property (retain,nonatomic) NSArray *arrayImages;


@property (weak, nonatomic) IBOutlet UITableView *tview;
@property (strong, nonatomic) IBOutlet ClickableUIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *lb_phone;
@property (weak, nonatomic) IBOutlet UILabel *lb_shen;
@property (weak, nonatomic) IBOutlet UILabel *lb_left;
@property (weak, nonatomic) IBOutlet UILabel *sss;
@property (weak, nonatomic) IBOutlet UILabel *yuan;

@property (strong,nonatomic) UIBarButtonItem *loginItem;

@property (strong,nonatomic) UIBarButtonItem *logoutItem;

@end

@implementation MyProfileVC

- (instancetype)init
{
    self = [super init];
    if (self) {
        _badgeIcon = [[UIImageView alloc ]init];
        _badgeIcon.image = [UIImage JSenImageNamed:@"xiaoxi.png"];
        _badgeIcon.size = CGSizeMake(10, 10);
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    
    NSIndexPath * selected = [self.tview indexPathForSelectedRow];
    
    if(selected)
    {
        [self.tview deselectRowAtIndexPath:selected animated:NO];
    }
    
    [super viewWillAppear:animated];
    [self cleanFooter:_tview];
    
    [self willShow];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [_tview reloadData];
}

- (void)willShow{
    User *user = [[UserManager sharedManager] readFromDisk];
    _badgeIcon.hidden = [UserManager sharedManager].appBadge == 0 ? YES:NO;
    if ([[UserManager sharedManager] hasLogin]) {
        
        [_icon sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"header"]];
        _lb_phone.text = user.mobile;
        _lb_shen.text = [NSString stringWithFormat:@"%@",[Utils fenToYuan:user.score_money]];
        _lb_left.text = [NSString stringWithFormat:@"%d",user.score];
        
        
        self.navigationItem.rightBarButtonItem = _logoutItem;
        
        for (UIView *oneView in _headerView.subviews) {
            if ([oneView isKindOfClass:[UILabel class]]) {
                oneView.hidden = NO;
            }
        }
        
    }else{
       
        _icon.image = [UIImage imageNamed:@"header.png"];
        
        _lb_phone.text = @"--您还没有登录--";
        
        _lb_phone.textColor = [UIColor colorWithRed:135/255.0 green:135/255.0 blue:135/255.0 alpha:1];
        
        _lb_phone.font = [UIFont systemFontOfSize:15];
        
        self.navigationItem.rightBarButtonItem = _loginItem;
        
        for (UIView *oneView in _headerView.subviews) {
            if ([oneView isKindOfClass:[UILabel class]]) {
                oneView.hidden = YES;
            }
        }
        _lb_phone.hidden = NO;
    }
    
    self.lb_shen.hidden= YES;
    
    self.sss.hidden = YES;
    
    self.yuan.hidden = YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createAlertView];
    
    self.view.backgroundColor = [UIColor appBackgroundColor];
    
    self.sss.hidden = YES;
    
    self.yuan.hidden = YES;
    
    _headerView.height = 112;
    self.tview.tableHeaderView = _headerView;
    
    self.tview.layer.borderWidth = 0.5;
    
    self.tview.layer.borderColor = [UIColor colorWithRed:228/255.0 green:228/255.0 blue:232/255.0 alpha:1].CGColor;
    
    self.tview.bounces = NO;
    
    [self.view addSubview:_tview];
    
    //[self createFooterView];
    
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"我的"];
    
    _loginItem = [[UIBarButtonItem alloc ]initWithTitle:@"登录" style:UIBarButtonItemStylePlain target:self action:@selector(loginAct)];
    _logoutItem = [[UIBarButtonItem alloc ]initWithTitle:@"退出登录" style:UIBarButtonItemStylePlain target:self action:@selector(logoutAct)];
    
    NSDictionary *attDict = @{
                              NSForegroundColorAttributeName:[UIColor appWihteColor]
                              };
    
    [_loginItem setTitleTextAttributes:attDict forState:UIControlStateNormal];
    [_logoutItem setTitleTextAttributes:attDict forState:UIControlStateNormal];
    
    [_icon setRoundedCornerWithRadius:44];
//    _icon.layer.cornerRadius = _icon.height/2;
//    _icon.layer.masksToBounds = YES;
    
    
    [_headerView handleComplemetionBlock:^(ClickableUIView *view) {
        if (![[UserManager sharedManager] hasLogin]) {
            
            [[UserManager sharedManager] showLoginAlertCancel:nil sure:^{
                LoginViewController *login =[[LoginViewController alloc ]init];
                BaseNavigationController *nav = [[BaseNavigationController alloc ]initWithRootViewController:login];
                [self presentViewController:nav animated:NO completion:nil];
            }];
            
        }else{
            
            PrifleVC *profile = [[PrifleVC alloc ]init];
            [self.navigationController pushViewController:profile animated:YES];
        }
    }];
    
    _arraySource = @[
                     @"兑换券",
                     @"我的订单",
                     // @"我的收藏",
                     @"派豆记录",
                     // @"我的银行卡",
//                     @"个人信息",
//                      @"消息通知",
                     @"派豆兑换码",
                     @"设置"
                    
                     ];
    _arrayImages = @[

                     @"duihuanquan.png",

                     @"cebian_order.png",
                     @"cb_jilu.png",
                     @"duihuanm.png",
//                     @"tongzhi.png",
                     @"cb_shezhi.png",
                     ];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login:) name:kNotifyLoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:kNotifyLogoff object:nil];
    
}

- (void)loginAct{
    LoginViewController *login =[[LoginViewController alloc ]init];
    BaseNavigationController *nav = [[BaseNavigationController alloc ]initWithRootViewController:login];
    [self presentViewController:nav animated:NO completion:nil];
}

- (void)logoutAct{
    RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消"];
    RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
       [[UserManager sharedManager] logOff];
    }];
    UIAlertView *alert = [[UIAlertView alloc ]initWithTitle:@"提示" message:@"确认退出" cancelButtonItem:cancel otherButtonItems:sure, nil];
    [alert show];
}
- (void)logout:(NSNotification *)noti{
    [self willShow];
}

- (void)login:(NSNotification *)noti{
    [self willShow];
}

-(void)createFooterView
{
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    
    footerView.backgroundColor = [UIColor appBackgroundColor];
    
    self.tview.tableFooterView = footerView;
}

-(void)createAlertView
{
    _pdAlertView = [[PDAlertView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    
    __weak MyProfileVC * vc = self;
    
    _pdAlertView.block = ^(NSString * str)
    {
        [vc showHUDText:str xOffset:0 yOffset:0];
    };
    
//    //[self.navigationController.view addSubview:_pdAlertView];
//    
//    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
//    
//    //delegate.window.alpha = 0.5;
//    
//    [delegate.window addSubview:_pdAlertView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arraySource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (indexPath.row == 2) {
//        if (!_badgeView) {
//            _badgeView = [[JSBadgeView alloc ]initWithParentView:cell.contentView alignment:JSBadgeViewAlignmentCenterRight];
//            
//        }
//        _badgeView.badgeText =@" ";
//        [_badgeView setRoundedCornerWithRadius:1];
//        _badgeView.badgeStrokeWidth  = 3;
//          _badgeView.hidden  = [UserManager sharedManager].appBadge == 0 ? YES:NO;
        
        _badgeIcon.origin = CGPointMake(cell.contentView.right-40, cell.contentView.height/2-_badgeIcon.height/2);
        [cell.contentView addSubview:_badgeIcon];
    }
    
    cell.textLabel.text = _arraySource[indexPath.row];
    
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    cell.textLabel.textColor = [UIColor app5d5d5dColor];
    
    cell.image =[UIImage imageNamed:_arrayImages[indexPath.row ]];
    //    cell.image =[UIImageSize thumbnailOfImage:[UIImage imageNamed:_arrayImages[indexPath.row ]] Size:CGSizeMake(20, 20) ];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(-1, 0, SCREEN_WIDTH + 2, 15)];
    
    view.backgroundColor = [UIColor appBackgroundColor];
    
    view.layer.borderWidth = 0.5;
    
    view.layer.borderColor = [UIColor colorWithRed:228/255.0 green:228/255.0 blue:232/255.0 alpha:1].CGColor;
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != 4) {
        if (![[UserManager sharedManager] hasLogin]) {
            
            [[UserManager sharedManager] showLoginAlertCancel:nil sure:^{
                LoginViewController *login =[[LoginViewController alloc ]init];
                BaseNavigationController *nav = [[BaseNavigationController alloc ]initWithRootViewController:login];
                [self presentViewController:nav animated:NO completion:nil];
            }];
            return;
        }
    }
   
    
      NSString *str = _arraySource[indexPath.row];
    if ([str isEqualToString:@"我的订单"]) {
        
        if ([[UserManager sharedManager] hasLogin]) {
            
            MyOrdersVC *order = [[MyOrdersVC alloc ]init];
            BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:order];
            
            [self.navigationController pushViewController:order animated:YES];
            
        }else{
            [self showHUDText:@"请先登录" xOffset:0 yOffset:0];
        }
    }else if ([str isEqualToString:@"兑换券"])
    {
        VerifyCodeVC *vc = [[VerifyCodeVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([str isEqualToString:@"派豆记录"]) {
        ScoreRecordVC *score = [[ScoreRecordVC alloc] init];
        BaseNavigationController *ns = [[BaseNavigationController alloc] initWithRootViewController:score];
         [self.navigationController pushViewController:score animated:YES];
    }else if ([str isEqualToString:@"消息通知"]){
        MessageVC *msg  = [[MessageVC alloc ]init];
        [self.navigationController pushViewController:msg animated:YES];
       
    }else if ([str isEqualToString:@"派豆兑换码"])
    {
        
        ZSYPopoverListView *listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 280, 200)];
        listView.titleName.text = @"派豆兑换码";
        
        //listView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"alertbg.jpg"]];
        //    listView.datasource = self;
        //    listView.delegate = self;
        //    [listView setCancelButtonTitle:@"Cancel" block:^{
        //        NSLog(@"cancel");
        //    }];
        //    [listView setDoneButtonWithTitle:@"OK" block:^{
        //        NSLog(@"Ok%d", [listView indexPathForSelectedRow].row);
        //    }];
        [listView show];
    }else if ([str isEqualToString:@"设置"]) {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"SettingsStory" bundle:nil];
        BaseNavigationController *nav = [story instantiateInitialViewController];
        UIViewController *vc = [story instantiateViewControllerWithIdentifier:@"settingId"];
        [self.navigationController pushViewController:vc animated:YES];
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 15;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue r:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
