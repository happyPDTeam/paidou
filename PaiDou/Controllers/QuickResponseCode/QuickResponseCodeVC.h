//
//  QuickResponseCodeVC.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"

@interface QuickResponseCodeVC : BaseViewController

@property (copy, nonatomic) NSString * shopName;

@property (copy, nonatomic) NSString * codeNumber;

@end
