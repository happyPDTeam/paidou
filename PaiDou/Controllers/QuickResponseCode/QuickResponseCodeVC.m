//
//  QuickResponseCodeVC.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "QuickResponseCodeVC.h"
#import "QRCodeGenerator.h"

@interface QuickResponseCodeVC ()
@property (weak, nonatomic) IBOutlet UILabel *shopLabel;
@property (weak, nonatomic) IBOutlet UIImageView *QRImage;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation QuickResponseCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bgView.layer.borderWidth = 0.5;
    
    self.bgView.layer.borderColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1].CGColor;
    
    self.codeLabel.text = [Utils insertSpace:self.codeNumber];
    
    self.shopLabel.text = self.shopName;
    
    UIImage * image = [QRCodeGenerator qrImageForString:self.codeNumber imageSize:500];
    
    self.QRImage.image = image;
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"兑换码详情"];
}

//-(void)createHeaderBgView
//{
//    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
//    
//    view.backgroundColor = [UIColor appBackgroundColor];
//    
//    [self.view addSubview:view];
//}

- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
