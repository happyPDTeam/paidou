//
//  ProductCell.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ProductCell.h"
#import "SDWebImageManager.h"

@implementation ProductCell

- (void)awakeFromNib {
    // Initialization code
    self.finishLabel.layer.borderWidth = 1;
    
    self.finishLabel.layer.masksToBounds = YES;
    
    self.finishLabel.layer.cornerRadius = 2;
    
    self.finishLabel.layer.borderColor = [UIColor colorWithRed:0 green:199/255.0 blue:191/255.0 alpha:1].CGColor;
    
    _ProductImage.layer.masksToBounds = YES;
    
    _ProductImage.layer.cornerRadius = 3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)config:(TaskModel *)model
{
    NSString * encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault, (CFStringRef)model.image_url, NULL, NULL,  kCFStringEncodingUTF8 ));
    
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:encodedString] options:SDWebImageAllowInvalidSSLCertificates progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        [self.ProductImage setImage:image];
    }];
    self.titleLabel.text = model.title;
    
    NSString * str = [NSString stringWithFormat:@"%@", model.score];
    
    [self setLabel:self.saveLabel bean:str];
    
    //self.saveLabel.text = [NSString stringWithFormat:@"赚%@派豆", model.score];
    
    self.countLabel.text = [NSString stringWithFormat:@"%d人已参加", model.join_count];
    
    //self.finishLabel.text = [self judge:model.status];
    NSString *strStatus = nil;
    if ([model.status isEqualToString:kTaskStatusAva]) {
        strStatus = @"未完成";
        //self.finishLabel.backgroundColor = UIColorFromRGB(0x00c7c0);
    }else if ([model.status isEqualToString:kTaskStatusComplete]) {
        strStatus = @"已完成";
        self.finishLabel.textColor = UIColorFromRGB(0x999999);
        
        self.finishLabel.layer.borderColor = UIColorFromRGB(0x999999).CGColor;
    }else if ([model.status isEqualToString:kTaskStatusSoldout]) {
        strStatus = @"已卖光";
        //self.finishLabel.backgroundColor = UIColorFromRGB(0xdedede);
    }
    self.finishLabel.text = [Utils getStatusText:model.status];
}

-(void)setLabel:(UILabel *)label bean:(NSString *)bean
{
    NSString * beanString = [NSString stringWithFormat:@"赚%@派豆", bean];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:beanString];
    
    NSRange range = NSMakeRange(1, bean.length);
    
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:range];
    
    label.attributedText = str;
}

-(NSString *)judge:(NSString *)str
{
    if ([str isEqualToString:@"available"]) {
        return @"未完成";
    }
    else if ([str isEqualToString:@"completed"])
    {
        return @"已完成";
    }
    else if ([str isEqualToString:@""])
    {
        return @"已发完";
    }
    return @"未完成";
}

@end