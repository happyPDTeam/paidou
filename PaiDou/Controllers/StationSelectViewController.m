//
//  StationSelectViewController.m
//  PaiDou
//
//  Created by JSen on 14/12/17.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "StationSelectViewController.h"
#import "StationLine.h"

@interface StationSelectViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *_metroArray;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtr;
- (IBAction)segActioon:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tview;
@end

@implementation StationSelectViewController

- (void)loadMetroData{
    if (!_metroArray) {
        _metroArray  = [NSMutableArray array];
    }else{
        [_metroArray removeAllObjects];
    }
    NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithFile:kMetroLinePath];
    
   
    
    if (arr.count) {
        [_metroArray addObjectsFromArray:arr];
        [self loadSegment];
    }
    [_tview reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:_tview];
    [self loadMetroData];

    [[NSNotificationCenter defaultCenter] addObserverForName:kNotifyFoundNearestStation object:nil queue:[NSOperationQueue currentQueue] usingBlock:^(NSNotification *note) {
        [self loadMetroData];
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotifyWriteMetroLineSuccess object:nil queue:[NSOperationQueue currentQueue] usingBlock:^(NSNotification *note) {
        [self loadMetroData];
    }];
    
    
}

- (void)loadSegment{
    [_segmentCtr removeAllSegments];
    
    for (int i = 0; i < _metroArray.count; i++) {
        StationLine *line  = _metroArray[i];
        [_segmentCtr insertSegmentWithTitle:line.line_name atIndex:i animated:YES];
       
    }
     _segmentCtr.selectedSegmentIndex = 0;
 
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_metroArray.count == 0) {
        return 0;
    }
    
    StationLine *line = _metroArray[_segmentCtr.selectedSegmentIndex];
    
    return line.station_list.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellid = @"cellid";
    UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc ]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    
    StationLine *line = _metroArray[_segmentCtr.selectedSegmentIndex];
    Station *station = line.station_list[indexPath.row];
    cell.textLabel.text = station.station_name;
    
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    
    cell.textLabel.textColor = UIColorFromRGB(0x666666);
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    StationLine *line = _metroArray[_segmentCtr.selectedSegmentIndex];
    Station *station = line.station_list[indexPath.row];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyDidSelectStation object:nil userInfo:@{@"S":station}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)segActioon:(UISegmentedControl *)sender {
    
    [_tview reloadData];
    
}
@end
