//
//  ProductCell.h
//  PaiDou
//
//  Created by JeremyRen on 14/12/18.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaskModel.h"

@interface ProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ProductImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveLabel;

-(void)config:(TaskModel *)model;

@end
