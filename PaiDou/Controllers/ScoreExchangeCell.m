//
//  ScoreExchangeCell.m
//  PaiDou
//
//  Created by JSen on 14/11/10.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "ScoreExchangeCell.h"

@implementation ScoreExchangeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ProductSummaryMD *)model{
    NSString *iamgeUlr = [model.avatar stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [_icon sd_setImageWithURL:[NSURL URLWithString:iamgeUlr] placeholderImage:[UIImage JSenImageNamed:kPlaceHolderImageName]];
    __lb_name.text = model.title;
    _lb_shopName.text = model.biz_name;
    __lb_salePrice.text =[NSString stringWithFormat:@"%@元", [Utils fenToYuan:model.sale_price]];
    __lb_oriPirce.text =[NSString stringWithFormat:@"市场价：%@元", [Utils fenToYuan:model.market_price]];
    __lb_needScore.text = [NSString stringWithFormat:@"%d派豆",model.score_max];
    __lb_getBackMoney.text =[NSString stringWithFormat:@"%@元",[Utils fenToYuan:model.earn_money]];
    __lb_duihuanNumber.text = [NSString stringWithFormat:@"共有%d人兑换",model.sold_count];
    __lb_endTime.text = [Utils dateFromTimeInterval:model.end_time];
    __lb_redeemDes.text = [NSString stringWithFormat:@"购买回报收益率%d",model.return_rate];
}

@end
