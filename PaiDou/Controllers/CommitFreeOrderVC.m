//
//  CommitFreeOrderVC.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/23.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "CommitFreeOrderVC.h"
#import "CustomStepper.h"
#import "OrderSummaryMD.h"
#import "OrderModel.h"
#import "PurchaseSuccessViewController.h"

@interface CommitFreeOrderVC ()<UITableViewDataSource, UITableViewDelegate>
{
    NSInteger _number;
    
    NSMutableDictionary *_orderInfoDict;
    
    CustomStepper *_stepper;
}

@property (strong, nonatomic) IBOutlet UITableViewCell *nameCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *countCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *totalCell;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPrice;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *promptLabel;

@property (nonatomic, retain) OrderSummaryMD *orderModel;

@end

@implementation CommitFreeOrderVC

-(void)viewWillAppear:(BOOL)animated
{
    self.promptLabel.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor appBackgroundColor];
    
    [self setTable];
    
    _number = 1;
    
    _stepper = [[CustomStepper alloc] init];
    UIView *calcView = [_stepper getCustomStepper];
    
    calcView.origin = CGPointMake(SCREEN_WIDTH-130, 7);
    [_countCell.contentView addSubview:calcView];
    
    if (_detailModel) {
        
        _titleLabel.text = [NSString stringWithFormat:@"%@",_detailModel.title];
        
        _priceLabel.text = [NSString stringWithFormat:@"%d派豆",_detailModel.score_max];
        
        _totalPrice.text = _priceLabel.text;
    }
    
    [self _addTableFooterView];
    
    [_stepper textDidChanged:^(int number) {
        _number = number;
        [self change:_number];
        
    }];
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"确认支付"];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)setTable
{
    self.tableView.layer.borderWidth = 0.5;
    
    self.tableView.layer.borderColor = [UIColor colorWithRed:228/255.0 green:228/255.0 blue:232/255.0 alpha:1].CGColor;
    
    self.tableView.bounces = NO;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return _nameCell;
            break;
        case 1:
            return _countCell;
            break;
        case 2:
            return _totalCell;
            break;
        default:
            break;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        return 50;
    }
    return 45;
}

- (void)change:(NSInteger)number{
    _totalPrice.text = [NSString stringWithFormat:@"%d派豆", [self getTotalPrice]];
}

-(NSInteger)getTotalPrice
{
    return _number * _detailModel.score_max;
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)_addTableFooterView{
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(20, 300, SCREEN_WIDTH - 40, 50)];
//    view.backgroundColor = [UIColor whiteColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(15, 180, SCREEN_WIDTH - 30, 50);
    NSString *title = @"确认购买";
    
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor appWihteColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage JSenImageNamed:@"btn_yellow.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buyClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
//    [view addSubview:btn];
//    self.tableView.tableFooterView = view;
    
    [self.view addSubview:btn];
    
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    
}

#pragma mark - 点击购买
- (void)buyClick:(UIButton *)btn
{
    if (![UserManager sharedManager].hasLogin) {
        [self showHUDText:@"请先登录" xOffset:0 yOffset:0];
        return;
    }
    User *user = [[UserManager sharedManager] readFromDisk];
    
    if ([self getTotalPrice] > user.score) {
        [self showHUDText:@"派豆数不足" xOffset:0 yOffset:0];
        
        [self setLabelTitle];
        
        return;
    }
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.userInteractionEnabled = NO;
    HUD.minShowTime = 1.0f;
    NSDictionary *info = @{
                           @"uid":user.uid,
                           @"bid":_detailModel.biz_id,
                           @"type":kProductTypeScore,
                           @"pid":_detailModel.pid,
                           @"unit_score":@(_detailModel.score_max),
                           @"count":@(_number),
                           @"total_score":@(_detailModel.score_max * _number),
                           @"unit_price":@0,
                           @"total_price":@0
                           };
    _orderInfoDict = [NSMutableDictionary dictionaryWithDictionary:info];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager POST:kCreateOrderURL parameters:_orderInfoDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        HUD.hidden = YES;
        if ([responseObject[@"err_code"] intValue] == 0)
        {
            NSError *error;
            OrderSummaryMD *model = [[OrderSummaryMD alloc]initWithDictionary:responseObject[@"order_brief_info"] error:&error];
            if (error) {
                NSLog(@"%@",error);
                return ;
            }
            _orderModel = model;
            //扣除用户的派豆
            [[UserManager sharedManager] decreaseDou:_orderModel.total_score];
            
            [self paySuccess:nil];
        }
        [self showHUDText:responseObject[@"err_msg"] xOffset:0 yOffset:0];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        HUD.hidden =YES;
        [self showHUDText:@"支付失败" xOffset:0 yOffset:0];
    }];
}

-(void)setLabelTitle
{
    self.promptLabel.hidden = NO;
    
    User *user = [[UserManager sharedManager] readFromDisk];
    
    NSString * paidou = [NSString stringWithFormat:@"%d派豆", user.score];
    
    NSString * title = [NSString stringWithFormat:@"*您的派豆余额为:%@，无法兑换更多了", paidou];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:title];
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor app7e7e7eColor] range:NSMakeRange(0, 8)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appfe3c00Color] range:NSMakeRange(8, paidou.length + 1)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor app7e7e7eColor] range:NSMakeRange(8 + paidou.length + 1, str.length - (9 + paidou.length))];
    
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, str.length)];
    
    self.promptLabel.attributedText = str;
}

- (void)paySuccess:(NSNotification *)note {
    PurchaseSuccessViewController *success = [[PurchaseSuccessViewController alloc]initWithNibName:@"PurchaseSuccessViewController" bundle:nil];
    /*
     {name = AliSafePaySuccess; userInfo = {
     safePayResult = "result = {\n\tstatusCode=9000\n\tstatusMessage=\n\tsignType=RSA\n\tsignString=VSLPlI6gq1f9zqiBPCfaiEZF6IMKvwy2oN8Ph4IEHhO/y3k7X3plJnIDwVEqMWe5x1AAL5Ib3f3k4WRrvrdxcvS+vaQ2VfOD5x2mEZGW1nxiJVVpkxxN8C1QA+5NnDGeLEIEvGheAKTtZLv4J42o9lGwCV2heUfDo4Yy9HwUdNQ=\n}\n";
     }}
     */
    NSLog(@"note-->%@",note);
    success.productModel = _detailModel;
    success.orderModel = _orderModel;
    [self.navigationController pushViewController:success animated:YES];
}

- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
