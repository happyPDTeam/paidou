//
//  LoginViewController.h
//  wft
//
//  Created by JSen on 14/10/12.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "TextFieldValidator.h"
#import "BaseViewController.h"

@interface LoginViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet TextFieldValidator *TFUserName;
@property (weak, nonatomic) IBOutlet TextFieldValidator *TFPassWord;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLoginAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *tableFooterView;
@end
