//
//  LoginVC.m
//  PaiDou
//
//  Created by JSen on 14/11/11.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "RegVC.h"
#import "PassVC.h"
#import "TextFieldValidator.h"
#import "ClickableUILabel.h"
#import "BaseNavigationController.h"

@interface RegVC (){
      NSTimer *_timer;
}
@property (weak, nonatomic) IBOutlet TextFieldValidator *TFPhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnGetCode;
- (IBAction)btnGetCodeAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet TextFieldValidator *TFCode;
@property (weak, nonatomic) IBOutlet UIButton *btnSure;
- (IBAction)btnSureAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet ClickableUILabel *lb_userProtocol;
@property (weak, nonatomic) IBOutlet UIButton *btn_check;
- (IBAction)btn_check_act:(UIButton *)sender;

@end

@implementation RegVC


 static int i = 60;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"注册"];
    [self showLeftBackBarbuttonItemWithSelector:@selector(backClick:)];
    
    [_TFPhoneNumber addRegx:REGEX_PHONE_DEFAULT withMsg:@"请输入11位手机号"];
    
    [_TFCode addRegx:REGEX_RANDOMCODE withMsg:@"请输入6位验证码"];
    
    
    
    _TFPhoneNumber.layer.cornerRadius = 5;
    _TFCode.layer.cornerRadius = 5;
    
    _TFPhoneNumber.layer.masksToBounds = YES;
    _TFCode.layer.masksToBounds = YES;
    _btnSure.layer.cornerRadius = 5;
    _btnSure.layer.masksToBounds = YES;
    
    _btnGetCode.layer.masksToBounds = YES;
    _btnGetCode.layer.cornerRadius = 5;
    
    
    [_lb_userProtocol handleClickEvent:^(ClickableUILabel *label) {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"SettingsStory" bundle:nil];
        UIViewController *vc = [story instantiateViewControllerWithIdentifier:@"servieceID"];
//        BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"已有账号" style:UIBarButtonItemStylePlain target:self action:@selector(backClick:)];
    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor appWihteColor]} forState:UIControlStateNormal];
    //self.navigationItem.rightBarButtonItem = rightItem;
    
}

- (void)backClick:(UIBarButtonItem *)item{
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//    }];
    [self stopTimer];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnGetCodeAction:(UIButton *)sender {
//    if (![_TFPhoneNumber validate]) {
//        return;
//    }
    if (![_TFPhoneNumber validate]) {
        [self showHUDText:@"请输入11位手机号" xOffset:0 yOffset:0];
        return;
    }
    
    sender.enabled = NO;
    NSDictionary *parmDic = @{@"mobile":_TFPhoneNumber.text};
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
  
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:kRandomVaildateCodeUrl parameters:parmDic success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"obj %@",responseObject);
        [hud hide:YES];
        if ([responseObject[@"err_code"] integerValue] == 0) {
            i = [responseObject[@"interval"] intValue];
            [self stopTimer];
            _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(_refreshButtonTitle:) userInfo:nil repeats:YES];
        }else{
            [self showHUDText:responseObject[@"err_msg"] xOffset:0 yOffset:0];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",[error description]);
        hud.labelText = @"获取失败，请重试";
        [hud hide:YES afterDelay:2];
        sender.enabled = YES;
    }];
}

- (void)_refreshButtonTitle:(NSTimer *)timer{
   
    if (i == 0) {
        _btnGetCode.enabled = YES;
        _btnGetCode.titleLabel.text = @"获取验证码";
        [self stopTimer];
        i = 60;
    }else{
        _btnGetCode.titleLabel.text = [NSString stringWithFormat:@"验证码(%d)",i];
        i--;
    }
    
    
}
- (void)stopTimer {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}
- (IBAction)btnSureAction:(UIButton *)sender {
    
    if (![_TFPhoneNumber validate]) {
        [self showHUDText:@"请输入11位手机号" xOffset:0 yOffset:0];
        return;
    }
    if (![_TFCode validate]) {
        [self showHUDText:@"请输入6位验证码" xOffset:0 yOffset:0];
        return;
    }
   
    if (!_btn_check.selected) {
        [self showHUDText:@"您未同意用户协议" xOffset:0 yOffset:0];
        return;
    }
    NSDictionary *paramDic = @{
                               @"mobile":_TFPhoneNumber.text,
                               @"sms_code":_TFCode.text,
                          
                               };
 
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.userInteractionEnabled = NO;
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:kVerifyVaildateCodeUrl parameters:paramDic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        hud.hidden = YES;
        int status = [responseObject[@"err_code"]intValue];
        if ( status == 0 ) {
            PassVC *vc = [[PassVC alloc] init];
            vc.phoneNum = _TFPhoneNumber.text;
            vc.code = _TFCode.text;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [self showHUDText:responseObject[@"err_msg"] xOffset:0 yOffset:0];
        }
        
//        }else if (status == 2012){
//            //sms code expired
//            [self showHUDText:@"验证码超时,请获取新验证码" xOffset:0 yOffset:0];
//        }else if (status == 2052){
//            //register user exists!
//            [self showHUDText:@"用户已注册" xOffset:0 yOffset:0];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",[error description]);
        hud.hidden = YES;
        sender.enabled = YES;
        [self showHUDText:@"操作失败" xOffset:0 yOffset:0];
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_TFPhoneNumber resignFirstResponder];
    [_TFCode resignFirstResponder];
}
- (IBAction)btn_check_act:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (void)dealloc{
    [_timer invalidate];
    _timer = nil;
}
@end
