//
//  PassVC.m
//  PaiDou
//
//  Created by JSen on 14/11/11.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "PassVC.h"
#import "TextFieldValidator.h"
#import "SVWebViewController.h"

@interface PassVC ()
@property (weak, nonatomic) IBOutlet TextFieldValidator *TFPass;

@property (weak, nonatomic) IBOutlet UITextField *TFInviteCode;
@end

@implementation PassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"设置密码"];
    
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(submit:)];
    NSDictionary *dict = @{
                           NSForegroundColorAttributeName:[UIColor appWihteColor]
                           };
    [rightItem setTitleTextAttributes:dict forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    _TFPass.layer.cornerRadius = 5;
    _TFPass.layer.masksToBounds = YES;
    
    _TFInviteCode.layer.cornerRadius = 5;
    _TFInviteCode.layer.masksToBounds = YES;
    
    [_TFPass addRegx:REGEX_PASSWORD withMsg:@"请输入6-32位密码"];
     [self showLeftBackBarbuttonItemWithSelector:@selector(backClick)];
    // Do any additional setup after loading the view from its nib.
}
- (void)backClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)submit:(UIBarButtonItem *)item{
    if (!_phoneNum.length || !_code.length) {
        return;
    }
    if (![_TFPass validate]) {
        [self showHUDText:@"请输入正确格式密码" xOffset:0 yOffset:0];
        return;
    }
    item.enabled = NO;
    NSString *verCode = _TFInviteCode.text.length ? _TFInviteCode.text:@"";
    NSString *strPass = [Utils processedPassWordString:_TFPass.text phoneNumber:_phoneNum];
    NSDictionary *paramDic = @{
                               @"mobile":_phoneNum,
                               @"sms_code":_code,
                               @"password":strPass,
                               @"device_id":DEV_UUID,
                               @"platform":@"ios",
                               @"invite_code":verCode
                               };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:kRegisterURL parameters:paramDic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        item.enabled = YES;
        NSLog(@"%@",responseObject);
        int status = [responseObject[@"err_code"] intValue];
        if ( status == 0 ) {
            [self _saveUserProfile:responseObject[@"user"] pass:_TFPass.text];
//            SVWebViewController *web = [[SVWebViewController alloc] initWithURL:[NSURL URLWithString:responseObject[@"product_url_ios"]]];
//            [self.navigationController pushViewController:web animated:YES];
            //success
            [self dismissViewControllerAnimated:YES completion:nil];
//            [self.navigationController popToRootViewControllerAnimated:YES];

        }else if (status == 2012){
            //sms code expired
            [self showHUDText:@"验证码超时,请获取新验证码" xOffset:0 yOffset:0];
        }else if (status == 2052){
            //register user exists!
            [self showHUDText:@"用户已注册" xOffset:0 yOffset:0];
        }else{
            [self showHUDText:responseObject[@"err_msg"] xOffset:0 yOffset:0];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [self showHUDText:@"注册失败，请检查网络连接" xOffset:0 yOffset:0];
        item.enabled = YES;
    }];

}
- (void)_saveUserProfile:(NSDictionary *)dict pass:(NSString *)password{
    NSError *error;
    User *user = [[User alloc]initWithDictionary:dict error:&error];
    user.passWord = password;
    
    if (error) {
        NSLog(@"%@",error);
    }
    UserManager *mgr = [UserManager sharedManager];
    mgr.hasLogin = YES;
    [mgr setLoginUser:user];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_TFPass resignFirstResponder];
    [_TFInviteCode resignFirstResponder];
}

@end
