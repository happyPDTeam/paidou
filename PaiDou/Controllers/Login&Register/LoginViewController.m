//
//  LoginViewController.m
//  wft
//
//  Created by JSen on 14/10/12.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "LoginViewController.h"
#import "User.h"
#import "UserManager.h"
#import "ClickableUILabel.h"
#import "RegVC.h"


@interface LoginViewController ()<UITableViewDataSource,UITableViewDelegate>{
    MBProgressHUD *_myHUD;
}

@property (strong, nonatomic) IBOutlet UITableViewCell *phoneCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *codeCell;
@property (weak, nonatomic) IBOutlet ClickableUILabel *lb_register;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self ) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.tableView.tableFooterView = _tableFooterView;
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"登录"];
    [self showLeftBackBarbuttonItemWithSelector:@selector(backClick:)];
    
    _btnLogin.layer.cornerRadius = 3;
    _btnLogin.layer.masksToBounds = YES;
    
    [_lb_register handleClickEvent:^(ClickableUILabel *label) {
        RegVC *reg = [[RegVC alloc]initWithNibName:@"RegVC" bundle:nil];
        
        [self.navigationController pushViewController:reg animated:NO];
        
    }];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"新用户注册" style:UIBarButtonItemStylePlain target:self action:@selector(registerAction:)];
    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor appWihteColor]} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
    [_TFUserName addRegx:REGEX_PHONE_DEFAULT withMsg:@"请输入11位手机号"];
    [_TFPassWord addRegx:REGEX_PASSWORD withMsg:@"请输入6-32位密码"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismiss) name:kNotifyNeedLoginVCDismiss object:nil];
   
}
- (void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)registerAction:(UIBarButtonItem *)item{
    RegVC *reg = [[RegVC alloc]initWithNibName:@"RegVC" bundle:nil];
    
    [self.navigationController pushViewController:reg animated:NO];
}

- (void)backClick:(UIBarButtonItem *)item {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //test
    if ([self.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
         [self dismissViewControllerAnimated:YES completion:nil];
    }
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return _phoneCell;
    }else if (indexPath.row == 1){
        return _codeCell;
    }
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    return 60;
}



- (IBAction)btnLoginAction:(UIButton *)sender {
    if (![_TFUserName validate] || ![_TFPassWord validate]) {
        [self showHUDText:@"请输入正确的手机号和密码" xOffset:0 yOffset:0];
        return;
    }else if (![CustomReachability networkConnectionAvailable]){
        NSLog(@"no internet connection!!!");
        [self showHUDText:@"网络已断开，请检查！" xOffset:0 yOffset:0];
        return;
    }
    _myHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    _myHUD.userInteractionEnabled = NO;
    _myHUD.minShowTime = 1.0f;
    sender.enabled = NO;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *dict = @{@"mobile":_TFUserName.text,
                           @"password":[Utils processedPassWordString:_TFPassWord.text phoneNumber:_TFUserName.text]
                           };
    [manager POST:kLoginURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        _myHUD.hidden = YES;
        NSLog(@"%@ %@",operation,responseObject);
        NSDictionary *resDict = (NSDictionary *)responseObject;
        if ([resDict[@"err_code"] intValue] == 0) {
            [self _saveUserProfile:resDict[@"user"] pass:_TFPassWord.text];
            [self backClick:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyLoginSuccess object:nil];
            [[NSNotificationCenter  defaultCenter] postNotificationName:kNotifyNeedReloadTaskList object:nil];
//            [self dismissViewControllerAnimated:YES completion:nil];
        }else if ([resDict[@"err_code"] intValue] == 2051){
            [self showHUDText:@"登录失败" xOffset:0 yOffset:0];
        }
        sender.enabled = YES;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@ %@",operation,error);
        sender.enabled = YES;
    }];
}

- (void)_saveUserProfile:(NSDictionary *)dict pass:(NSString *)password{
    NSError *error;
    User *user = [[User alloc]initWithDictionary:dict error:&error];
    user.passWord = password;
    
    if (error) {
        NSLog(@"%@",error);
    }
    UserManager *mgr = [UserManager sharedManager];
    mgr.hasLogin = YES;
    [mgr setLoginUser:user];
}
@end
