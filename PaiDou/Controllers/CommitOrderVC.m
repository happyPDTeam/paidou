//
//  CommitOrderVC.m
//  PaiDou
//
//  Created by JSen on 14/11/11.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "CommitOrderVC.h"
#import "ProductDetailMD.h"
#import "CustomStepper.h"
#import "OrderSummaryMD.h"
#import "CommPay.h"
#import "BuySuccessVC.h"
#import "WXApi.h"
#import "PurchaseSuccessViewController.h"

@interface CommitOrderVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>{
    CustomStepper *_stepper;
    NSInteger _number;
    NSInteger _dous;
    NSDecimalNumber *_lastMoney;//需要付的前
    NSMutableDictionary *_orderInfoDict;
    
    NSDecimalNumber *_v; //  钱豆比率  ¥/豆
    
    UITextField *_alertTF;
    
}
@property (nonatomic, retain)  OrderSummaryMD *orderModel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *nameCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *priceCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *payMethodsCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *canUseCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_canUseCount;
@property (weak, nonatomic) IBOutlet UISwitch *sw;
- (IBAction)swAct:(UISwitch *)sender;
@property (strong, nonatomic) IBOutlet UITableViewCell *numberCell;

@property (strong, nonatomic) IBOutlet UITableViewCell *quhuoCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_quhuoTime;
@property (strong, nonatomic) IBOutlet UITableViewCell *beizhuCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_beizhu;


@property (strong, nonatomic) IBOutlet UITableViewCell *lastPriceCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (weak, nonatomic) IBOutlet UITextField *TFOriginalPrice;
- (IBAction)TFChanged:(UITextField *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *payMethodBtns;
- (IBAction)payMethodAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lb_lastPrice;

@property (weak, nonatomic) IBOutlet UILabel *lb_canUse_paidou_num;
@property (weak, nonatomic) IBOutlet UILabel *lb_canUse_paidou_yuan;
@end

@implementation CommitOrderVC


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _number = 1;
    _dous = _detailModel.score_max;
    [self.view addSubview:_tableView];
    _stepper = [[CustomStepper alloc] init];
    UIView *calcView = [_stepper getCustomStepper];
    
    calcView.origin = CGPointMake(SCREEN_WIDTH-130, 7);
    [_numberCell.contentView addSubview:calcView];
    
   
    [self _addTableFooterView];
    
    _TFOriginalPrice.enabled = NO;
    if (_detailModel) {
        
        [self calHuiLv];
        
        _lb_name.text = [NSString stringWithFormat:@"%@",_detailModel.title];
        _TFOriginalPrice.text = [NSString stringWithFormat:@"%@",[Utils fenToYuan:_detailModel.sale_price]];
        
        _lb_canUse_paidou_num.text = [NSString stringWithFormat:@"%d",_detailModel.score_max];
        _lb_canUse_paidou_yuan.text = [NSString stringWithFormat:@"%@元",[Utils fenToYuan:_detailModel.original_price - _detailModel.sale_price]];
        
      NSString *money =  [Utils fenToYuan:_detailModel.sale_price];
//      NSString *dou = [Utils moneyToDou:(_detailModel.original_price - _detailModel.sale_price)];
        
     //   _lb_lastPrice.text = [NSString stringWithFormat:@"¥%@ + %@",money,dou ];
          _lb_lastPrice.text = [NSString stringWithFormat:@"¥%@ + %d",money,_detailModel.score_max ];
        
    }
    
    for (UIButton *btn in _payMethodBtns) {
        if (btn.tag == 100) {
            btn.selected = YES;
        }
        
        
    }
    
    [_stepper textDidChanged:^(int number) {
        _number = number;
        [self change:_number];
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notiAction) name:kSafePaySuccessNotificatino object:nil];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];
    _lb_quhuoTime.text = dateStr;
    
    
    
    //ui
    _TFOriginalPrice.layer.borderWidth = 0.5;
    _TFOriginalPrice.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
    _TFOriginalPrice.layer.cornerRadius = 5;
    _TFOriginalPrice.layer.masksToBounds = YES;
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
     self.navigationItem.titleView = [Utils titleLabelWithTitle:@"确认支付"];
}

#pragma mark -钱豆汇率 ¥/豆
//MARK: 计算都以分为单位
- (void)calHuiLv{
    NSDecimalNumberHandler*roundPlain = [NSDecimalNumberHandler                                   decimalNumberHandlerWithRoundingMode: NSRoundPlain                                   scale:2                                   raiseOnExactness:NO                                   raiseOnOverflow:NO                                   raiseOnUnderflow:NO                                   raiseOnDivideByZero:YES];
    
    NSDecimalNumber *ori = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",_detailModel.original_price]];
     NSDecimalNumber *sal = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",_detailModel.sale_price]];
    NSDecimalNumber *sal_dou = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",_detailModel.score_max]];
    
    NSDecimalNumber *chajia = [ori decimalNumberBySubtracting:sal withBehavior:roundPlain];
    _v = [chajia decimalNumberByDividingBy:sal_dou withBehavior:roundPlain];
    NSLog(@"一个豆值 %@ 钱",_v);
}

- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self addTapToCell];
}


#pragma mark - 支付通知
- (void)notiAction{
    
//    BuySuccessVC *buy = [[BuySuccessVC alloc] init];
//    buy.orderModel = _orderModel;
//    buy.productModel = _detailModel;
//    buy.timeStr = _lb_quhuoTime.text;
//    buy.xuqiuStr = _lb_beizhu.text;
//    [self.navigationController pushViewController:buy animated:YES];
    
    PurchaseSuccessViewController *purchase = [[PurchaseSuccessViewController alloc ]init];
    purchase.orderModel = _orderModel;
    purchase.productModel = _detailModel;
    [self.navigationController pushViewController:purchase animated:YES];
}

- (void)change:(NSInteger)number{
    if (_sw.isOn) {
        _TFOriginalPrice.text = [Utils priceWithUnitPrice:_detailModel.sale_price count:number];

        NSString *douNeed = [Utils moneyToDou:(_detailModel.original_price - _detailModel.sale_price) * number];
       // _dous = [douNeed integerValue];
        
        _dous = _detailModel.score_max * _number;
        _lb_lastPrice.text = [NSString stringWithFormat:@"¥%@ + %d豆",_TFOriginalPrice.text, _dous];
        
    }else{

        _TFOriginalPrice.text = [Utils priceWithUnitPrice:_detailModel.original_price count:number];
        _lb_lastPrice.text = [NSString stringWithFormat:@"¥%@",_TFOriginalPrice.text];
        _dous = 0;
    }
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
        {
            return 4;
        }
            break;
        case 1:{
            return 1;
        }
        case 2:{
            return 2;
        }
        case 3:{
            return 1;
        }
            
        default:
            return 0;
            break;
    }
    return 0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                
                return _nameCell;
                break;
            case 1:{
                return _priceCell;
            }
            case 2:{
                return _numberCell;
            }
            case 3:{
                return _canUseCell;
            }
                
            default:
                return nil;
                break;
        }
    }else if (indexPath.section == 1) {
        return _payMethodsCell;
    }else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            return _quhuoCell;
        }else if (indexPath.row == 1){
            return _beizhuCell;
        }
    }else if (indexPath.section == 3) {
        return _lastPriceCell;
    }
    
    
   
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 1) {
                return 56;
            }
            
            else{
                return 50;
            }
        }
            break;
        case 1:{
            return 70;
        }
        case 2:{
            return 40;
        }
        case 3:{
            return 50;
        }
            
        default:
             return 0;
            break;
    }
        return 0;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.1;
    }
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            //取货时间
            [self showDatePicker];
        }else{
            //备注
            [self showInputAlert];
        }
    }
}
- (void)showDatePicker{
    UIView *pbg = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-246, SCREEN_WIDTH, 246)];
    pbg.backgroundColor = [UIColor whiteColor];
    
    
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, pbg.height-216, pbg.width, 216)];
    picker.datePickerMode = UIDatePickerModeDate;
    picker.minimumDate = [NSDate date];
    picker.maximumDate = [NSDate dateWithTimeInterval:30*24*60*60 sinceDate:[NSDate date]];
    [picker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    picker.backgroundColor = [UIColor whiteColor];
  
    [picker setDate:[NSDate date] animated:YES];
    
    [pbg addSubview:picker];
    [self.view addSubview:pbg];
    
    UIButton *btn = [UIButton buttonWithType: UIButtonTypeCustom];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
    btn.frame =CGRectMake(pbg.width-80, 5, 70, 30);
    [pbg addSubview:btn];
    [btn handleControlEvent:UIControlEventTouchUpInside withBlock:^(id sender) {
        [pbg removeFromSuperview];
        [self updateDate:picker.date];
    }];
    
    UIButton *btn02 = [UIButton buttonWithType: UIButtonTypeCustom];
    [btn02 setTitle:@"取消" forState:UIControlStateNormal];
    [btn02 setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
    btn02.frame =CGRectMake(10, 5, 70, 30);
    [pbg addSubview:btn02];
    [btn02 handleControlEvent:UIControlEventTouchUpInside withBlock:^(id sender) {
        [pbg removeFromSuperview];
        
    }];

}
- (void)updateDate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [formatter stringFromDate:date];
    _lb_quhuoTime.text = dateStr;
}

- (void)datePickerChanged:(UIDatePicker *)picker{
    NSLog(@"%@",picker.date);
}

- (void)showInputAlert{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"请输入您的需求" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    
    _alertTF = [alert textFieldAtIndex:0];
    _alertTF.delegate = self;
    [_alertTF addTarget:self action:@selector(alertTFAction:) forControlEvents:UIControlEventEditingChanged];
    if (![_lb_beizhu.text isEqualToString:@"可输入您的特殊需求"]) {
        _alertTF.text = _lb_beizhu.text;
    }

    
    [alert show];
}

- (void)alertTFAction:(UITextField *)tf{
    if (tf.text.length > 50) {
        tf.text = [tf.text substringToIndex:50];
    }
}
#pragma mark - alert delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    UITextField *tf = [alertView textFieldAtIndex:0];
    
    NSLog(@"%d",buttonIndex);
    if (buttonIndex == 1) {
        //确定
        if (tf.text.length) {
            _lb_beizhu.text = tf.text;
        }
        
    }
}


#pragma mark - switch action
- (IBAction)swAct:(UISwitch *)sender {
    if (sender.isOn) {
        _TFOriginalPrice.enabled = NO;
     
        _TFOriginalPrice.text = [NSString stringWithFormat:@"%@",[Utils priceWithUnitPrice:_detailModel.sale_price count:_number]];
        
        // NSString *douNeed = [Utils moneyToDou:(_detailModel.original_price - _detailModel.sale_price) * _number];
       // _dous = [douNeed integerValue];
        
        _dous = _detailModel.score_max * _number;
        _lb_lastPrice.text = [NSString stringWithFormat:@"%@元 + %d豆",_TFOriginalPrice.text,_dous];
    }else{
        _TFOriginalPrice.enabled = NO;

        _TFOriginalPrice.text = [NSString stringWithFormat:@"%@",[Utils priceWithUnitPrice:_detailModel.original_price count:_number]];
        _lb_lastPrice.text = [NSString stringWithFormat:@"%@元",_TFOriginalPrice.text];
        _dous = 0;
    }
    
}

#pragma mark - 输入框变化
- (IBAction)TFChanged:(UITextField *)sender {
    NSLog(@"%@",sender.text);
    if ([sender.text floatValue] >= [[Utils priceWithUnitPrice:_detailModel.sale_price count:_number] floatValue]) {
        NSLog(@"%@", @"I am ok");
    }
}

#pragma mark - tf delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _alertTF) {
        
//        if ([string isEqualToString:@"\n"]){
//            return YES;
//        }
//        NSString * aString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//        if ([aString length] > 50) {
//            textField.text = [aString substringToIndex:50];
//            
//        }
        
        return YES;
//        int MAX_CHARS = 50;
//        NSMutableString *newtxt = [NSMutableString stringWithString:textField.text];
//         [newtxt replaceCharactersInRange:range withString:string];
//         return ([newtxt length] <= MAX_CHARS);
    }
    return YES;
}// return NO to not change text

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}// called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}// called when 'return' key pressed. return NO to ignore

- (void)_addTableFooterView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.width, 50)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(20, 0, view.width-40, view.height);
    NSString *title = @"确认购买";
   
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor appWihteColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage JSenImageNamed:@"btn_yellow.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buyClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [view addSubview:btn];
    self.tableView.tableFooterView = view;
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    
}
#pragma mark - 点击购买
- (void)buyClick:(UIButton *)btn{
   
    if (_number == 0) {
        [self showHUDText:@"最少购买一件商品" xOffset:0 yOffset:0];
        return;
    }
    if (![self check]) {
        return;
    }
    
    //12.1 new
    {

        if (_sw.isOn) {
        [self createOrderTotoalPrice:[NSDecimalNumber decimalNumberWithString:_TFOriginalPrice.text] score:_dous];
        }else{
        [self createOrderTotoalPrice:[NSDecimalNumber decimalNumberWithString:_TFOriginalPrice.text] score:0];
        }
    }
    
    return;
    //12.1以下是考虑了可以自己输入金额的情况，现已不用
    _dous = 0;
    _lastMoney = [[NSDecimalNumber alloc] initWithInt:0];
    
    User *user = [[UserManager sharedManager] readFromDisk];
    
     NSDecimalNumberHandler*roundPlain = [NSDecimalNumberHandler                                   decimalNumberHandlerWithRoundingMode: NSRoundPlain                                   scale:2                                   raiseOnExactness:NO                                   raiseOnOverflow:NO                                   raiseOnUnderflow:NO                                   raiseOnDivideByZero:YES];
    NSDecimalNumber *tfPrice = [NSDecimalNumber decimalNumberWithString:_TFOriginalPrice.text];
    NSDecimalNumber *minPrice = [NSDecimalNumber decimalNumberWithString:[Utils priceWithUnitPrice:_detailModel.sale_price count:_number]];
    NSDecimalNumber *maxPrice = [NSDecimalNumber decimalNumberWithString:[Utils priceWithUnitPrice:_detailModel.original_price count:_number]];
    
    NSDecimalNumber *chajia = [tfPrice decimalNumberBySubtracting:minPrice withBehavior:roundPlain];
    NSDecimalNumber *huilv = [NSDecimalNumber decimalNumberWithString:@"10.00"];
    NSDecimalNumber *douNeed = [chajia decimalNumberByMultiplyingBy:huilv withBehavior:roundPlain];
    //开关状态
    if (_sw.isOn) {
        
        //输入的价格在最低价和最高价之间，则算需要用多少豆来兑换
        if ([tfPrice compare:minPrice] == NSOrderedDescending && [tfPrice compare:maxPrice] == NSOrderedAscending) {
           
            
            //豆不足
            if (user.score < [douNeed integerValue]) {
                NSString *str = [NSString stringWithFormat:@"您的豆不足，还差%d豆",([douNeed integerValue] - user.score)];
                //改最终显示的价格
                _dous = user.score;
                
                //自己的豆值多少钱
                NSDecimalNumber *douValue = [NSDecimalNumber decimalNumberWithString:[Utils douToYuan:_dous]];
                //加钱
                NSDecimalNumber *jiaqian = [tfPrice decimalNumberByAdding:douValue withBehavior:roundPlain];
                _TFOriginalPrice.text = [NSString stringWithFormat:@"%@",jiaqian];
                _lb_lastPrice.text = [NSString stringWithFormat:@"¥%@ + %d豆",_TFOriginalPrice.text, _dous];
                //需要付多少钱
                _lastMoney = jiaqian;
                
                [self showHUDText:str xOffset:0 yOffset:0];
                return;
            }else{
                //豆足就支付
                NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:_TFOriginalPrice.text];
                _lastMoney = price;
                
                NSDecimalNumber *chajia = [maxPrice decimalNumberBySubtracting:_lastMoney withBehavior:roundPlain];
                
                //需要支付的豆子数量
                NSDecimalNumber *chaDou = [chajia decimalNumberByMultiplyingBy:huilv withBehavior:roundPlain];
                
                _dous = [chaDou integerValue];
                
//                RIButtonItem *cancle = [RIButtonItem itemWithLabel:@"取消"];
//                RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
//                    //支付
//                    [self createOrderTotoalPrice:_lastMoney score:_dous];
//                }];
                
              //  NSString *s = [NSString stringWithFormat:@"您将要支付 ¥%@ + %d豆",_lastMoney,_dous];
                
//                UIAlertView *alert = [[UIAlertView alloc ]initWithTitle:@"提示" message:s cancelButtonItem:cancle otherButtonItems:sure, nil];
//                [alert show];
                
                _lb_lastPrice.text = [NSString stringWithFormat:@"¥%@ + %d豆",_lastMoney,_dous];
                //支付
                [self createOrderTotoalPrice:_lastMoney score:_dous];
                
                
            }
        }else if ([tfPrice compare:minPrice] == NSOrderedSame ) {
            //用户没有改价格  输入框中价格和该商品的最低价格一样
            //付款  （价格+逗比）x 数量
            [self createOrderTotoalPrice:tfPrice score:_number * _detailModel.score_max];
        }else if ([tfPrice compare:maxPrice] == NSOrderedSame){
            //
            [self createOrderTotoalPrice:tfPrice score:0];
        }
    }else {
        //开关是关着的，即全额付款
        //检查价格是否正确
        if ([tfPrice compare:maxPrice] == NSOrderedSame) {
            //正确的 付款了
            [self createOrderTotoalPrice:tfPrice score:0];
        }else{
            //错误的，不付款
            
        }
        
    }
   
    
    //改用户的豆
    //[[UserManager sharedManager] decreaseDou:[chaDou integerValue]];
   
}

#pragma mark - 生成订单
- (void)createOrderTotoalPrice:(NSDecimalNumber *)totalPrice score:(NSInteger)totalScore{
    NSString *type = nil;
    if (_sw.isOn) {
        type = kProductTypeMixed;
    }else{
        type = kProductTypeCash;
    }
    
    NSString *payType = nil;
    for (UIButton *btn in _payMethodBtns) {
        if (btn.selected ) {
         payType =  (btn.tag == 100) ? kPayMethodAli:kPayMethodWeiXin;
           
        }
        
    }
    
    //是否安装微信
    if ([payType isEqualToString:kPayMethodWeiXin]) {
        //
        if (![WXApi isWXAppInstalled]) {
            RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消"];
            RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[WXApi getWXAppInstallUrl]]];
            }];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"请安装微信服务" cancelButtonItem:cancel otherButtonItems:sure, nil];
            [alert show];
            return;
        }
    }
    
    if (_detailModel == nil) {
        NSLog(@"产品信息错误");
        return;
    }
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.userInteractionEnabled = YES;
    HUD.minShowTime = 1.0f;
    HUD.labelText = @"正在创建订单";
    
    _orderInfoDict = nil;
    //step 1 生成订单信息
    
    NSDecimalNumber *toFen = [NSDecimalNumber decimalNumberWithString:@"100"];
    
    //用分表示的商品价格
    NSDecimalNumber *fenPrice = [totalPrice decimalNumberByMultiplyingBy:toFen];
    
    
    NSDictionary *infoDic = @{
                              @"uid":[[UserManager sharedManager]uid],
                              @"bid":_detailModel.biz_id,
                              @"type":type,
                              @"pid":_detailModel.pid,
                              @"unit_price":@(_detailModel.sale_price),
                              @"count":@(_number),
                              @"total_price":fenPrice,
                              @"unit_score":@0,
                                @"total_score":@(totalScore)
                              };
    
    _orderInfoDict = [NSMutableDictionary dictionaryWithDictionary:infoDic];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:kCreateOrderURL parameters:_orderInfoDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        
        if ([responseObject[@"err_code"]intValue] == 0) {
            HUD.labelText = @"订单创建成功";
            NSError *error = nil;
            OrderSummaryMD *order = [[OrderSummaryMD alloc]initWithDictionary:responseObject[@"order_brief_info"] error:&error];
            if (error) {
                NSLog(@"%@",error);
                return;
            }
            _orderModel = order;
            [CommPay payWithUID:[[UserManager sharedManager]uid] OID:_orderModel.oid payChannel:payType];
            
        }else {
            HUD.mode = MBProgressHUDModeText;
            HUD.labelText = responseObject[@"err_msg"];
           [HUD hide:YES afterDelay:1.5];
        }
         [HUD hide:YES afterDelay:1.5];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        HUD.labelText = @"操作失败";
         [HUD hide:YES afterDelay:0.9];
    }];

}

/*
 纯积分支付 已经被废除？
#pragma mark - score pay action
- (void)_scorePay{
//    if (![UserManager sharedManager].hasLogin) {
//        [self showHUDText:@"请先登录" xOffset:0 yOffset:0];
//        return;
//    }
//    User *user = [[UserManager sharedManager] readFromDisk];
//#warning 浮点值的比较，注意不一定对
//    if ([[self _getTotalPrice]floatValue ] > user.score) {
//        [self showHUDText:@"积分不足" xOffset:0 yOffset:0];
//        return;
//    }
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.userInteractionEnabled = NO;
    HUD.minShowTime = 1.0f;
    NSDictionary *info = @{
                           @"uid":user.uid,
                           @"bid":_productModel.bid,
                           @"type":_productModel.type,
                           @"pid":_productModel.pid,
                           @"unit_score":@(_productModel.redeem_score),
                           @"count":@(_productNumber),
                           @"total_score":@(_productModel.redeem_score * _productNumber),
                           @"unit_price":@0,
                           @"total_price":@0
                           };
    _orderInfoDict = [NSMutableDictionary dictionaryWithDictionary:info];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager POST:kCreateOrderURL parameters:_orderInfoDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        HUD.hidden = YES;
        if ([responseObject[@"err_code"] intValue] == 0) {
            NSError *error;
            OrderSummaryMD *model = [[OrderSummaryMD alloc]initWithDictionary:responseObject[@"order_brief_info"] error:&error];
            if (error) {
                NSLog(@"%@",error);
                return ;
            }
            _orderModel = model;
            [self paySuccess:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        HUD.hidden =YES;
        [self showHUDText:@"支付失败" xOffset:0 yOffset:0];
    }];
}
*/

#pragma mark - check price
- (BOOL)check{
    
    return [self checkBeans];
    
    
    
    //12.1 不让输入金额
    //检查金额
    if (_sw.isOn) {
        if ([_TFOriginalPrice.text floatValue] < [[Utils fenToYuan:_detailModel.sale_price] floatValue] * _number) {
            [self showHUDText:@"请输入足够金额" xOffset:0 yOffset:0];
            return NO;
        }else if([_TFOriginalPrice.text floatValue] > [[Utils fenToYuan:_detailModel.original_price] floatValue] * _number){
            [self showHUDText:@"您输入金额过多" xOffset:0 yOffset:0];
            return NO;
        }else{
            //检查豆子数量
            return [self checkBeans];
        }
    }else{
        if ([_TFOriginalPrice.text floatValue] != [[Utils priceWithUnitPrice:_detailModel.original_price count:_number] floatValue]) {
            [self showHUDText:@"金额不对,请调整" xOffset:0 yOffset:0];
            return NO;
        }
    }
    return YES;
}

- (BOOL)checkBeans{
    
    if (![[UserManager sharedManager] hasLogin]) {
        [self showHUDText:@"请先登录" xOffset:0 yOffset:0];
        return NO;
    }
    
    User *user = [[UserManager sharedManager] readFromDisk];
    if ( user.score < _dous) {
        [self showHUDText:@"您的豆不足,请使用钱抵付" xOffset:0 yOffset:0];
        
        //更新价格
        {
            NSDecimalNumberHandler*round=[NSDecimalNumberHandler                                   decimalNumberHandlerWithRoundingMode:NSRoundPlain                                   scale:2      raiseOnExactness:NO                                   raiseOnOverflow:NO
                                                                                                                       raiseOnUnderflow:NO              raiseOnDivideByZero:YES];
            //需要的总豆
            NSDecimalNumber *totalScore = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",_dous]];
            //我的豆数
            NSDecimalNumber *myScore = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",user.score]];
            //总豆 - 我的豆
            NSDecimalNumber *chaScore = [totalScore  decimalNumberBySubtracting:myScore withBehavior:round];
            
            //还需要的钱数(以 分 为单位)
            NSDecimalNumber *m = [chaScore decimalNumberByMultiplyingBy:_v withBehavior:round];
            
            //原来输入框中得钱数（元 为单位）
            NSDecimalNumber *tf_money = [NSDecimalNumber decimalNumberWithString:[Utils yuanToFen:_TFOriginalPrice.text]];
            
            //输入框中应该显示的新的钱数（以分位单位）
            NSDecimalNumber *tf_money_new =[tf_money decimalNumberByAdding:m withBehavior:round];
            //更新输入框钱数
            _TFOriginalPrice.text = [Utils fenToYuan:[tf_money_new integerValue]] ;
            //更新最终显示价格
            _lb_lastPrice.text = [NSString stringWithFormat:@"¥%@ + %d豆",_TFOriginalPrice.text, user.score];
            
            _dous = user.score;
            
        }
        
        return NO;
    }else{
        return YES;
    }
    
    
    NSDecimalNumberHandler*roundUp=[NSDecimalNumberHandler                                   decimalNumberHandlerWithRoundingMode:NSRoundPlain                                   scale:0                                   raiseOnExactness:NO                                   raiseOnOverflow:NO                                   raiseOnUnderflow:NO                                   raiseOnDivideByZero:YES];
    NSDecimalNumber *tfprice = [NSDecimalNumber decimalNumberWithString:_TFOriginalPrice.text];
    NSDecimalNumber *salePrice = [NSDecimalNumber decimalNumberWithString:[Utils priceWithUnitPrice:_detailModel.sale_price count:_number]];
    
    //差价
    NSDecimalNumber *middlePrice = [tfprice decimalNumberBySubtracting:salePrice withBehavior:roundUp];
    
    NSDecimalNumber *m = [NSDecimalNumber decimalNumberWithString:@"10"];
    
    //差价代表的豆子数量
    NSDecimalNumber *beans = [middlePrice decimalNumberByMultiplyingBy:m withBehavior:roundUp];
   
    
}



- (IBAction)payMethodAction:(UIButton *)sender {
    
    if (sender.selected) {
        return;
    }
    for (UIButton *oneBtn in _payMethodBtns) {
        if (sender == oneBtn) {
            oneBtn.selected = YES;
        }else{
            oneBtn.selected = NO;
        }
    }

}

- (void)dealloc{
    [_stepper removeObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addTapToCell{
    NSIndexPath *onePath = [NSIndexPath indexPathForRow:0 inSection:2];
     NSIndexPath *twoPath = [NSIndexPath indexPathForRow:1 inSection:2];
    NSArray *arr = @[onePath,twoPath];
    NSArray *arrCells = [_tableView visibleCells];
    for (UITableViewCell *cell in arrCells) {
        NSIndexPath *path = [self.tableView indexPathForCell:cell];
        if ([arr containsObject:path]) {
            continue;
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTable:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [cell.contentView addGestureRecognizer:tap];
    }
    
}



- (void)tapTable:(UITapGestureRecognizer *)tap {
    [_TFOriginalPrice resignFirstResponder];
    [_stepper.textField resignFirstResponder];
    
//    UITouch *touch = [[event allTouches] anyObject];
//    CGPoint point = [touch locationInView:self.view];
//    
//    CGPoint p = [self.view convertPoint:point toView:self.tableView];
    
    
}


@end
