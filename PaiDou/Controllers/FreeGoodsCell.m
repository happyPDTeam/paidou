//
//  FreeGoodsCell.m
//  PaiDou
//
//  Created by JeremyRen on 14/12/19.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "FreeGoodsCell.h"
#import "SDWebImageManager.h"

@implementation FreeGoodsCell

- (void)awakeFromNib {
    // Initialization code
    
    [self createStrikeLabel];
    
    _savePrice.layer.masksToBounds = YES;
    
    _savePrice.layer.cornerRadius = 2;
    
    _goodsImage.layer.masksToBounds = YES;
    
    _goodsImage.layer.cornerRadius = 2;
}

//创建strikeLabel
-(void)createStrikeLabel
{
    __strikeLabel = [[StrikeLabel alloc] initWithFrame:CGRectMake(111, 75, 200, 20)];
    
    __strikeLabel.text = @"5.0元";
    
    __strikeLabel.textColor = [UIColor appGrayColor];
    
    __strikeLabel.font = [UIFont systemFontOfSize:10];
    
    [self.contentView addSubview:__strikeLabel];
}

//将ProductDetailMD存储的信息显示到控件上
-(void)config:(ProductSummaryMD *)model
{
    NSString * encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault, (CFStringRef)model.avatar, NULL, NULL,  kCFStringEncodingUTF8 ));
    
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:encodedString] options:SDWebImageAllowInvalidSSLCertificates progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        [self.goodsImage setImage:image];
    }];
    
    self.titleLabel.text = model.title;
    
    self.shopLabel.text = model.biz_name;
    
    NSString * currentPrice = [Utils fenToYuan:model.sale_price];
    
    if (![currentPrice isEqualToString:@"0"]) {
        self.freeImage.hidden = YES;
    }
    
    //self.currenPrice.text = [NSString stringWithFormat:@"%@元+%d派豆", currentPrice, model.score_max];
    
    [self setLabel:self.currenPrice price:currentPrice bean:model.score_max];
    
    self.savePrice.text = [NSString stringWithFormat:@"省%@元",[Utils fenToYuan:model.market_price - model.sale_price]];
    
    __strikeLabel.text = [NSString stringWithFormat:@"%@元", [Utils fenToYuan:model.market_price]];
    
    self.countLabel.text = [NSString stringWithFormat:@"%d人已兑换", model.sold_count];
}

-(void)setLabel:(UILabel *)label price:(NSString *)price bean:(int)bean
{
    NSString * beanString = [NSString stringWithFormat:@"%d", bean];
    
    NSString * string = [NSString stringWithFormat:@"%@元+%@派豆", price, beanString];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSRange range1 = NSMakeRange(0, price.length);
    
    NSRange range2 = NSMakeRange(price.length + 1, beanString.length + 1);
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appRedTextColor] range:range1];
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor app797979Color] range:NSMakeRange(price.length, 1)];
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor appBlueTextColor] range:range2];
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor app6a6a6aColor] range:NSMakeRange(range2.length + range1.length + 1, 2)];
    
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, price.length)];
    
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(price.length, 1)];
    
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(price.length + 1, beanString.length + 1)];
    
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(range2.length + range1.length + 1, 2)];
    
    label.attributedText = str;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end