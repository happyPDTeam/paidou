 //
//  TabFirstNewVC.m
//  PaiDou
//
//  Created by JSen on 14/12/17.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//
/*
 <Station>
 [station_name]: 元通
 [station_id]: 40
 [latitude]: 32.001636
 [longitude]: 118.728019
 </Station>
 
 */

#import "TabFirstNewVC.h"
#import "LocationMgr.h"
#import "StationLine.h"
#import "ClickableUIView.h"
#import "StationSelectViewController.h"
#import "ClickableUIView.h"
#import "ShowBeansView.h"
#import "JOLImageSlide.h"
#import "JOLImageSlider.h"
#import "ProductCell.h"
#import "FreeGoodsCell.h"
#import "ScoreRecordVC.h"
#import "TabFirstViewController.h"
#import "TabSecondViewController.h"
#import "ProductDetailMD.h"
#import "TaskModel.h"
#import "CommodityDetailVC.h"
#import "TaskModel.h"
#import "URLTaskVC.h"
#import "TaskDetailInfo.h"
#import "TaskDetail01VC.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "ProductSummaryMD.h"
#import "TopicViewController.h"
#import "GuideView.h"
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"

@interface TabFirstNewVC ()<UITableViewDataSource, UITableViewDelegate, JOLImageSliderDelegate >
{
    NSMutableArray *_metroLineArr;
    StationSelectViewController *_stationVC;
    
    Station *_currStation; //当前站点
    
    ClickableUIView *_bgView;
    
    ShowBeansView * _beanView;
    
    UIView * _tableHeaderview;
    
    UITableView * _tableView;
    
    JOLImageSlider * _joliImageSlider;
}
@property (strong, nonatomic) IBOutlet ClickableUIView *titleVew;

@property (weak, nonatomic) IBOutlet UILabel *lb_title;

@property (strong, nonatomic) NSMutableArray * bannerArray;

@property (strong, nonatomic) NSMutableArray * productArray;

@property (strong, nonatomic) NSMutableArray * taskArray;

@property (strong, nonatomic) TaskModel * taskModel;

@end

@implementation TabFirstNewVC

-(void)viewWillAppear:(BOOL)animated
{
    [self setBeanView];
    
    NSIndexPath * selected = [_tableView indexPathForSelectedRow];
    
    if(selected)
    {
        [_tableView deselectRowAtIndexPath:selected animated:NO];
    }
    
//    NSUserDefaults * settings1 = [NSUserDefaults standardUserDefaults];
//    NSString *key1 = [NSString stringWithFormat:@"is_first"];
//    NSString *value = [settings1 objectForKey:key1];
//    
//    if (!value) {
//        AppDelegate *del = [[UIApplication sharedApplication]delegate];
//        
//        GuideView * view = [[GuideView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) array:@[@"引导页1.jpg", @"引导页2.jpg"]];
//        
//        [view becomeFirstResponder];
//        
//        [del.window addSubview:view];
//    }
//    
//    NSUserDefaults * setting = [NSUserDefaults standardUserDefaults];
//    NSString * key = [NSString stringWithFormat:@"is_first"];
//    [setting setObject:[NSString stringWithFormat:@"false"] forKey:key];
//    [setting synchronize];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    _metroLineArr = [NSMutableArray new];
    
    _titleVew.size = CGSizeMake(150, 40);
    self.navigationItem.titleView = _titleVew;
    
    _currStation = [[Station alloc] init];
    _currStation.station_name = @"元通";
    _currStation.station_id = 40;
    
    _stationVC = [[StationSelectViewController alloc ]init];
    
    _bgView = [[ClickableUIView alloc ]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _bgView.backgroundColor = [UIColor clearColor];
   [_bgView handleComplemetionBlock:^(ClickableUIView *view) {
       _bgView.hidden = !_bgView.hidden;
   }];
    _stationVC.view.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    [_bgView addSubview:_stationVC.view];
    
    self.taskArray = [NSMutableArray arrayWithCapacity:0];
    
    AppDelegate *del = [[UIApplication sharedApplication]delegate];
    [del.window addSubview:_bgView];
    _bgView.hidden = YES;
    
    [_titleVew handleComplemetionBlock:^(ClickableUIView *view) {
        _bgView.hidden = !_bgView.hidden;
    }];
    
    
    [self createTableView];
    
    [self createTableHeaderView];
    
    [self createBeanView];
    
    //[self createActiveImage];
    
    [self createEarnBean];
    
    [self createChangeProducts];
    
    [self loadBannerData];
    
    [self loadTaskData];
    
    [self loadProductData];
    
    [[LocationMgr shared] startLocate];

    if ( ! [[NSFileManager defaultManager] fileExistsAtPath:kMetroLinePath]) {
        [self loadSubwayStations];
    }else{
        NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithFile:kMetroLinePath];
        [_metroLineArr addObjectsFromArray:arr];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadList) name:kNotifyNeedReloadTaskList object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotifyDidSelectStation object:nil queue:[NSOperationQueue currentQueue] usingBlock:^(NSNotification *note) {
        Station *station = note.userInfo[@"S"];
        _lb_title.text = station.station_name;
        _currStation = station;
        _bgView.hidden = YES;
    }];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc ]initWithTitle:@"分享" style:UIBarButtonItemStylePlain target:self action:@selector(shareClick:)];
    self.navigationItem.rightBarButtonItem = item;
    // Do any additional setup after loading the view from its nib.
}
- (void)shareClick:(UIBarButtonItem *)sender{
//    [ShareSDK connectWeChatWithAppId:@"wxffe514bedc4db701"
//                           appSecret:@"2180362e1b76c94dc8910f77dc24d45d"
//                           wechatCls:[WXApi class]];
    [WXApi registerApp:@"wxffe514bedc4db701"];
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:CONTENT
                                       defaultContent:@""
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"ShareSDK"
                                                  url:@"http://www.apaidou.com"
                                          description:@"这是一条测试信息"
                                            mediaType:SSPublishContentMediaTypeNews];
    //定制QQ空间信息
    [publishContent addQQSpaceUnitWithTitle:@"Hello QQ空间"
                                        url:INHERIT_VALUE
                                       site:nil
                                    fromUrl:nil
                                    comment:INHERIT_VALUE
                                    summary:INHERIT_VALUE
                                      image:INHERIT_VALUE
                                       type:INHERIT_VALUE
                                    playUrl:nil
                                       nswb:nil];
    //定制微信好友信息
    [publishContent addWeixinSessionUnitWithType:INHERIT_VALUE
                                         content:INHERIT_VALUE
                                           title:@"Hello 微信好友!"
                                             url:INHERIT_VALUE
                                      thumbImage:[ShareSDK imageWithUrl:@"http://img1.bdstatic.com/img/image/67037d3d539b6003af38f5c4c4f372ac65c1038b63f.jpg"]
                                           image:INHERIT_VALUE
                                    musicFileUrl:nil
                                         extInfo:nil
                                        fileData:nil
                                    emoticonData:nil];
    
    //定制微信朋友圈信息
    [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInteger:SSPublishContentMediaTypeMusic]
                                          content:INHERIT_VALUE
                                            title:@"Hello 微信朋友圈!"
                                              url:@"http://y.qq.com/i/song.html#p=7B22736F6E675F4E616D65223A22E4BDA0E4B88DE698AFE79C9FE6ADA3E79A84E5BFABE4B990222C22736F6E675F5761704C69766555524C223A22687474703A2F2F74736D7573696332342E74632E71712E636F6D2F586B303051563558484A645574315070536F4B7458796931667443755A68646C2F316F5A4465637734356375386355672B474B304964794E6A3770633447524A574C48795333383D2F3634363232332E6D34613F7569643D32333230303738313038266469723D423226663D312663743D3026636869643D222C22736F6E675F5769666955524C223A22687474703A2F2F73747265616D31382E71716D757369632E71712E636F6D2F33303634363232332E6D7033222C226E657454797065223A2277696669222C22736F6E675F416C62756D223A22E5889BE980A0EFBC9AE5B08FE5B7A8E89B8B444E414C495645EFBC81E6BC94E594B1E4BC9AE5889BE7BAAAE5BD95E99FB3222C22736F6E675F4944223A3634363232332C22736F6E675F54797065223A312C22736F6E675F53696E676572223A22E4BA94E69C88E5A4A9222C22736F6E675F576170446F776E4C6F616455524C223A22687474703A2F2F74736D757369633132382E74632E71712E636F6D2F586C464E4D31354C5569396961495674593739786D436534456B5275696879366A702F674B65356E4D6E684178494C73484D6C6A307849634A454B394568572F4E3978464B316368316F37636848323568413D3D2F33303634363232332E6D70333F7569643D32333230303738313038266469723D423226663D302663743D3026636869643D2673747265616D5F706F733D38227D"
                                       thumbImage:[ShareSDK imageWithUrl:@"http://img1.bdstatic.com/img/image/67037d3d539b6003af38f5c4c4f372ac65c1038b63f.jpg"]
                                            image:INHERIT_VALUE
                                     musicFileUrl:@"http://mp3.mwap8.com/destdir/Music/2009/20090601/ZuiXuanMinZuFeng20090601119.mp3"
                                          extInfo:nil
                                         fileData:nil
                                     emoticonData:nil];
    
    //定制微信收藏信息
    [publishContent addWeixinFavUnitWithType:INHERIT_VALUE
                                     content:INHERIT_VALUE
                                       title:NSLocalizedString(@"TEXT_HELLO_WECHAT_FAV", @"Hello 微信收藏!")
                                         url:INHERIT_VALUE
                                  thumbImage:[ShareSDK imageWithUrl:@"http://img1.bdstatic.com/img/image/67037d3d539b6003af38f5c4c4f372ac65c1038b63f.jpg"]
                                       image:INHERIT_VALUE
                                musicFileUrl:nil
                                     extInfo:nil
                                    fileData:nil
                                emoticonData:nil];
    
    //定制QQ分享信息
    [publishContent addQQUnitWithType:INHERIT_VALUE
                              content:INHERIT_VALUE
                                title:@"Hello QQ!"
                                  url:INHERIT_VALUE
                                image:INHERIT_VALUE];
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
//    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:NO
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:[UIApplication sharedApplication].delegate];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    id<ISSShareOptions> shareOptions = [ShareSDK defaultShareOptionsWithTitle:NSLocalizedString(@"TEXT_SHARE_TITLE", @"内容分享")
                                                              oneKeyShareList:[NSArray defaultOneKeyShareList]
                                                               qqButtonHidden:YES
                                                        wxSessionButtonHidden:YES
                                                       wxTimelineButtonHidden:YES
                                                         showKeyboardOnAppear:NO
                                                            shareViewDelegate:nil
                                                          friendsViewDelegate:nil
                                                        picViewerViewDelegate:nil];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:authOptions
                      shareOptions:shareOptions
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];

    
}

-(void)reloadList
{
    [self loadTaskData];
}

-(void)createTableHeaderView
{
    _tableHeaderview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 252)];
    
    _tableHeaderview.backgroundColor = [UIColor whiteColor];
    
    _tableView.tableHeaderView = _tableHeaderview;
}

-(void)createBeanView
{
    //派豆剩余
    _beanView = [[ShowBeansView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];

    __weak TabFirstNewVC * vc = self;
    
    _beanView.block = ^()
    {
//        ScoreRecordVC *score = [[ScoreRecordVC alloc] init];
//        BaseNavigationController *ns = [[BaseNavigationController alloc] initWithRootViewController:score];
//        [self.navigationController pushViewController:score animated:YES];
        [vc.navigationController pushViewController:[[ScoreRecordVC alloc] init] animated:YES];
    };
    
    [_tableHeaderview addSubview:_beanView];
}

-(void)setBeanView
{
    User *user = [[UserManager sharedManager] readFromDisk];
    
    NSString * str1;
    
    NSString * str2;
    
    if ([[UserManager sharedManager] hasLogin])
    {
        str1 = [NSString stringWithFormat:@"已省: %@元",[Utils fenToYuan:user.score_money]];
        
        str2 = [NSString stringWithFormat:@"剩余: %d派豆",user.score];
    }
    
    else
    {
        str1 = [NSString stringWithFormat:@"已省：0元"];
        
        str2 = [NSString stringWithFormat:@"剩余：0派豆"];
    }
    
    [_beanView setTitle:str1 secondTitle:str2 buttonTitle:@"派豆明细》"];
}

//活动图片
//-(void)createActiveImage
//{
//    _joliImageSlider = [[JOLImageSlider alloc] initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH, 120) andSlides:nil];
//    
//    _joliImageSlider.backgroundColor = [UIColor blackColor];
//    
//    [_tableHeaderview addSubview:_joliImageSlider];
//}

-(void)createEarnBean
{
//    UIButton * earnButton = [ViewManager createButton:CGRectMake(((SCREEN_WIDTH / 2) - 130) / 2, 192, 50, 50) bgImage:nil image:@"zhuan" title:nil method:@selector(click:) target:self];
//    
//    earnButton.layer.masksToBounds = YES;
//    
//    earnButton.layer.cornerRadius = 20;
//    
//    //earnButton.tag = 1;
//    
//    [earnButton setImage:[UIImage JSenImageNamed:@"zhuan_highlight"] forState:UIControlStateHighlighted];
//    
//    [_tableHeaderview addSubview:earnButton];
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH / 2) - 130) / 2, 192, 50, 50)];
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    imageView.image = [UIImage imageNamed:@"zhuan"];
    
    imageView.layer.masksToBounds = YES;
    
    imageView.layer.cornerRadius = 20;
    
    [_tableHeaderview addSubview:imageView];
    
    UILabel * earnBeanLabel = [ViewManager createLabel:CGRectMake(((SCREEN_WIDTH / 2) - 130) / 2 + 58, 195, 75, 30) title:@"赚取派豆" font:18 textColor:[UIColor appLightGrayColor]];
    
    [_tableHeaderview addSubview:earnBeanLabel];
    
    UILabel * earnfreeLabel = [ViewManager createLabel:CGRectMake(((SCREEN_WIDTH / 2) - 130) / 2 + 58, 214, 75, 30) title:@"免费赚取派豆" font:10 textColor:[UIColor appGrayColor]];
    
    [_tableHeaderview addSubview:earnfreeLabel];
    
    UIButton * button = [ViewManager createButton:CGRectMake(0, 180, SCREEN_WIDTH / 2, 72) bgImage:nil image:nil title:nil method:@selector(click:) target:self];
    
    button.tag = 1;
    
    [_tableHeaderview addSubview:button];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2, 191, 0.3, 50)];
    
    view.layer.borderWidth = 0.5;
    
    view.layer.borderColor = [UIColor appLineColor].CGColor;
    
    [_tableHeaderview addSubview:view];
}

-(void)createChangeProducts
{
//    UIButton * changeButton = [ViewManager createButton:CGRectMake(SCREEN_WIDTH / 2 + ((SCREEN_WIDTH / 2) - 130) / 2, 192, 50, 50) bgImage:nil image:@"dui" title:nil method:@selector(click:) target:self];
//    
//    changeButton.layer.masksToBounds = YES;
//    
//    changeButton.layer.cornerRadius = 20;
//    
//    [changeButton setImage:[UIImage JSenImageNamed:@"dui_highlight"] forState:UIControlStateHighlighted];
//    
//    //changeButton.tag = 2;
//    
//    [_tableHeaderview addSubview:changeButton];
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 + ((SCREEN_WIDTH / 2) - 130) / 2, 192, 50, 50)];
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    imageView.image = [UIImage imageNamed:@"dui"];
    
    imageView.layer.masksToBounds = YES;
    
    imageView.layer.cornerRadius = 20;
    
    [_tableHeaderview addSubview:imageView];
    
    UILabel * changeLabel = [ViewManager createLabel:CGRectMake(SCREEN_WIDTH / 2 + ((SCREEN_WIDTH / 2) - 130) / 2 + 58, 195, 75, 30) title:@"兑换商品" font:18 textColor:[UIColor appLightGrayColor]];
    
    [_tableHeaderview addSubview:changeLabel];
    
    UILabel * lowChangeLabel = [ViewManager createLabel:CGRectMake(SCREEN_WIDTH / 2 + ((SCREEN_WIDTH / 2) - 130) / 2 + 58, 214, 75, 30) title:@"低价兑换商品" font:10 textColor:[UIColor appGrayColor]];
    
    [_tableHeaderview addSubview:lowChangeLabel];
    
    UIButton * button = [ViewManager createButton:CGRectMake(SCREEN_WIDTH / 2, 180, SCREEN_WIDTH / 2, 72) bgImage:nil image:nil title:nil method:@selector(click:) target:self];
    
    button.tag = 2;
    
    [_tableHeaderview addSubview:button];
}

-(void)createTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 49) style:UITableViewStylePlain];
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    _tableView.separatorColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1];
    
    [self.view addSubview:_tableView];
}

//新增加载广告
-(void)loadBannerData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *dict = @{
                           @"station":@"19",
                           @"locate":@"index"
                           };
    [manager GET:kAdvertisementImageURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if (![responseObject[@"banners"] isKindOfClass:[NSNull class]]) {
                
                 
                NSError *err ;
                
                NSArray * array = responseObject[@"banners"];
                
                [self createJS:array];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    
                }
                
            }
        
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)loadTaskData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSString *uid = [[UserManager sharedManager] uid];
    
//    NSDictionary *dict = @{
//                           @"station":@"19",
//                           @"uid":uid
//                           };
    NSMutableDictionary * dic = [NSMutableDictionary new];
    
    [dic setValue:@(_currStation.station_id) forKey:@"station"];
    
    [dic setValue:uid forKey:@"uid"];
    
    [manager GET:kAdvertisementTaskURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if (![responseObject[@"banners"] isKindOfClass:[NSNull class]]) {
                
                
                NSError *err ;
                
                NSArray * array = responseObject[@"task_list"];
                
                [self createTaskModel:array];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    
                }
                
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}


-(void)loadProductData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *dict = @{
                           @"station":@"19"
                           };
    [manager GET:kAdvertisementProductURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if (![responseObject[@"banners"] isKindOfClass:[NSNull class]]) {
                
                
                NSError *err ;
                
                NSArray * array = responseObject[@"products_list"];
                
                [self createProductModel:array];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    
                }
                
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)createJS:(NSArray *)array
{
    NSMutableArray * jArray = [NSMutableArray arrayWithCapacity:0];
    
    self.bannerArray = [NSMutableArray arrayWithArray:array];
    
    for (int i = 0; i < array.count; i++) {
        JOLImageSlide * joliImageSlide = [[JOLImageSlide alloc] init];
        
        NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:array[i]];
        
        NSString * encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault, (CFStringRef)dic[@"image_url"], NULL, NULL,  kCFStringEncodingUTF8 ));
        
        [dic setValue:encodedString forKey:@"image_url"];
        
        joliImageSlide.infoDict = dic;
        
        joliImageSlide.image = dic[@"image_url"];
        
        joliImageSlide.title = @"";
        
        [jArray addObject:joliImageSlide];
        
        if ([dic[@"type"] isEqualToString:@"task"]) {
            [self loadTaskDetailData:dic];
        }
    }
    
    _joliImageSlider = [[JOLImageSlider alloc] initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH, 140) andSlides:jArray];
    
    _joliImageSlider.backgroundColor = [UIColor blackColor];
    
    _joliImageSlider.delegate = self;
    
    [_tableHeaderview addSubview:_joliImageSlider];
    //_joliImageSlider.slideArray = jArray;
}

-(void)loadTaskDetailData:(NSDictionary *)dic
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSDictionary *dict = @{
                           @"tid":dic[@"relate_id"]
                           };
    [manager GET:kGetTaskDetailURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if (![responseObject[@"task"] isKindOfClass:[NSNull class]]) {
                
                NSError *err ;
                
                NSDictionary * dic = responseObject[@"task"];
                
                [self createTaskDetailModel:dic];
                
                if (err) {
                    NSLog(@"%@",err);
                }
                else
                {
                    
                }
                
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];

}


-(void)createTaskDetailModel:(NSDictionary *)dic
{
    self.taskModel = [[TaskModel alloc] init];
    
    [self.taskModel setValuesForKeysWithDictionary:dic];
}

-(void)createTaskModel:(NSArray *)array
{
    if (self.taskArray.count) {
        [self.taskArray removeAllObjects];
    }
    
    for (NSDictionary * dic in array) {
        TaskModel * model = [[TaskModel alloc] init];
        
        [model setValuesForKeysWithDictionary:dic];
        
        [self.taskArray addObject:model];
    }
    
    [_tableView reloadData];
}


-(void)createProductModel:(NSArray *)array
{
    self.productArray = [NSMutableArray arrayWithCapacity:0];
    
    for (NSDictionary * dic in array) {
        ProductSummaryMD * model  = [[ProductSummaryMD alloc] init];
        
        [model setValuesForKeysWithDictionary:dic];
        
        [self.productArray addObject:model];
    }
    
    [_tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.taskArray.count;
    }
    
    return self.productArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray * array = @[@"爆棚赚派豆", @"热门兑换商品"];
    
    return array[section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ProductCell * cell = [tableView dequeueReusableCellWithIdentifier:@"product"];
        
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:self options:nil] firstObject];
        }
        TaskModel * model = self.taskArray[indexPath.row];
        
        [cell config:model];
        //通过RGB来定义自己的颜色
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor appBackgroundColor];
        
        return cell;
    }
    else
    {
        FreeGoodsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"free"];
        
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"FreeGoodsCell" owner:self options:nil] firstObject];
        }
        
        ProductDetailMD * model = self.productArray[indexPath.row];
        
        [cell config:model];
        
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor appBackgroundColor];
        
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 104;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        if (![[UserManager sharedManager] hasLogin])  {
            [self showloginAlert];
            return;
        }
        
        TaskModel *model = self.taskArray[indexPath.row];
        if ([model.status isEqualToString:kTaskStatusComplete]) {
            NSString *s = [NSString stringWithFormat:@"您已完成 %@",model.title];
            [self showHUDText:s  xOffset:0 yOffset:0];
            
            return;
        }
        
        URLTaskVC *task = [[URLTaskVC alloc ]initWithNibName:@"URLTaskVC" bundle:nil];
        task.tmodel = model;
        [self.navigationController pushViewController:task animated:YES];
        
        return;
        if ([model.type isEqualToString:kTaskTypeInfo]) {
            TaskDetailInfo *info = [[TaskDetailInfo alloc] initWithNibName:@"TaskDetailInfo" bundle:nil];
            info.tmodel = model;
            [self.navigationController pushViewController:info animated:YES];
            
        }else if ([model.type isEqualToString:kTaskTypeSign]) {
            TaskDetail01VC *detail = [[TaskDetail01VC alloc] initWithNibName:@"TaskDetail01VC" bundle:nil];
            detail.tmodel = model;
            [self.navigationController pushViewController:detail animated:YES];
        }else if ([model.type isEqualToString:kTaskTypeExtend]) {
            
        }
        
    }
    else
    {
        CommodityDetailVC *detailvc = [[CommodityDetailVC alloc] init];
        detailvc.productSummoryMD = self.productArray[indexPath.row];
        [self.navigationController pushViewController:detailvc animated:YES];
    }
   
}

- (void)showloginAlert{
    RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"取消"];
    RIButtonItem *sure = [RIButtonItem itemWithLabel:@"确定" action:^{
        [self login:nil];
    }];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您尚未登录，请先登录" cancelButtonItem:cancel otherButtonItems:sure, nil];
    [alert show];
}

- (void)login:(id)sender{
    LoginViewController *Login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:Login];
    [self presentViewController:nav animated:YES completion:nil];
    // [self.navigationController pushViewController:Login animated:YES];
}


-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 38;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSArray * array = @[@"爆棚赚派豆:", @"热门兑换的商品:"];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 38)];
    
    view.backgroundColor = [UIColor appBackgroundColor];
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(15, 14, 120, 15)];
    
    label.font = [UIFont systemFontOfSize:14];
    
    label.textAlignment = NSTextAlignmentLeft;
    
    label.text = array[section];
    
    label.textColor = [UIColor appTableTitleColor];
    
    [view addSubview:label];
    
    return view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)imagePager:(JOLImageSlider *)imagePager didSelectImageAtIndex:(NSUInteger)index
{
    NSDictionary * dic = self.bannerArray[index];
    
    if ([dic[@"type"] isEqualToString:@"product"]) {
        CommodityDetailVC *detailvc = [[CommodityDetailVC alloc] init];
        
        detailvc.releated_id = dic[@"relate_id"];
        
        [self.navigationController pushViewController:detailvc animated:YES];
    }
    else if ([dic[@"type"] isEqualToString:@"task"])
    {
        if (![[UserManager sharedManager] hasLogin])  {
            [self showloginAlert];
            return;
        }
        
        if ([self.taskModel.status isEqualToString:kTaskStatusComplete]) {
            NSString *s = [NSString stringWithFormat:@"您已完成 %@",self.taskModel.title];
            [self showHUDText:s  xOffset:0 yOffset:0];
            
            return;
        }
        
        URLTaskVC *task = [[URLTaskVC alloc ]initWithNibName:@"URLTaskVC" bundle:nil];
        task.tmodel = self.taskModel;
        [self.navigationController pushViewController:task animated:YES];
        
        
        return;
        if ([self.taskModel.type isEqualToString:kTaskTypeInfo]) {
            TaskDetailInfo *info = [[TaskDetailInfo alloc] initWithNibName:@"TaskDetailInfo" bundle:nil];
            info.tmodel = self.taskModel;
            [self.navigationController pushViewController:info animated:YES];
            
        }else if ([self.taskModel.type isEqualToString:kTaskTypeSign]) {
            TaskDetail01VC *detail = [[TaskDetail01VC alloc] initWithNibName:@"TaskDetail01VC" bundle:nil];
            detail.tmodel = self.taskModel;
            [self.navigationController pushViewController:detail animated:YES];
        }else if ([self.taskModel.type isEqualToString:kTaskTypeExtend]) {
            
        }
    }
    
    else
    {
        TopicViewController * vc = [[TopicViewController alloc] init];
        
        vc.pid = dic[@"relate_id"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)click:(UIButton *)button
{
    if (button.tag == 1) {
        [self.navigationController pushViewController:[[TabFirstViewController alloc] init] animated:YES];
    }else if(button.tag == 2) {
        TabSecondViewController *tabSecond = [[TabSecondViewController alloc]init];
        tabSecond.currStation = _currStation;
        tabSecond.bannerArray = self.bannerArray;
        [self.navigationController pushViewController:tabSecond animated:YES];
    }
}

- (void)loadSubwayStations{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    NSDictionary *param = @{
                            @"city":@"南京",
                            @"last_update_time":@0
                            };
    [manager GET:kGetSubwayStations parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"] intValue ] == 0) {
            NSError *error = nil;
            NSArray *arr = [StationLine arrayOfModelsFromDictionaries:responseObject[@"line_list"] error:&error];
            if (error) {
                NSLog(@"%@",error);
            }else{
                if (_metroLineArr.count) {
                    [_metroLineArr removeAllObjects];
                }
                [_metroLineArr addObjectsFromArray:arr];
                //flash
                
                
                if([NSKeyedArchiver archiveRootObject:_metroLineArr toFile:kMetroLinePath]){
                    NSLog(@"写入地铁线路成功");
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyWriteMetroLineSuccess object:nil];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
