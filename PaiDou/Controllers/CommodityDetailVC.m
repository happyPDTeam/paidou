//
//  CommodityDetailVC.m
//  PaiDou
//
//  Created by JSen on 14/11/11.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "CommodityDetailVC.h"
#import "ProductSummaryMD.h"
#import "CommitOrderVC.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "ProductDetailMD.h"
#import "StrikeLabel.h"
#import "CommitFreeOrderVC.h"

@interface CommodityDetailVC ()<UITableViewDelegate,UITableViewDataSource>{
    UIButton *_buyButton;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *imageCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *nameCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *dateCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *priceCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *numberCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *desCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *shopCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *addCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *zeroPriceCell;
@property (weak, nonatomic) IBOutlet UILabel *lb_zero_name;
@property (weak, nonatomic) IBOutlet UILabel *lb_zero_douzi;
@property (weak, nonatomic) IBOutlet UILabel *lb_zero_oriPrice;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (weak, nonatomic) IBOutlet UILabel *lb_songdou;
@property (weak, nonatomic) IBOutlet UILabel *lb_yuanjian;
@property (weak, nonatomic) IBOutlet UILabel *lb_kezhuan;
@property (weak, nonatomic) IBOutlet UILabel *_lb_huibaoRate;
@property (weak, nonatomic) IBOutlet UILabel *lb_people;
@property (weak, nonatomic) IBOutlet UILabel *lb_time;
@property (weak, nonatomic) IBOutlet UIButton *btn_comment;
- (IBAction)btn_payAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_pay;

- (IBAction)btn_comm_act:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextView *productDes;
@property (weak, nonatomic) IBOutlet UITextView *tViewShop;
@property (weak, nonatomic) IBOutlet UITextView *tAddress;

@property (strong,nonatomic) ProductDetailMD *detaiModel;


@property (weak, nonatomic) IBOutlet UILabel *lb_p1;
@property (weak, nonatomic) IBOutlet UILabel *lb_p2;

@end

@implementation CommodityDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   // [self _addTableFooterView];
    
    self.tableView.separatorColor = UIColorFromRGB(0xf2f2f2);
    _btn_pay.enabled = NO;
    _btn_pay.layer.cornerRadius = 3;
    _btn_pay.layer.masksToBounds = YES;
    
    if (_productSummoryMD) {
        [self loadData:_productSummoryMD.pid];
    }
    else
    {
        [self loadData:self.releated_id];
    }
    
//    if (_productSummoryMD)
//    {
//        [self loadData];
//        NSString *iconUrl = [_productSummoryMD.avatar stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        [_icon sd_setImageWithURL:[NSURL URLWithString:iconUrl] placeholderImage:[UIImage JSenImageNamed:kPlaceHolderImageName]];
//        
//        _lb_name.text = _productSummoryMD.title;
////        _lb_songdou.text= [NSString stringWithFormat:@"送%d豆",_productSummoryMD.return_score];
//        _lb_yuanjian.text = [NSString stringWithFormat:@"原价:%@",[Utils fenToYuan:_productSummoryMD.original_price]];
//        _lb_kezhuan.text =[NSString stringWithFormat:@"%@元", [Utils fenToYuan:_productSummoryMD.earn_money]];
//        __lb_huibaoRate.text = [NSString stringWithFormat:@"%d%%",_productSummoryMD.return_rate];
//        
//        _lb_people.text = [NSString stringWithFormat:@"%d人领取",_productSummoryMD.sold_count];
//        _lb_time.text =[NSString stringWithFormat:@"有效期:%@",[Utils dateFromTimeInterval:_productSummoryMD.end_time]];
//        
//        _lb_p1.text = [NSString stringWithFormat:@"%@元",[Utils fenToYuan:_productSummoryMD.sale_price]];
//        _lb_p2.text = [NSString stringWithFormat:@"%d派豆",_productSummoryMD.score_max];
////        _btn_comment setTitle:[NSString stringWithFormat:@"%d",_productSummoryMD.] forState:<#(UIControlState)#>
//        
//        if (_productSummoryMD.sale_price == 0)
//        {
//            _lb_zero_name.text = _productSummoryMD.title;
//            _lb_zero_douzi.text = [NSString stringWithFormat:@"%d派豆",_productSummoryMD.score_max];
//            _lb_zero_oriPrice.text = [NSString stringWithFormat:@"市场价%@",[Utils fenToYuan:_productSummoryMD.original_price]];
//        }
//    
//    }
    
    
    [self showLeftBackBarbuttonItemWithSelector:@selector(back:)];
    self.navigationItem.titleView = [Utils titleLabelWithTitle:@"商品详情"];
      // Do any additional setup after loading the view from its nib.
}
- (void)back:(UIBarButtonItem *)item{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)loadData:(NSString *)releated_id
{
    _buyButton.enabled = NO;
  //  [self showLoadingAnimated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONReadingAllowFragments];
    NSString *uid = [[UserManager sharedManager ]uid];
  
    NSDictionary *dict = @{
                           @"uid":uid,
                           @"pid":releated_id
                           };
    [manager GET:kProductDetailURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"err_code"]intValue ]== 0) {
            if (![responseObject[@"product"] isKindOfClass:[NSNull class]]) {
                
                
                NSError *err ;
                _detaiModel = [[ProductDetailMD alloc]initWithDictionary:responseObject[@"product"] error:&err];
              
                if (err) {
                    NSLog(@"%@",err);
                }else{
                    _btn_pay.enabled = YES;
                    [self loadDetailInfo];
                }
               
            }
          
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        _buyButton.enabled = YES;
    }];
}

- (void)loadDetailInfo{
    
    NSString *str = nil;
    if (_detaiModel.display_stock) {
        str = [NSString stringWithFormat:@"%d人领取 剩余%d份",_productSummoryMD.sold_count,_productSummoryMD.in_stock-_productSummoryMD.sold_count];
    }else{
        str = [NSString stringWithFormat:@"%d人领取",_productSummoryMD.sold_count];
    }
    _lb_people.text = str;
   
    [_btn_comment setTitle:[NSString stringWithFormat:@"%d",_detaiModel.comment_count] forState:UIControlStateNormal];
    
   
    //    textview 改变字体的行间距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 10;// 字体的行间距
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14],
                                 NSParagraphStyleAttributeName:paragraphStyle,
                                 NSForegroundColorAttributeName:UIColorFromRGB(0x7c7c7c)
                                 };
    NSString *strShop = [NSString stringWithFormat:@"商户地址：%@ \n商户电话：%@",_detaiModel.biz.address,_detaiModel.biz.tel];
    
    _tViewShop.attributedText = [[NSAttributedString alloc] initWithString:strShop attributes:attributes];
    
    _tAddress.attributedText = [[NSAttributedString alloc] initWithString:_detaiModel.notes attributes:attributes];
    
    _productDes.attributedText = [[NSAttributedString alloc] initWithString:_detaiModel.info attributes:attributes];
    
    
    NSString *iconUrl = [_detaiModel.avatar stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [_icon sd_setImageWithURL:[NSURL URLWithString:iconUrl] placeholderImage:[UIImage JSenImageNamed:kPlaceHolderImageName]];
    
    _lb_name.text = _detaiModel.title;
    //        _lb_songdou.text= [NSString stringWithFormat:@"送%d豆",_productSummoryMD.return_score];
    _lb_yuanjian.text = [NSString stringWithFormat:@"会员价:%@元",[Utils fenToYuan:_detaiModel.original_price]];
    _lb_kezhuan.text =[NSString stringWithFormat:@"%@元", [Utils fenToYuan:_detaiModel.earn_money]];
    __lb_huibaoRate.text = [NSString stringWithFormat:@"%d%%",_detaiModel.return_rate];
    
    //_lb_people.text = [NSString stringWithFormat:@"%d人领取",_detaiModel.sold_count];
    _lb_time.text =[NSString stringWithFormat:@"有效期:%@",[Utils dateFromTimeInterval:_detaiModel.end_time]];
    
    _lb_p1.text = [NSString stringWithFormat:@"%@元",[Utils fenToYuan:_detaiModel.sale_price]];
    _lb_p2.text = [NSString stringWithFormat:@"%d派豆",_detaiModel.score_max];
    //        _btn_comment setTitle:[NSString stringWithFormat:@"%d",_productSummoryMD.] forState:<#(UIControlState)#>
    
    if (_detaiModel.sale_price == 0)
    {
        _lb_zero_name.text = _detaiModel.title;
        _lb_zero_douzi.text = [NSString stringWithFormat:@"%d派豆",_detaiModel.score_max];
        _lb_zero_oriPrice.text = [NSString stringWithFormat:@"会员价%@",[Utils fenToYuan:_detaiModel.original_price]];
    }
    
//    _tViewShop.text = strShop;
//    _tAddress.text = _detaiModel.notes;
//    _productDes.text = _detaiModel.info;
//    
//    
//    _productDes.textColor = UIColorFromRGB(0x7c7c7c);
//    _productDes.font = [UIFont systemFontOfSize:14];
//    
//    _tViewShop.textColor = UIColorFromRGB(0x7c7c7c);
//    _tViewShop.font = [UIFont systemFontOfSize:14];
//    
//    _tAddress.textColor = UIColorFromRGB(0x7c7c7c);
//    _tAddress.font = [UIFont systemFontOfSize:14];
    //
     [_tableView reloadData];
}


- (void)_addTableFooterView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    view.backgroundColor = [UIColor lightGrayColor];
    
    _buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _buyButton.frame = CGRectMake(20, 0, view.width-40, view.height);
    [_buyButton setTitle:@"立即购买" forState:UIControlStateNormal];
    
    [_buyButton setTitleColor:[UIColor appBlackTextColor] forState:UIControlStateNormal];
    [_buyButton setBackgroundImage:[UIImage JSenImageNamed:@"btn_bg_normal.png"] forState:UIControlStateNormal];
   // _buyButton.enabled = NO;
    
    [_buyButton handleControlEvent:UIControlEventTouchUpInside withBlock:^(id sender) {
        
//        CommitOrderVC *commitOrder = [[CommitOrderVC alloc ]initWithNibName:@"CommitOrderVC" bundle:nil];
//        commitOrder.detailModel = _detaiModel;
//        [self.navigationController pushViewController:commitOrder animated:YES];
        UserManager *manager = [UserManager sharedManager];
        if (manager.hasLogin) {

#warning  - 需要等到_detaiModel获取到才继续

            CommitOrderVC *commitOrder = [[CommitOrderVC alloc ]initWithNibName:@"CommitOrderVC" bundle:nil];
            commitOrder.detailModel = _detaiModel;
            [self.navigationController pushViewController:commitOrder animated:YES];
            
        }else {
        //登录
            [self _login];
        }
       
    }];
    
    [view addSubview:_buyButton];
    self.tableView.tableFooterView = view;
    
    
}

- (void)_login{
    LoginViewController *login = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    BaseNavigationController *navLogin = [[BaseNavigationController alloc]initWithRootViewController:login];
    [self presentViewController:navLogin animated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 4;
            break;
        case 1:{
            return 1;
        }
        case 2:{
            return 1;
        }
        case 3:{
            return 1;
        }
        default:
            break;
    }
    return 0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:{
                    
                    return _imageCell;
                    break;
                }
                case 1:{
                    if (_productSummoryMD.sale_price == 0) {
                        return _zeroPriceCell;
                    }
                    return _nameCell;
                }
                case 2:{
                    return _dateCell;
                }
                case 3:{
                    return _numberCell;
                }
               
                default:{
                    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
                    return cell;
                }
        }
 
    }
        case 1:{
            if (indexPath.row == 0) {
                return _shopCell;
                
                
            }
            
        }
            
        case 2:{
             return _desCell;
        }
        case 3:{
            return _addCell;
        }
        default:{
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            return cell;
    }
      return nil;
}
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                return 145;
                break;
            case 1:{
                return 65;
            }
            case 2:
            case 3:
            case 4:{
                return 44;
            }
            default:
                return 44;
                break;
        }
    }else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            //商户简介
            if (_detaiModel) {
                CGSize tsize = [_tViewShop sizeThatFits:CGSizeMake(_desCell.contentView.width-10, MAXFLOAT)];
                return tsize.height + 35;
            }else{
                return 60;
            }
            
        }
    }else if (indexPath.section == 2) {
        //商品信息
      
        
        if (_detaiModel) {
            
            CGSize tsize = [_productDes sizeThatFits:CGSizeMake(_desCell.contentView.width-10, MAXFLOAT)];
            return tsize.height + 30;
        }else{
            return 60;
        }
    }else if (indexPath.section == 3) {
        //使用须知
        if (_detaiModel) {
            CGSize tsize = [_tAddress sizeThatFits:CGSizeMake(_desCell.contentView.width-10, MAXFLOAT)];
            return tsize.height + 40;
        }else{
            return 60;
        }
    }
    
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section ) {
        case 0:
            return 0.1;
            break;
        case 1:
            case 2:
            case 3:
        {
            return 10;
        }
            
            
        default:
            break;
    }
    return 10;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_payAction:(id)sender {
    
//    CommitOrderVC *commitOrder = [[CommitOrderVC alloc ]initWithNibName:@"CommitOrderVC" bundle:nil];
//    commitOrder.detailModel = _detaiModel;
//    [self.navigationController pushViewController:commitOrder animated:YES];
//    
//    return;
    UserManager *manager = [UserManager sharedManager];
    if (manager.hasLogin) {
        
#warning  - 需要等到_detaiModel获取到才继续
        if (_detaiModel.sale_price > 0) {
            CommitOrderVC *commitOrder = [[CommitOrderVC alloc ]initWithNibName:@"CommitOrderVC" bundle:nil];
            commitOrder.detailModel = _detaiModel;
            [self.navigationController pushViewController:commitOrder animated:YES];
        }
        else
        {
            CommitFreeOrderVC * commitFreeOrder = [[CommitFreeOrderVC alloc] initWithNibName:@"CommitFreeOrderVC" bundle:nil];
            
            commitFreeOrder.detailModel = _detaiModel;
            
            [self.navigationController pushViewController:commitFreeOrder animated:YES];
        }
        
        
    }else {
        //登录
         [self _login];
    }
}

- (IBAction)btn_comm_act:(UIButton *)sender {
}
@end
