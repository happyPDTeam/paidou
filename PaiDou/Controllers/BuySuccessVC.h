//
//  BuySuccessVC.h
//  PaiDou
//
//  Created by JSen on 14/11/17.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "BaseViewController.h"
@class ProductDetailMD;
@class OrderSummaryMD;

@interface BuySuccessVC : BaseViewController
@property (nonatomic, strong) ProductDetailMD *productModel ;

@property (nonatomic, retain) OrderSummaryMD *orderModel;

@property (nonatomic, copy) NSString *timeStr;
@property (nonatomic, copy) NSString *xuqiuStr;
@end
