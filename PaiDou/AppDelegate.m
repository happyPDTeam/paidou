//
//  AppDelegate.m
//  PaiDou
//
//  Created by JSen on 14/11/5.
//  Copyright (c) 2014年 wifitong. All rights reserved.
//

#import "AppDelegate.h"
#import "CustomTabBarController.h"
#import "TabFirstViewController.h"
#import "TabSecondViewController.h"
#import "SideMenuViewController.h"
#import "RESideMenu.h"
#import "BaseNavigationController.h"
#import "UIImageSize.h"
#import "WXApi.h"
#import "AlixPayResult.h"
#import "DataSigner.h"
#import "DataVerifier.h"
#import "PartnerConfig.h"
#import "ScanQRCodeVC.h"
#import "TabFirstNewVC.h"
#import "SideMenuViewController.h"
#import "LocationMgr.h"
#import "BMapKit.h"
#import "MyProfileVC.h"
#import "MiPushSDK.h"
#import "EAIntroPage.h"
#import "EAIntroView.h"
#import "GuideView.h"
#import <ShareSDK/ShareSDK.h>
#import <WeiboApi.h>
#import <QQConnection/ISSQQApp.h>
#import <QZoneConnection/QZoneConnection.h>
#import <QZoneConnection/ISSQZoneApp.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>


static NSString *const  kNotFirstLanuch = @"not first lanuch";

@interface AppDelegate ()<RESideMenuDelegate,BMKGeneralDelegate,MiPushSDKDelegate,EAIntroDelegate>
{
    BMKMapManager* _mapManager;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
   
//    [MiPushSDK registerMiPush:self];
    
    [[UserManager sharedManager] autoLogin];
    
    
    //保留badge
//    [UserManager sharedManager].appBadge = [UIApplication sharedApplication].applicationIconBadgeNumber;
    NSLog(@"%@",launchOptions);
    if (launchOptions) {
        [UserManager sharedManager].appBadge = 1;
    }
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
    [ShareSDK registerApp:ShareSDKAppkey];
#warning initializePlat: 必须在registerApp：之前调用
    [self initializePlat];
    BOOL isok = [WXApi registerApp:kWXAppID];
    if (isok) {
        NSLog(@"注册微信成功");
    }else{
        NSLog(@"注册微信失败");
    }
    
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg_tabbar.png"]];
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    
    // 要使用百度地图，请先启动BaiduMapManager
    _mapManager = [[BMKMapManager alloc]init];
    BOOL ret = [_mapManager start:kBaiDuMapKey generalDelegate:self];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
//
    
    [self loadTabbar];
    
    
    BOOL notFirstLanuch = [[NSUserDefaults standardUserDefaults ]boolForKey:kNotFirstLanuch];
    if (!notFirstLanuch) {
        [self showSplash];
    }
    
   
    return YES;
}


- (void)initializePlat
{
    /**
     连接新浪微博开放平台应用以使用相关功能，此应用需要引用SinaWeiboConnection.framework
     http://open.weibo.com上注册新浪微博开放平台应用，并将相关信息填写到以下字段
     **/
    [ShareSDK connectSinaWeiboWithAppKey:@"822617119"
                               appSecret:@"2a79219d21593e38cef294ae11be3a5c"
                             redirectUri:@"http://www.apaidou.com"];
    
    /**
     连接腾讯微博开放平台应用以使用相关功能，此应用需要引用TencentWeiboConnection.framework
     http://dev.t.qq.com上注册腾讯微博开放平台应用，并将相关信息填写到以下字段
     
     如果需要实现SSO，需要导入libWeiboSDK.a，并引入WBApi.h，将WBApi类型传入接口
     **/
//    [ShareSDK connectTencentWeiboWithAppKey:@"801307650"
//                                  appSecret:@"ae36f4ee3946e1cbb98d6965b0b2ff5c"
//                                redirectUri:@"http://www.sharesdk.cn"
//                                   wbApiCls:[WeiboApi class]];
    /**
     连接QQ空间应用以使用相关功能，此应用需要引用QZoneConnection.framework
     http://connect.qq.com/intro/login/上申请加入QQ登录，并将相关信息填写到以下字段
     
     如果需要实现SSO，需要导入TencentOpenAPI.framework,并引入QQApiInterface.h和TencentOAuth.h，将QQApiInterface和TencentOAuth的类型传入接口
     **/
    [ShareSDK connectQZoneWithAppKey:@"1103818269"
                           appSecret:@"YNwanSXXJ1MpV3zq"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    
    /**
     连接微信应用以使用相关功能，此应用需要引用WeChatConnection.framework和微信官方SDK
     http://open.weixin.qq.com上注册应用，并将相关信息填写以下字段
     **/
    //    [ShareSDK connectWeChatWithAppId:@"wx4868b35061f87885" wechatCls:[WXApi class]];
    [ShareSDK connectWeChatWithAppId:@"wxffe514bedc4db701"
                           appSecret:@"2180362e1b76c94dc8910f77dc24d45d"
                           wechatCls:[WXApi class]];
//    [ShareSDK connectWeChatWithAppId:kWXAppID
//                           appSecret:@"9d5b7c975a5ac253d622bb7fdb9a7148"
//                           wechatCls:[WXApi class]];
    
    /**
     连接QQ应用以使用相关功能，此应用需要引用QQConnection.framework和QQApi.framework库
     http://mobile.qq.com/api/上注册应用，并将相关信息填写到以下字段
     **/
    //旧版中申请的AppId（如：QQxxxxxx类型），可以通过下面方法进行初始化
    //    [ShareSDK connectQQWithAppId:@"QQ075BCD15" qqApiCls:[QQApi class]];
    
    [ShareSDK connectQQWithQZoneAppKey:@"1103818269"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];


}

- (void)showSplash {
   
    EAIntroPage *page01 = [EAIntroPage page];
    page01.bgImage = [UIImage JSenImageNamed:@"引导页-05.jpg"];
    
    EAIntroPage *page02 = [EAIntroPage page];
    page02.bgImage = [UIImage JSenImageNamed:@"引导页-06.jpg"];
    
    EAIntroPage *page03 = [EAIntroPage page];
    page03.bgImage = [UIImage JSenImageNamed:@"引导页-08.jpg"];
    
    EAIntroPage *page04 = [EAIntroPage page];
    page04.bgImage = [UIImage JSenImageNamed:@"引导页-07.jpg"];
    EAIntroView *view = [[EAIntroView alloc]initWithFrame:self.window.bounds andPages:@[page01,page02,page03,page04]];
    view.delegate = self;
    // [view showSkipButtonOnlyOnLastPage];
    [view.skipButton setTitle: @"跳过" forState:UIControlStateNormal];
    [view showInView:self.window.rootViewController.view animateDuration:0.3f];
}

- (void)introDidFinish:(EAIntroView *)introView{
    NSArray * array = @[@"引导页1.jpg", @"引导页2.jpg"];
    
    GuideView * view = [[GuideView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) array:array];
    
    [view becomeFirstResponder];
    
    [[UIApplication  sharedApplication].delegate.window addSubview:view];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kNotFirstLanuch];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadTabbar{
    CustomTabBarController *tabbar = [[CustomTabBarController alloc]init];
    
    
//    NSDictionary *firstVCInfo = @{
//                                  kTabClassName:@"TabFirstViewController",
//                                  kTabItemTitle:@"赚派豆",
//                                  kTabNormalImageName:@"index_first_normal.png",
//                                  kTabHighlihtImageName:@"index_first_selected.png"
//                                  };
//    NSDictionary *secondVCInfo = @{
//                                   kTabClassName:@"TabSecondViewController",
//                                   kTabItemTitle:@"派豆兑换",
//                                   kTabNormalImageName:@"index_second_normal.png",
//                                   kTabHighlihtImageName:@"index_second_selected.png"
//                                   };
    
    
    NSDictionary *thirdVCInfo = @{
                                   kTabClassName:@"ScanQRCodeVC",
                                   kTabItemTitle:@"",
                                   kTabNormalImageName:@"saomiao",
                                   kTabHighlihtImageName:@"saomiao_hov"
                                   };
    
    NSDictionary *firstNewVCInfo = @{
                                   kTabClassName:@"TabFirstNewVC",
                                   kTabItemTitle:@"首页",
                                   kTabNormalImageName:@"index_first_normal.png",
                                   kTabHighlihtImageName:@"index_first_selected.png"
                                   };
    
    NSDictionary *sidemenuVCInfo = @{
                                     kTabClassName:@"MyProfileVC",
                                     kTabItemTitle:@"我的",
                                     kTabNormalImageName:@"index_second_normal.png",
                                     kTabHighlihtImageName:@"index_second_selected.png"
                                     };
    [tabbar loadViewControllersWithInfoDictionary:firstNewVCInfo,thirdVCInfo,sidemenuVCInfo,nil];
    
   
//    [tabbar addCenterButtonWithImage:[UIImage imageNamed:@"camera_button_take.png"] highlightImage:[UIImage imageNamed:@"camera_button_highlight.png"]];
    
    SideMenuViewController *leftvc = [[SideMenuViewController alloc]init];
    BaseNavigationController *navleft = [[BaseNavigationController alloc]initWithRootViewController:leftvc];
    RESideMenu *sideMenu = [[RESideMenu alloc] initWithContentViewController:tabbar leftMenuViewController:nil rightMenuViewController:navleft];
    
    sideMenu.backgroundImage = [UIImage imageNamed:@"Stars.png"];
    sideMenu.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    sideMenu.contentViewShadowColor = [UIColor blackColor];
    sideMenu.contentViewShadowOffset = CGSizeMake(0, 1);
    sideMenu.contentViewShadowRadius = 10;
    sideMenu.contentViewShadowEnabled = YES;
    sideMenu.contentViewShadowOpacity = 0.6;
    sideMenu.delegate = self;
   
    self.window.rootViewController = tabbar;
}

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController{
    UINavigationController *nav = (UINavigationController *)menuViewController;
    UIViewController *vc = nav.viewControllers[0];
    if ([vc isKindOfClass:[SideMenuViewController class]]) {
        SideMenuViewController *sd = (SideMenuViewController *)vc;
        if ([sd respondsToSelector:@selector(willShow)]) {
            [sd willShow];
        }
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [UserManager sharedManager].appBadge = [UIApplication sharedApplication].applicationIconBadgeNumber;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
 sourceApplication:
 
 1.com.tencent.xin
 
 2.com.alipay.safepayclient
 */
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    NSLog(@"source app-%@, des app-%@",sourceApplication,application);
    
    if ([sourceApplication isEqualToString:@"com.tencent.xin"]) {
        return [WXApi handleOpenURL:url delegate:self];
    }else if ([sourceApplication isEqualToString:@"com.alipay.safepayclient"]) {
        [self parse:url application:application];
        return YES;
    }
    return YES;
    
}


- (void)onResp:(BaseResp *)resp
{
    NSLog(@"%@",resp);
    if ([resp isKindOfClass:[PayResp class]]) {
        
        if (resp.errCode == 0) {
             [[NSNotificationCenter defaultCenter] postNotificationName:kSafePaySuccessNotificatino object:nil userInfo:nil];
        }else{
             [[NSNotificationCenter defaultCenter] postNotificationName:kSafePayFailedNotification object:nil userInfo:nil];
        }
        
        NSString *strTitle = [NSString stringWithFormat:@"支付结果"];
        NSString *strMsg = [NSString stringWithFormat:@"errcode:%d", resp.errCode];
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle
                                                        message:strMsg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
     //   [alert show];
        
        
    }
}



- (void)parse:(NSURL *)url application:(UIApplication *)application {
    
    //结果处理
    AlixPayResult* result = [self handleOpenURL:url];
    
    if (result)
    {
        NSDictionary *dict = @{kSafePayResultKey:result};
        
        if (result.statusCode == 9000)
        {
            /*
             *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
             */
            
            //交易成功
            // NSString* key = @"签约帐户后获取到的支付宝公钥";
            id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(AlipayPubKey);
            
            if ([verifier verifyString:result.resultString withSign:result.signString])
            {
                //验证签名成功，交易结果无篡改
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kSafePaySuccessNotificatino object:nil userInfo:dict];
            }
            
        }
        else
        {
            //交易失败
            [[NSNotificationCenter defaultCenter] postNotificationName:kSafePayFailedNotification object:nil userInfo:dict];
        }
    }
    else
    {
        //失败
        [[NSNotificationCenter defaultCenter] postNotificationName:kSafePayFailedNotification object:nil userInfo:nil];
    }
    
}

- (AlixPayResult *)resultFromURL:(NSURL *)url {
    NSString * query = [[url query] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return [[AlixPayResult alloc] initWithString:query];
    
}



- (void)onGetNetworkState:(int)iError
{
    if (0 == iError) {
        NSLog(@"联网成功");
    }
    else{
        NSLog(@"onGetNetworkState %d",iError);
    }
    
}

- (void)onGetPermissionState:(int)iError
{
    if (0 == iError) {
        NSLog(@"授权成功");
    }
    else {
        NSLog(@"onGetPermissionState %d",iError);
    }
}


- (AlixPayResult *)handleOpenURL:(NSURL *)url {
    AlixPayResult * result = nil;
    
    if (url != nil && [[url host] compare:@"safepay"] == 0) {
        result = [self resultFromURL:url];
    }
    
    return result;
}

#pragma mark - mi push

#pragma mark 注册push服务.
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"%@",[NSString stringWithFormat:@"APNS token: %@", [deviceToken description]]);
   
    // 注册APNS成功, 注册deviceToken
    [MiPushSDK bindDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    
    NSLog(@"%@",[NSString stringWithFormat:@"APNS error: %@", err]);
    // 注册APNS失败.
    // 自行处理.
}

#pragma mark Local And Push Notification
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    NSLog(@"收到通知  %@",userInfo);
    NSString *messageId = [userInfo objectForKey:@"_id_"];
    [UserManager sharedManager].appBadge = 1;
    if (application.applicationState == UIApplicationStateActive) {
        // 转换成一个本地通知，显示到通知栏，你也可以直接显示出一个alertView，只是那样稍显aggressive：）
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.userInfo = userInfo;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:2];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    [MiPushSDK openAppNotify:messageId];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    NSLog(@"noti setting -->%@",notificationSettings);
}

#pragma mark MiPushSDKDelegate
- (void)miPushRequestSuccWithSelector:(NSString *)selector data:(NSDictionary *)data
{
   
    if ([selector isEqualToString:@"registerMiPush:"]) {
      
    }else if ([selector isEqualToString:@"unregisterMiPush"]) {
        
    }
}

- (void)miPushRequestErrWithSelector:(NSString *)selector error:(int)error data:(NSDictionary *)data
{
    NSLog(@"%@",[NSString stringWithFormat:@"error(%d): %@", error, data]);
}

- (NSString*)getOperateType:(NSString*)selector
{
    NSString *ret = nil;
    if ([selector isEqualToString:@"registerMiPush:"]) {
        ret = @"客户端注册设备";
    }else if ([selector isEqualToString:@"unregisterMiPush"]) {
        ret = @"客户端设备注销";
    }else if ([selector isEqualToString:@"bindDeviceToken:"]) {
        ret = @"绑定 PushDeviceToken";
    }else if ([selector isEqualToString:@"setAlias:"]) {
        ret = @"客户端设置别名";
    }else if ([selector isEqualToString:@"unsetAlias:"]) {
        ret = @"客户端取消别名";
    }else if ([selector isEqualToString:@"subscribe:"]) {
        ret = @"客户端设置主题";
    }else if ([selector isEqualToString:@"unsubscribe:"]) {
        ret = @"客户端取消主题";
    }else if ([selector isEqualToString:@"openAppNotify:"]) {
        ret = @"统计客户端";
    }
    
    return ret;
}


@end
